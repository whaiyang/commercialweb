<?php
namespace Store\Controller;
use Think\Controller;
class BaseController extends Controller {

	public function _initialize() {
			
	}

	public function getMsg($errCode,$errMsg,$returnData=null) {
		$returnArr = array(
			"errNo" => $errCode,
			"errMsg" => $errMsg,
		);
		if ($returnData != null) {
			$returnArr["data"] = $returnData;
		}
		echo json_encode($returnArr);
		return true;
	}
}
