<?php
namespace Store\Controller;
use Think\Controller;
class ProductController extends BaseController {

	public function _initialize() {
		$this->_userModel = D("User/CustomerUser");
		$this->_productModel = D("Store/Product");
	}

	public function getlistbysellerid(){
		$sellerid = _REQUEST[sellerid];
		$arrResult = $this->_productModel->getProductListBySellerId($sellerid);
		if (empty($arrResult)) {
			$this->getMsg(0,"商品查询失败",0);
			return;
		}
		return $this->getMsg(0,"",$arrResult);
	}
}