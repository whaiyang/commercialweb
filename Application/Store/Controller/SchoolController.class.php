<?php
namespace Store\Controller;
use Think\Controller;
class SchoolController extends BaseController {

	public function _initialize() {
		$this->_schoolModel = D("Store/School");
	}

	public function list(){
		$arrResult = $this->_schoolModel->getschoollist();
		$this->getMsg(0,"",$arrResult);
	}
}