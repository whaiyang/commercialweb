<?php
namespace Store\Controller;
use Think\Controller;
class UserController extends BaseController {

	public function _initialize() {
		$this->_userModel = D("User/CustomerUser");
		$this->_settingModel = D("Store/Settings");
	}

	public function register(){
		$strInput = $_REQUEST;
		$arrInput = json_decode($strInput);
		$arrResult = $this->_userModel->register($arrInput);
		if (empty($arrResult['errNo'])) {
			$this->getMsg(0,"注册成功",1);
			return;
		}
		else{
			$this->getMsg(0,"注册失败",0);
		}
		echo json_encode($arrResult);
	}

	public function login(){
		$strInput = $_REQUEST;
		$arrInput = json_decode($strInput);
		$boolResult = $this->_userModel->login($arrInput);
		if ($boolResult == true) {
			$this->getMsg(0,"登陆成功",1);
		} else {
			$this->getMsg(0,"登陆失败",0);
		}
	}

	public function forgetpwd(){
		$strInput = $_REQUEST;
		$arrInput = json_decode($strInput);
		$username = $arrInput[username];
		$emailaddress = $arrInput[emailaddress];
		$arrResult = $this->_userModel->getUserInfoByAccountName($username);
		if (empty($arrResult)) {
			$this->getMsg(0,"用户名验证失败",0);
			return;
		}
		$result=mysql_fetch_array($arrResult);
		if(result[email] ！= $emailaddress){
			$this->getMsg(0,"用户名验证失败",0);
			return;
		}
		else{
			$time=time();
			$_url=$username.$result[id]..$time;
			$coded_url = md5(_url);
			$expiration_time=$time+1800;
			$get_array = array('id' => $result[id], 'expiration_time' => $expiration_time);
			$coded_get_array = json_encode(get_array);
			$name='客户'.$username;
			$body='';
			$arrResult = think_send_mail('$emailaddress','$name','找回密码','$body',null);
			if(!arrResult){
				$this->getMsg(0,"邮件发送失败",0);
				return;
			}
			$boolResult = $this->_settingModel->insersettings($coded_url,$coded_get_array);
			if (empty($boolResult)) {
				$this->getMsg(0,"上传数据库失败",0);
				return;
			}
			$this->getMsg(0,"找回密码程序结束",1);
		}
	}

	public function getRandStr($length) {
	  $str = 'abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'; 
	  $randString = ''; 
	  $len = strlen($str)-1; 
	  for($i = 0;$i < $length;$i ++){
	  	$num = mt_rand(0, $len); 
	  	$randString .= $str[$num]; 
	  } 
	  return $randString ;  
	  }

	public function change_pwd_first(){
		$strInput = $_REQUEST;
		$arrInput = json_decode($strInput);
		$boolResult = $this->_userModel->checkAccAndPwd($arrInput);
		if ($boolResult == true) {
			$arrInput['status'] = 1;
			$boolResult = $this->_userModel->changePwd($arrInput);
			if ($boolResult == true) {
				$this->getMsg(0,"密码修改成功",1);
			}
			else{
				$this->getMsg(0,"密码修改失败",0);
			}
		} else {
			$this->getMsg(0,"验证失败",0);
		}
	}

	public function change_pwd_second(){
		$coded_url = $_GET[url];
		$arrResult = $this->_settingModel->getsettingsbykey($coded_url);
		if (empty($arrResult)) {
			$this->getMsg(0,"查询数据库失败",0);
			return;
		}
		$result=mysql_fetch_array($arrResult);
		$decoded_url = json_decode(result[value]);
		$time = time();
		if($time > $decoded_url[expiration_time]){
			$this->getMsg(0,"找回密码超时",0);
			return;
		}
		else{
			
		}
	}

	public function getUserInfoById(){
		$userid = $_REQUEST[userid];
		$arrResult = $this->_userModel->getUserInfoByUid($userid);
		if(empty($arrResult)){
			$this->getMsg(0,"用户数据查询失败",0);
		}
		$arrResult=mysql_fetch_array($arrResult);
		$result = json_encode($arrResult);
		return $this->getMsg(0,"",$result);
	}
}