<?php
namespace Store\Controller;
use Think\Controller;
class UserController extends BaseController {

	public function _initialize() {
		$this->_orderModel = D("Store/Order");
	}

	public function submitOrder(){
		$strInput = $_REQUEST;
		$arrInput = json_decode($strInput);
		$boolResult = $this->_orderModel->createOrder($arrInput);
		if ($boolResult == true) {
			$this->getMsg(0,"订单提交成功",1);
		} else {
			$this->getMsg(0,"订单提交失败",0);
		}
	}

	public function getAllOrders(){
		$custom_id = $_REQUEST[custom_id];
		$arrResult = $this->_orderModel->getAllOrderList($custom_id);
		if (empty($arrResult['errNo'])) {
			$this->getMsg(0,"获取所有订单成功",1);
			return;
		}
		echo json_encode($arrResult);
	}

	public function getUnsolvedOrders(){
		$custom_id = $_REQUEST[custom_id];
		$arrResult = $this->_orderModel->getUnsolvedOrderList($custom_id);
		if (empty($arrResult['errNo'])) {
			$this->getMsg(0,"获取未收货订单成功",1);
			return;
		}
		echo json_encode($arrResult);
	}

	public function getDeliveringOrders(){
		$custom_id = $_REQUEST[custom_id];
		$arrResult = $this->_orderModel->getDeliveringOrderList($custom_id);
		if (empty($arrResult['errNo'])) {
			$this->getMsg(0,"获取配送中订单成功",1);
			return;
		}
		echo json_encode($arrResult);
	}

	public function confirmOrders(){
		$order_id = $_REQUEST[order_id];
		$boolResult = $this->_orderModel->confirmOrder($order_id);
		if ($boolResult == true) {
			$this->getMsg(0,"确认收货成功",1);
		} else {
			$this->getMsg(0,"确认收货失败",0);
		}
	}
}