<?php
	namespace Store\Model;
	use Think\Model;

	/*
	*学校信息model
	*/
	class SchoolModel extends BaseModel {

		protected $SchoolTableName = "school";

		/**
		*@desc 查询所有学校的列表
		*@param none
		*@return school_list
		**/

		public function getschoollist(){
			$strsql = "SELECT name FROM {$This->SchoolTableName}";
			$arrResult = $this->query($strSql);
			if (empty($arrResult)){
				return array();
			}
			return $arrResult;
		}
		
	}