<?php
	namespace Store\Model;
	use Think\Model;

	/*
	*商品信息model
	*/
	class ProductModel extends BaseModel {
		protected $productTableName = "goods";
		protected $productTypeTable = "goods_type";

		/**
		*@desc 按商品id来获取商品信息
		*@param product id
		*@return product infomation
		*/
		public function getProductById($productId) {
			$strSql = "SELECT * FROM {$this->productTableName} WHERE id=%d";
			$arrResult = $this->query($strSql,array($productId));
			if (empty($arrResult)) {
				return array();
			}
			return $arrResult[0];
		}

		/**
		*@desc 按商品id数组来获取所有商品信息
		*@param product id list
		*@return products infomation
		*/
		public function getProductsByIds($idList) {
			if (empty($idList) || !is_array($idList)) {
				return return $this->getErrorMsg(C("ERRNO.ERROR_PARAM_ERR"),C("ERRNO.ERROR_PARAM_ERR_DESC"));
			}
			$strIdGroup = join(",",$idList);
			$strSql = "SELECT * FROM {$this->productTableName} WHERE id IN ($strIdGroup)";
			$productsInfo = $this->query($strSql);
			if (empty($productsInfo)) {
				return $this->getErrorMsg(C("ERRNO.ERR_GET_PRODUCT_FAIL"),C("ERRNO.ERR_GET_PRODUCT_FAIL_DESC"));
			}
			return $productsInfo;
		}

		/**
		*@desc 更新商品信息
		*@param product id,更新字段和值array(key=>value)
		*@return result
		*/
		public function updateProduct($productId,$arrInput) {
			if (empty($productId) || empty($arrInput)) {
				return $this->getErrorMsg(C("ERRNO.ERROR_PARAM_ERR"),C("ERRNO.ERROR_PARAM_ERR_DESC"));
			}
			$strSql = "UPDATE {$this->productTableName} SET ";
			$setFields = array();
			foreach ($arrInput as $key => $value) {
				if (is_string($value)) {
					$setFields[] = "{$key}=%s";
				} else {
					$setFields[] = "{$key}=%d";
				}
			}
			$strSql .= join(",",$setFields);
			$strSql .= " WHERE id=%d";
			$boolResult = $this->execute($strSql,array_merge($arrInput,array($productId)));
			if (empty($boolResult)) {
				return $this->getErrorMsg(C("ERRNO.ERR_UPDATE_PRODUCT_FAIL"),C("ERRNO.ERR_UPDATE_PRODUCT_FAIL_DESC"));
			}
			return $boolResult;
		}

		/**
		*@desc 按仓库id来获取商品列表
		*@param depot id
		*@return product infomation
		*/
		public function getProductListByDepotId($depotId) {
			$strSql = "SELECT * FROM {$this->productTableName} WHERE depot_id=%d";
			$arrResult = $this->query($strSql,$depotId);
			if (empty($arrResult)) {
				return array();
			}
			return $arrResult;
		}

		/**
		*@desc 按商户来获取商品列表
		*@param seller id
		*@return product infomation
		*/
		public function getProductListBySellerId($sellerId) {
			$strSql = "SELECT * FROM {$this->productTableName} WHERE seller_id=%d";
			$arrResult = $this->query($strSql,$sellerId);
			if (empty($arrResult)) {
				return array();
			}
			return $arrResult;
		}

		/**
		*@desc 根据商品类别和仓库来筛选商品
		*@param 类别id，仓库id
		*@return product infomation
		*/
		public function getProductByTypeId($typeId,$depotId=null) {
			$strSql = "SELECT * FROM {$this->productTableName} WHERE type_id=%d";
			if (empty($depotId)) {
				$arrResult = $this->query($strSql,array($typeId));
			} else {
				$strSql .= " AND depot_id=%d";
				$arrResult = $this->query($strSql,array($typeId,$depotId));
			}
			if (empty($arrResult)) {
				return array();
			}
			return $arrResult[0];
		}

		/**
		*@desc 获取商品类型列表
		*@param seller id
		*@return product infomation
		*/
		public function getProductTypeList() {
			$strSql = "SELECT * FROM {$this->productTypeTable}";
			$arrResult = $this->query($strSql);
			if (empty($arrResult)) {
				return array();
			}
			return $arrResult;
		}

		/**
		*@desc 添加商品类型
		*@param $name,$icon_url,$pic_url
		*@return result
		*/
		public function addProductType($name,$iconUrl,$picUrl) {
			if (empty($name) || empty($iconUrl) || empty($picUrl)) {
				return $this->getErrorMsg(C("ERRNO.ERROR_PARAM_ERR"),C("ERRNO.ERROR_PARAM_ERR_DESC"));
			}
			$strSql = "INSERT INTO {$this->productTypeTable}(name,icon_url,pic_url) VALUES(%s,%s,%s)";
			$boolResult = $this->execute($strSql,array($name,$iconUrl,$picUrl));
			if (empty($boolResult)) {
				return $this->getErrorMsg(C("ERRNO.ERR_ADD_PRODUCT_TYPE_FAIL"),C("ERRNO.ERR_ADD_PRODUCT_TYPE_FAIL_DESC"));
			}
			return $boolResult;
		}

		/**
		*@desc 更新商品类型
		*@param id，$name,$icon_url,$pic_url
		*@return result
		*/
		public function updateProductType($productTypeId,$name=null,$iconUrl=null,$picUrl=null) {
			if (empty($name) && empty($iconUrl) && empty($picUrl)) {
				return false;
			}

			$strSql = "UPDATE {$this->productTypeTable} SET ";
			$fieldsArr = array();
			$valuesArr = array();
			if (!empty($name)) {
				$fieldsArr[] = "name=%s";
				$valuesArr[] = $name;
			}

			if (!empty($iconUrl)) {
				$fieldsArr[] = "icon_url=%s";
				$valuesArr[] = $iconUrl;
			}

			if (!empty($picUrl)) {
				$fieldsArr[] = "pic_url=%s";
				$valuesArr[] = $picUrl;
			}

			$valuesArr[] = $productTypeId;

			$strSql .= join(",",$fieldsArr);
			$strSql .= " WHERE id=%d";

			$boolResult = $this->execute($strSql,$valuesArr);
			if (empty($boolResult)) {
				return $this->getErrorMsg(C("ERRNO.ERR_UPDATE_PRODUCT_TYPE_FAIL"),C("ERRNO.ERR_UPDATE_PRODUCT_TYPE_FAIL_DESC"));
			}
			return $boolResult;
		}

		/**
		*@desc 仓库添加商品
		*@param 商品信息
		*@param name
		*@param info
		*@param type_id
		*@param cost
		*@param price
		*@param price_market
		*@param price_sale
		*@param supplier
		*@param pic_url
		*@param cur_amount
		*@param total_amount
		*@param depot_id
		*@param recommand_amount
		*@return result
		*/
		public function depotAddProduct($arrInput) {
			if (empty($arrInput["name"]) || empty($arrInput["info"]) || empty($arrInput["type_id"])
				|| empty($arrInput["cost"]) || empty($arrInput["price"]) || empty($arrInput["price_market"])
				|| empty($arrInput["price_sale"]) || empty($arrInput["supplier"]) || empty($arrInput["pic_url"])
				|| empty($arrInput["cur_amount"]) || empty($arrInput["total_amount"]) || empty($arrInput["depot_id"])
				|| empty($arrInput["recommand_amount"])) {
				return $this->getErrorMsg(C("ERRNO.ERROR_PARAM_ERR"),C("ERRNO.ERROR_PARAM_ERR_DESC"));
			}

			$fields = "name,info,type_id,cost,price,price_market,price_sale,supplier,pic_url,cur_amount,total_amount,depot_id,recommand_amount";
			$valuesStr = "%s,%s,%d,%d,%d,%d,%d,%s,%s,%d,%d,%d,%d";
			$valuesArr = array(
				$arrInput["name"],
				$arrInput["info"],
				$arrInput["type_id"],
				$arrInput["cost"],
				$arrInput["price"],
				$arrInput["price_market"],
				$arrInput["price_sale"],
				$arrInput["supplier"],
				$arrInput["pic_url"],
				$arrInput["cur_amount"],
				$arrInput["total_amount"],
				$arrInput["depot_id"],
				$arrInput["recommand_amount"],
			);
			$strSql = "INSERT INTO {$this->productTableName}({$fields}) VALUES({$valuesStr})";
			$boolResult = $this->execute($strSql,$valuesArr);
			if (empty($boolResult)) {
				return $this->getErrorMsg(C("ERRNO.ERR_ADD_PRODUCT_FAIL"),C("ERRNO.ERR_ADD_PRODUCT_FAIL_DESC"));
			}
			return $boolResult;
		}

		/**
		*@desc 商户添加商品
		*		传入仓库里的商品id，自动获取商品信息，关联商户id
		*		会在表里新加一条记录 默认on_sale为0
		*@param 商品id，seller id
		*@return result
		*/
		public function sellerAddProduct($productId,$sellerId) {
			if (empty($productId) || empty($sellerId)) {
				return $this->getErrorMsg(C("ERRNO.ERROR_PARAM_ERR"),C("ERRNO.ERROR_PARAM_ERR_DESC"));
			}
			$productInfo = $this->getProductById($productId);
			if (empty($productInfo)) {
				return $this->getErrorMsg(C("ERRNO.ERR_NO_SUCH_PRODUCT"),C("ERRNO.ERR_NO_SUCH_PRODUCT_DESC"));	
			}
			$fields = "name,info,type_id,cost,price,price_market,price_sale,supplier,pic_url,cur_amount,total_amount,depot_id,recommand_amount,on_sale,product_id,seller_id";
			$valuesStr = "%s,%s,%d,%d,%d,%d,%d,%s,%s,%d,%d,%d,%d,0,%d,%d";
			$valuesArr = array(
				$productInfo["name"],
				$productInfo["info"],
				$productInfo["type_id"],
				$productInfo["cost"],
				$productInfo["price"],
				$$productInfo["price_market"],
				$productInfo["price_sale"],
				$productInfo["supplier"],
				$productInfo["pic_url"],
				$productInfo["cur_amount"],
				$productInfo["total_amount"],
				$productInfo["depot_id"],
				$productInfo["recommand_amount"],
				$productId,
				$sellerId,
			);
			$strSql = "INSERT INTO {$this->productTableName}({$fields}) VALUES({$valuesStr})";
			$boolResult = $this->execute($strSql,$valuesArr);
			if (empty($boolResult)) {
				return $this->getErrorMsg(C("ERRNO.ERR_ADD_PRODUCT_FAIL"),C("ERRNO.ERR_ADD_PRODUCT_FAIL_DESC"));
			}
			return $boolResult;
		}
	}