<?php
	namespace Store\Model;
	use Think\Model;

	/*
	*订单信息model
	*/
	class OrderModel extends BaseModel {
		protected $orderTableName = "order";

		/**
		*@desc 建立订单
		*@param orderinfo
		*@return result
		*/

		public function createOrder($arrInput) {
			if (empty($arrInput)) {
				return $this->getErrorMsg(C("ERROR_ORDER_ERR"),C("ERROR_ORDER_ERR_DESC"));
			}

			$fields = "order_custom_id,goods,seller_id,price,refund,payment,create_time,update_time,status";
			$valuesStr = "%d,%d,%d,%d,%d,%d,%d,%d,%d";
			$valuesArr = array(
				$inputarray["order_custom_id"],
				$inputarray["goods"],
				$inputarray["seller_id"],
				$inputarray["price"],
				$inputarray["refund"],
				$inputarray["payment"],
				$inputarray["create_time"],
				$inputarray["update_time"],
				$inputarray["status"],
			);

			$strSql = "INSERT INTO {$this->orderTableName}({$fields}) VALUES({$valuesStr})";
			$boolResult = $this->execute($strSql,$valuesArr);
			if (empty($boolResult)) {
				return $this->getErrorMsg(C("ERRNO.ERR_ADD_ORDER_FAIL"),C("ERRNO.ERR_ADD_ORDER_FAIL_DESC"));
			}
			return $boolResult;
		}

		/**
		*@desc 用户未收货订单列表
		*@param custom_id
		*@return resultArray
		*/

		public function getUnsolvedOrderList($order_custom_id){
			$strSql = "SELECT * FROM {$this->orderTableName} WHERE order_custom_id=%d AND status IN (1,2)";
			$arrResult = $this->query($strSql,$order_custom_id);
			if (empty($arrResult)) {
				return array();
			}
			return $arrResult;
		}

		/**
		*@desc 用户所有订单列表
		*@param custom_id
		*@return resultArray
		*/

		public function getAllOrderList($order_custom_id){
			$strSql = "SELECT * FROM {$this->orderTableName} WHERE order_custom_id=%d ";
			$arrResult = $this->query($strSql,$order_custom_id);
			if (empty($arrResult)) {
				return array();
			}
			return $arrResult;
		}

		/**
		*@desc 用户正在配送订单列表
		*@param custom_id
		*@return resultArray
		*/

		public function getDeliveringOrderList($order_custom_id){
			$strSql = "SELECT * FROM {$this->orderTableName} WHERE order_custom_id=%d AND status=1";
			$arrResult = $this->query($strSql,$order_custom_id);
			if (empty($arrResult)) {
				return array();
			}
			return $arrResult;
		}

		/**
		*@desc 用户确认收货
		*@param order_id
		*@return result
		*/

		public function confirmOrder($order_id,$custom_id){
			$valuesArr = array(
				$custom_id,
				$order_id,
				)
			$strSql = "SELECT * FROM {$this->orderTableName} WHERE order_custom_id=%d AND id=%d"
			$arrResult = $this->execute($strSql,$valuesArr);
			if (empty($arrResult)) {
				return $this->getErrorMsg(C("ERRNO.ERROR_CUSTOM_COMPARE_FAIL"),C("ERRNO.ERROR_CUSTOM_COMPARE_FAIL_DESC"));
			}
			$result = mysql_fetch_array($arrResult);
			if(!(result[status] == 2 || result[status] == 5)){
				return $this->getErrorMsg(C("ERRNO.ERROR_CHECK_PAID_FAIL"),C("ERRNO.ERROR_CHECK_PAID_FAIL_DESC"));
			}
			$strSql = "UPDATE {$this->productTypeTable} SET status=3 WHERE id=%d";
			$boolResult = $this->execute($strSql,$order_id);
			if (empty($boolResult)) {
				return $this->getErrorMsg(C("ERRNO.ERROR_CONFIRM_ORDER_FAIL"),C("ERRNO.ERROR_CONFIRM_ORDER_FAIL_DESC"));
			}
			return $boolResult;
		}

		/**
		*@desc 楼主开始派送
		*@param order_id
		*@return result
		*/

		public function beginDeliverOrder($order_id){
			$strSql = "SELECT * FROM {$this->orderTableName} WHERE id=%d"
			$arrResult = $this->execute($strSql,$order_id);
			$result = mysql_fetch_array($arrResult);
			if($result['status'] != 5){
				return $this->getErrorMsg(C("ERRNO.ERROR_CHECK_PAID_FAIL"),C("ERRNO.ERROR_CHECK_PAID_FAIL_DESC"));
			}
			$strSql = "UPDATE {$this->productTypeTable} SET status=2 WHERE id=%d";
			$boolResult = $this->execute($strSql,$order_id);
			if (empty($boolResult)) {
				return $this->getErrorMsg(C("ERRNO.ERROR_CHANGE_DELIVER_ORDER_FAIL"),C("ERRNO.ERROR_CHANGE_DELIVER_ORDER_FAIL_DESC"));
			}
			return $boolResult;
		}
	}