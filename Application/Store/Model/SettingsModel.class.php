<?php
	namespace Store\Model;
	use Think\Model;

	/*
	*配置表model
	*/
	class SettingsModel extends BaseModel {
		protected $settingsTableName = "settings";

		/**
		*@desc 插入字段到系统配置表
		*@param input_key,input_value
		*@return 操作结果
		**/
		
		public function insersettings($input_key,$input_value){
			if (empty($input_key) || empty($input_value)) {
				return $this->getErrorMsg(C("ERRNO.ERROR_PARAM_ERR"),C("ERRNO.ERROR_PARAM_ERR_DESC"));
			}
			$strsql = 'INSERT INTO settings VALUES (' . 'input_key' . ',' . 'input_value' . ');';
			$boolResult = $this->query($strsql);
			if (empty($boolResult)) {
				return $this->getErrorMsg(C("ERROR_INSERT_SETTINGS_ERR"),C("ERROR_INSERT_SETTINGS_ERR_DESC"));
			}
			return $boolResult;
		}

		/**
		*@desc 通过key查询系统配置表
		*@param key
		*@return settingsinfo
		**/

		public function getsettingsbykey($input_key){
			if (empty($input_key)) {
				return $this->getErrorMsg(C("ERRNO.ERROR_PARAM_ERR"),C("ERRNO.ERROR_PARAM_ERR_DESC"));
			}
			$strsql = 'SELECT * FROM settings WHERE key=' . '$input_key;';
			$arrResult = $this->query($strsql);
			if (empty($arrResult)) {
				return $this->getErrorMsg(C("ERROR_SELECT_SETTINGS_ERR"),C("ERROR_SELECT_SETTINGS_ERR_DESC"));
			}
			return $arrResult;
		}