<?php
	namespace Store\Model;
	use Think\Model;

	/*
	*商品信息model
	*/
	class SellerModel extends BaseModel {
		protected $dormitoryTableName = "dormitory";
		protected $managerTableName = "manager";
		protected $goodsTableName = "goods";
		
		/**
		*@desc 查询一所学校所有宿舍楼的列表
		*@param school_id
		*@return dormitore_list
		**/
		
		public function getdormitorylist($schoolId) {
			$strsql = "SELECT * FROM {$This->dormitoryTableName} WHERE id = " .$schoolId;
			$arrResult = $this->query($strSql);
			if (empty($arrResult)) {
				return array();
			}
			return $arrResult;
		}
		
		/**
		*@desc 查询一个宿舍楼所有上架商品的列表
		*@param sellerId
		*@return goods_list
		**/
		
		public function getgoodslist($sellerId) {
			$strsql = "SELECT name, type_id FROM {$This->goodsTableName} GROUP BY type_id WHERE id = " .$sellerId;
			$arrResult = $this->query($strSql);
			if (empty($arrResult)) {
				return array();
			}
			return $arrResult;
		}
		
		
	}