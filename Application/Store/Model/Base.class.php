<?php
	namespace Store\Model;
	use Think\Model;

	class BaseModel extends Model {
		public function getErrorMsg($errCode=0,$errMsg="") {
			return array("errNo" => $errCode,"errMsg" => $errMsg);
		}
	}