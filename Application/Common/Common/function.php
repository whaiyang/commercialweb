<?php

	function getSafeValue($input)
	{
		if(is_array($input))
		{
			foreach($input as &$value)
			{
				$value = getSafeValue($value);
			}
			return $input;
		}
		else
		{
			return get_magic_quotes_gpc()?htmlspecialchars($input):htmlspecialchars(addslashes($input));
		}
	}

	function getOutputValue($input) 
	{
		if(is_array($input))
		{
			foreach($input as &$value)
			{
				$value = getOutputValue($value);
			}
			return $input;
		}
		else
		{
			return get_magic_quotes_gpc()?$input:stripslashes($input);
		}
	}

	/**
    *@param 待加密字段
    *@return 加密结果
    */
    function simpleEncrypt($strValue) {
        $strResult = base64_encode($strValue);
        $strResult = strrev($strResult);
        return $strResult;
    }

    /**
    *@param 待解密字段
    *@return 解密结果
    */
    function simpleDecrypt($strValue) {
        $strResult = strrev($strValue);
        $strResult = base64_decode($strResult);
        return $strResult;
    }

    function encrypt($data, $key) { 
    	$key    =   md5($key); 
    	$x      =   0; 
    	$len    =   strlen($data); 
    	$l      =   strlen($key); 
    	for ($i = 0; $i < $len; $i++)  { 
        	if ($x == $l)  
        	{ 
        	    $x = 0; 
        	} 
        	$char .= $key{$x}; 
        	$x++; 
    	} 
    	for ($i = 0; $i < $len; $i++) { 
        	$str .= chr(ord($data{$i}) + (ord($char{$i})) % 256); 
    	} 
    	return base64_encode($str); 
	}

	function decrypt($data, $key) 
	{ 
    	$key = md5($key); 
    	$x = 0; 
    	$data = base64_decode($data); 
    	$len = strlen($data); 
    	$l = strlen($key); 
    	for ($i = 0; $i < $len; $i++) { 
        	if ($x == $l)  
        	{ 
        	    $x = 0; 
        	} 
        	$char .= substr($key, $x, 1); 
        	$x++; 
    	} 
    	for ($i = 0; $i < $len; $i++) { 
        	if (ord(substr($data, $i, 1)) < ord(substr($char, $i, 1))) 
        	{ 
            	$str .= chr((ord(substr($data, $i, 1)) + 256) - ord(substr($char, $i, 1))); 
        	} 
        	else 
        	{ 
            	$str .= chr(ord(substr($data, $i, 1)) - ord(substr($char, $i, 1))); 
        	} 
    	} 
    	return $str; 
	}

	/*$reqArr = array("<script>alert(1)</script>",array("<h></h>","<a></a>"),array("'123'",'"',"\\"));
	$reqArr = getSafeValue($reqArr);
	var_dump($reqArr);
	$reqArr = getOutputValue($reqArr);
	var_dump($reqArr);*/