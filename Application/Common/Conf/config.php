<?php
return array(
	//'配置项'=>'配置值'
	//数据库配置信息
	'DB_TYPE'   => 'mysql', // 数据库类型
	'DB_HOST'   => '118.123.240.96', // 服务器地址
	'DB_NAME'   => 'panda', // 数据库名
	'DB_USER'   => 'why', // 用户名
	'DB_PWD'    => 'why#commercial', // 密码
	'DB_PORT'   => 3306, // 端口
	'DB_PREFIX' => 'commercial_', // 数据库表前缀 
	'DB_CHARSET'=> 'utf8', // 字符集
	'DB_DEBUG'  =>  TRUE, // 数据库调试模式 开启后可以记录SQL日志 3.2.3新增
	'DEFAULT_MODULE' => 'Store',
	'DEFAULT_CONTROLLER' => 'Showpage',

	/*
	*Cookie Session 相关配置
	*/
	'COOKIE_EXPIRE_TIME' => 7*24*3600,//Cookie过期时间
	'MANAGER_USER_INFO_COOKIE' => "MANAGER_USER_INFO_COOKIE",//管理员Cookie名
	'MANAGER_USER_INFO_SESSION' => "MANAGER_USER_INFO_SESSION",//管理员Session名
	'CUSTOM_USER_INFO_COOKIE' => "CUSTOM_USER_INFO_COOKIE",//管理员Cookie名
	'CUSTOM_USER_INFO_SESSION' => "CUSTOM_USER_INFO_SESSION",//管理员Session名


	//邮件配置
	'THINK_EMAIL' => array(
	'SMTP_HOST'   => '118.123.240.96', //SMTP服务器
    'SMTP_PORT'   => '25', //SMTP服务器端口
    'SMTP_USER'   => 'mailbox', //SMTP服务器用户名
    'SMTP_PASS'   => 'mailbox#commercial', //SMTP服务器密码
    'FROM_EMAIL'  => '449607843@qq.com', //发件人EMAIL
    'FROM_NAME'   => 'Peter', //发件人名称
    'REPLY_EMAIL' => '', //回复EMAIL（留空则为发件人EMAIL）
    'REPLY_NAME'  => '', //回复名称（留空则为发件人名称）
 ),




	/*
	*其他配置
	*/
	'LOAD_EXT_CONFIG' => array(
		"ERRNO" => "errno",//错误码
		"STATUS" => "status",//状态码
		"ALIPAY" => "alipay",//支付宝配置
		"DEPOT" => "depot",//仓库配置
		"CITY" => "city",//城市列表 
	),

	'TMPL_PARSE_STRING' => array(
		"__UPLOAD__" => __ROOT__."/Upload",	
	),

	'__UPLOAD__' => "Upload/",

	'LOG_RECORD' => true, // 开启日志记录
);
