<?php
	return array(
		"partner" => "111",//合作者身份id
		"seller_email" => "222",//收款账号
		"key" => "333",//安全检验码
		"sign_type" => "MD5",//签名方式
		"input_charset" => "utf-8",//字符编码格式
		"cacert" => "cacert.pem",//CA证书名，用curl中ssh校验
		"transport" => "http",//访问模式
		"notify_url" => "Store/Pay/notify",//顾客下单，异步通知url
		"return_url" => "Store/Pay/callback",//顾客下单，通知页面url
		"seller_notify_url" => "Seller/Pay/notify",//商户充值，异步通知url
		"seller_return_url" => "Seller/Pay/callback",//商户充值，通知页面url
		"seller_refund_notify_url" => "Seller/Pay/refundNotify",//退款，异步通知url
		"seller_refund_return_url" => "Seller/Pay/refundCallback",//退款，通知页面url
	);