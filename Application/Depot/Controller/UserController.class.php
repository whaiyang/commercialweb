<?php
namespace Depot\Controller;
use Think\Controller;
class UserController extends BaseController {

	public function _initialize() {
		parent::_initialize();
		$this->_userModel = D("User/ManagerUser");
	}


	public function checkLogin() {
		$isLogin = $this->_userModel->checkLogin();
		if ($isLogin) {
			$this->getMsg(0,"已登陆",1);	
		} else {
		    $this->getMsg(0,"未登陆",0);
		}
	}

	public function login() {
		$arrInput = $_GET;
		$boolResult = $this->_userModel->login($arrInput);
		if ($boolResult == true) {
			$this->getMsg(0,"登陆成功",1);
		} else {
			$this->getMsg(0,"登陆失败",0);
		}
	}

	public function logout() {
		$arrInput = $_GET;
		$boolResult = $this->_userModel->logout($arrInput);
		if ($boolResult == true) {
			 $this->getMsg(0,"退出成功",1);
		} else {
			$this->getMsg(0,"退出失败",0);
		}
	}

	public function register() {
		$arrInput = $_GET;
		$arrResult = $this->_userModel->register($arrInput);
		if (empty($arrResult['errNo'])) {
			$this->getMsg(0,"注册成功",1);
			return;
		}
		echo json_encode($arrResult);
	}

	public function userList() {
		$arrResult = $this->_userModel->getUserList();
		return $this->getMsg(0,"",$arrResult);	
	}
}
