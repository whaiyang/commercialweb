<?php
namespace Depot\Controller;
use Think\Controller;
class ProductController extends BaseController {

	public function _initialize() {
		parent::_initialize();
		$this->_productModel = D("Product");
	}

	//主仓库添加商品,POST
	public function primaryDepotAdd() {
		if (!$this->_is_primary_depot) {
			return $this->getMsg(1,"主仓库才有此操作权限");
		}
		$this->_arrInputPost["depot_id"] = $this->_depot_id;
		$mixResult = $this->_productModel->depotAddProduct($this->_arrInputPost);
		if (empty($mixResult['errNo'])) {
			return $this->getMsg(0,"添加成功",1);
		}
		echo json_encode($mixResult);
	}

	//分仓库添加商品,商品id逗号分隔
	public function add() {
		if ($this->_is_primary_depot) {
			return $this->getMsg(1,"分仓库才有此操作权限");
		}
		$arrParam = array(
			"depot_id" => $this->_depot_id,
			"goodsIdArr" => empty($this->_arrInputGet["goods_id"]) ? null : explode(",",$this->_arrInputGet["goods_id"]),
		);
		$mixResult = $this->_productModel->minorDepotAddProduct($arrParam);
		if (empty($mixResult['errNo'])) {
			return $this->getMsg(0,"添加成功",1);
		}
		echo json_encode($mixResult);
	}

	//商品列表
	public function productList() {
		$arrList = $this->_productModel->getProductListByDepotId($this->_depot_id);
		if (empty($arrList)) {
			$arrList = array();
		}
		return $this->getMsg(0,"",$arrList);
	}

	//获取主仓库的商品列表,用于分仓库添加商品，去掉了重复的部分
	public function primaryDepotProductList() {
		if ($this->_is_primary_depot) {
			return $this->getMsg(1,"分仓库才有此操作权限");
		}
		$minorDepotGoods = $this->_productModel->where("depot_id=%d",$this->_depot_id)->field(array("name"))->select();
		$nameArr = array();
		foreach ($minorDepotGoods as $arr) {
			$nameArr[] = $arr["name"];
		}
		$resultGoodsInfo = $this->_productModel->productDepotMatch($nameArr,$this->_primary_depot_id);
		if ($resultGoodsInfo === false) {
			return $this->getMsg(1,"获取商品列表失败");	
		}
		return $this->getMsg(0,"",$resultGoodsInfo);
	}
	
	//主仓库更新商品信息
	public function primaryDepotUpdate() {
	
	}

	//删除商品
	public function delete() {
		$boolResult = $this->_productModel->where("depot_id=%d AND id=%d",$this->_depot_id,$this->_arrInputGet["product_id"])->delete();
		if (!$boolResult) {
			return $this->getMsg(1,"删除商品失败");
		}
		return $this->getMsg(0,"删除商品成功",1);
	}

	//分仓库同步主仓库数据
	public function synchroData() {
	
	}

	//由id获取商品信息
	public function getProduct() {
		$goodInfo = $this->_productModel->where("depot_id=%d AND id=%d",$this->_depot_id,$this->_arrInputGet["product_id"])->find();
		if (empty($goodInfo)) {
			return $this->getMsg(1,"获取商品信息失败");
		}
		return $this->getMsg(0,"",$goodInfo);
	}

	//添加商品种类
	public function addProductType() {
		$arrResult = $this->_productModel->addProductType(
			$this->_arrInputGet["name"],
			$this->_arrInputGet["icon_url"],
			$this->_arrInputGet["pic_url"]
		);
		if (empty($arrResult['errNo'])) {
			return $this->getMsg(0,"添加成功",1);
		}
		echo json_encode($arrResult);
	}

	//更新商品种类
	public function updateProductType() {
		$arrResult = $this->_productModel->updateProductType(
			$this->_arrInputGet["type_id"],
			$this->_arrInputGet["name"],
			$this->_arrInputGet["icon_url"],
			$this->_arrInputGet["pic_url"]
		);
		if (empty($arrResult['errNo'])) {
			return $this->getMsg(0,"更新成功",1);
		}
		echo json_encode($arrResult);
	}

	//商品种类列表
	public function productTypeList() {
		$arrResult = $this->_productModel->getProductTypeList();
		return $this->getMsg(0,"",$arrResult);
	}
}
