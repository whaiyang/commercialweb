<?php
namespace Depot\Controller;
use Think\Controller;
class DamageController extends BaseController {

	public function _initialize() {
		parent::_initialize();
		$this->_damageRecordModel = D("DepotDamageRecord");
		$this->_productModel = D("Product");
	}

	public function add() {
		if ($this->_is_primary_depot) {
			return $this->getMsg(1,"分仓库才能进行此操作");
		}
		if (empty($this->_arrInputGet["goodsId"]) || empty($this->_arrInputGet["goodsCount"])
			|| empty($this->_arrInputGet["reason"])) {
			return $this->getMsg(C("ERRNO.ERROR_PARAM_ERR"),C("ERRNO.ERROR_PARAM_ERR_DESC"));
		}
		$goodsId = explode(",", $this->_arrInputGet["goodsId"]);
		$countArr = explode(",", $this->_arrInputGet["goodsCount"]);
		if (count($goodsId) != count($countArr)) {
			return $this->getMsg(C("ERRNO.ERROR_PARAM_ERR"),"报损数量有误");
		}
		$arrResult = $this->_damageRecordModel->addDamageRecord($this->_depot_id,$this->_primary_depot_id,
			$this->_arrInputGet["reason"],
			$goodsId,
			$countArr
		);
		if (empty($arrResult['errNo'])) {
			$this->getMsg(0,"创建报损申请成功",1);
			return;
		}
		echo json_encode($arrResult);
	}

	public function checkDamageRecord() {
		if (!$this->_is_primary_depot) {
			return $this->getMsg(1,"主仓库才能进行此操作");
		}
		$arrResult = $this->_damageRecordModel->checkDamageRecord($this->_arrInputGet["order_id"],
			$this->_arrInputGet["pass"] == 1 ? true : false);
		if (empty($arrResult['errNo'])) {
			$this->getMsg(0,"审核成功",1);
			return;
		}
		echo json_encode($arrResult);
	}

	public function damageRecordList() {
		if ($this->_is_primary_depot) {
			return $this->getMsg(1,"分仓库才能进行此操作");
		}
		if (empty($this->_arrInputGet['status'])) {
			$arrList = $this->_damageRecordModel->getDamageRecordList($this->_depot_id,false,$this->_arrInputGet["start_time"],
				$this->_arrInputGet["end_time"]);
		} else {
			$arrList = $this->_damageRecordModel->getDamageRecordList($this->_depot_id,false,$this->_arrInputGet["start_time"],
				$this->_arrInputGet["end_time"],$this->_arrInputGet["status"]);
		}
		if (empty($arrList['errNo'])) {
			$this->getMsg(0,"",$arrList);
			return;
		}
		echo json_encode($arrList);
	}

	public function damageRecordListToCheck() {
		if (!$this->_is_primary_depot) {
			return $this->getMsg(1,"主仓库才能进行此操作");
		}
		if (empty($this->_arrInputGet['status'])) {
			$arrList = $this->_damageRecordModel->getDamageRecordList(false,$this->_depot_id,$this->_arrInputGet["start_time"],
				$this->_arrInputGet["end_time"]);
		} else {
			$arrList = $this->_damageRecordModel->getDamageRecordList(false,$this->_depot_id,$this->_arrInputGet["start_time"],
				$this->_arrInputGet["end_time"],$this->_arrInputGet["status"]);
		}
		if (empty($arrList['errNo'])) {
			$this->getMsg(0,"",$arrList);
			return;
		}
		echo json_encode($arrList);
	}
}