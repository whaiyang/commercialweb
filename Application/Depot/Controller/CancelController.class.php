<?php
namespace Depot\Controller;
use Think\Controller;
class CancelController extends BaseController {

	public function _initialize() {
		parent::_initialize();
		$this->_orderRecordModel = D("DepotOrderRefund");
		$this->_productModel = D("Product");
		$this->_depotModel = D("Depots");
	}

	public function addRefundApplication() {
		if (empty($this->_arrInputGet["goodsId"]) || empty($this->_arrInputGet["goodsCount"]) 
			|| empty($this->_arrInputGet["reason"])) {
			return $this->getMsg(C("ERRNO.ERROR_PARAM_ERR"),C("ERRNO.ERROR_PARAM_ERR_DESC"));
		}
		if (!$this->_is_primary_depot) {
			//分仓
			$goodsId = explode(",", $this->_arrInputGet["goodsId"]);
			$countArr = explode(",", $this->_arrInputGet["goodsCount"]);
			$strReason = $this->_arrInputGet["reason"];
			if (count($goodsId) != count($countArr)) {
				return $this->getMsg(C("ERRNO.ERROR_PARAM_ERR"),"退货数量有误");
			}
			$minorDepotGoods = $this->_productModel->where(array(
				"depot_id" => $this->_depot_id,
				"id" => array("in",join(",",$goodsId)),
			))->field(array("name"))->select();
			$nameArr = array();
			if (!empty($minorDepotGoods)) {
				foreach ($minorDepotGoods as $strName) {
					$nameArr[] = $strName["name"];
				}
			}
			$resultGoodsInfo = $this->_productModel->productDepotMatch($nameArr,$this->_primary_depot_id);
			if (!empty($resultGoodsInfo)) {
				return $this->getMsg(0,"存在主仓没有的商品",$resultGoodsInfo);
			}
			$goodsIdTo = $this->_productModel->productDepotMatchIn($nameArr,$this->_primary_depot_id);
			$arrResult = $this->_orderRecordModel->addOrderRefund($this->_primary_depot_id,$this->_depot_id,
				$goodsIdTo,
				$goodsId,
				$countArr,
				$strReason);
			if (empty($arrResult['errNo'])) {
				$this->getMsg(0,"创建退货申请成功",1);
				return;
			}
			echo json_encode($arrResult);
		} else {
			//总仓
			$goodsId = explode(",", $this->_arrInputGet["goodsId"]);
			$countArr = explode(",", $this->_arrInputGet["goodsCount"]);
			$strReason = $this->_arrInputGet["reason"];
			if (count($goodsId) != count($countArr)) {
				return $this->getMsg(C("ERRNO.ERROR_PARAM_ERR"),"退货数量有误");
			}
			$arrResult = $this->_orderRecordModel->addPrimaryDepotOrderRefund($this->_depot_id,
				$goodsId,
				$countArr,
				$strReason);
			if (empty($arrResult['errNo'])) {
				$this->getMsg(0,"退货成功",1);
				return;
			}
			echo json_encode($arrResult);
		}
	}

	public function checkRefundApplication() {
		if (!$this->_is_primary_depot) {
			return $this->getMsg(1,"主仓库才能进行此操作");
		}
		$arrResult = $this->_orderRecordModel->checkOrderRefund($this->_arrInputGet["order_id"],
			$this->_arrInputGet["pass"] == 1 ? true : false);
		if (empty($arrResult['errNo'])) {
			$this->getMsg(0,"审核成功",1);
			return;
		}
		echo json_encode($arrResult);
	}

	//分仓的退货申请
	public function getRefundList() {
		if ($this->_is_primary_depot) {
			return $this->getMsg(1,"分仓库才能进行此操作");
		}
		if (empty($this->_arrInputGet['status'])) {
			$arrList = $this->_orderRecordModel->getRefundOrderList($this->_depot_id,false,$this->_arrInputGet["start_time"],
					$this->_arrInputGet["end_time"]);
		} else {
			$arrList = $this->_orderRecordModel->getRefundOrderList($this->_depot_id,false,$this->_arrInputGet["start_time"],
					$this->_arrInputGet["end_time"],$this->_arrInputGet['status']);
		}
		if (empty($arrList['errNo'])) {
			$this->getMsg(0,"",$arrList);
			return;
		}
		echo json_encode($arrList);
	}

	//主仓获取分仓提交的退货申请
	public function getRefundListToCheck() {
		if (!$this->_is_primary_depot) {
			return $this->getMsg(1,"主仓库才有此操作权限");
		}
		if (empty($this->_arrInputGet['status'])) {
			$arrList = $this->_orderRecordModel->getRefundOrderList(false,$this->_depot_id,$this->_arrInputGet["start_time"],
					$this->_arrInputGet["end_time"]);
		} else {
			$arrList = $this->_orderRecordModel->getRefundOrderList(false,$this->_depot_id,$this->_arrInputGet["start_time"],
					$this->_arrInputGet["end_time"],$this->_arrInputGet['status']);
		}
		if (empty($arrList['errNo'])) {
			$this->getMsg(0,"",$arrList);
			return;
		}
		echo json_encode($arrList);
	}
}