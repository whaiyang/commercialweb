<?php
namespace Depot\Controller;
use Think\Controller;
class InputController extends BaseController {

	public function _initialize() {
		parent::_initialize();
		$this->_orderRecordModel = D("DepotOrderRecord");
		$this->_productModel = D("Product");
	}

	//主仓库生成进货单
	public function primaryDepotAdd() {
		if (!$this->_is_primary_depot) {
			return $this->getMsg(1,"主仓库才有此操作权限");
		}
		$arrResult = $this->_orderRecordModel->primaryDepotPurchase($this->_depot_id,
			explode(",", $this->_arrInputGet["goodsId"]),
			explode(",", $this->_arrInputGet["goodsCount"]));
		if (empty($arrResult['errNo'])) {
			$this->getMsg(0,"创建进货单成功",1);
			return;
		}
		echo json_encode($arrResult);
	}

	//分仓库提交进货申请
	public function addApplication() {
		if ($this->_is_primary_depot) {
			return $this->getMsg(1,"分仓库才能进行此操作");
		}
		if (empty($this->_arrInputGet["goodsId"]) || empty($this->_arrInputGet["goodsCount"])) {
			return $this->getMsg(C("ERRNO.ERROR_PARAM_ERR"),C("ERRNO.ERROR_PARAM_ERR_DESC"));
		}
		$goodsId = explode(",", $this->_arrInputGet["goodsId"]);
		$countArr = explode(",", $this->_arrInputGet["goodsCount"]);
		if (count($goodsId) != count($countArr)) {
			return $this->getMsg(C("ERRNO.ERROR_PARAM_ERR"),"进货数量有误");
		}
		$minorDepotGoods = $this->_productModel->where(array(
			"depot_id" => $this->_depot_id,
			"id" => array("in",join(",",$goodsId)),
		))->field(array("name"))->select();
		$nameArr = array();
		if (!empty($minorDepotGoods)) {
			foreach ($minorDepotGoods as $strName) {
				$nameArr[] = $strName["name"];
			}
		}
		$resultGoodsInfo = $this->_productModel->productDepotCheck($nameArr,$this->_primary_depot_id);
		if (!empty($resultGoodsInfo)) {
			return $this->getMsg(0,"存在主仓没有的商品",$resultGoodsInfo);
		}
		$goodsIdFrom = $this->_productModel->productDepotMatchIn($nameArr,$this->_primary_depot_id);
		$arrResult = $this->_orderRecordModel->createPurchaseOrder($this->_depot_id,$this->_primary_depot_id,
			$goodsId,
			$goodsIdFrom,
			$countArr);
		if (empty($arrResult['errNo'])) {
			$this->getMsg(0,"创建进货申请成功",1);
			return;
		}
		echo json_encode($arrResult);
	}

	//主仓库审批进货申请
	public function checkOrder() {
		if (!$this->_is_primary_depot) {
			return $this->getMsg(1,"主仓库才有此操作权限");
		}
		$arrResult = $this->_orderRecordModel->checkPurchaseOrder($this->_arrInputGet["order_id"],
			$this->_arrInputGet["pass"] == 1 ? true : false);
		if (empty($arrResult['errNo'])) {
			$this->getMsg(0,"审核成功",1);
			return;
		}
		echo json_encode($arrResult);
	}

	//获取进货单
	public function purchaseList() {
		if (empty($this->_arrInputGet['status'])) {
			$arrList = $this->_orderRecordModel->getPurchaseRecordList($this->_depot_id,$this->_arrInputGet["start_time"],
				$this->_arrInputGet["end_time"]);
		} else {
			$arrList = $this->_orderRecordModel->getPurchaseRecordList($this->_depot_id,$this->_arrInputGet["start_time"],
				$this->_arrInputGet["end_time"],$this->_arrInputGet["status"]);
		}
		return $this->getMsg(0,"",$arrList);
	}
}