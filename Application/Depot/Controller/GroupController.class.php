<?php
namespace Depot\Controller;
use Think\Controller;
class GroupController extends BaseController {

	public function _initialize() {
		parent::_initialize();
		$this->_group = D("User/ManagerGroup");
	}

	public function groupList() {
		$arrResult = $this->_group->getGroupList();
		$this->getMsg(0,"",$arrResult);
	}

	public function add() {
		$arrInput = $_REQUEST;
		$arrResult = $this->_group->addGroup($arrInput);
		if (empty($arrResult['errNo'])) {
			$this->getMsg(0,"添加成功",1);
			return;
		}
		echo json_encode($arrResult);
	}

	public function delete() {
		$arrInput = $_GET;
		$arrResult = $this->_group->deleteGroup($arrInput);
		if (empty($arrResult['errNo'])) {
		    $this->getMsg(0,"删除成功",1);
			return;
		}
	    echo json_encode($arrResult);	
	}

	public function update() {
		return $this->getMsg(1,"更新失败");
	}
}
