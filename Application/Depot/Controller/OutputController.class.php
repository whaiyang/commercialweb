<?php
namespace Depot\Controller;
use Think\Controller;
class OutputController extends BaseController {

	public function _initialize() {
		parent::_initialize();
		$this->_orderRecordModel = D("DepotOrderRecord");
	}

	//主仓库和分仓库发货
	public function add() {
		$arrResult = $this->_orderRecordModel->sendOut($this->_arrInputGet["order_id"]);
		if (empty($arrResult['errNo'])) {
			$this->getMsg(0,"发货成功",1);
			return;
		}
		echo json_encode($arrResult);
	}

	//分仓库确认收货
	public function confirm() {
		if ($this->_is_primary_depot) {
			return $this->getMsg(1,"分仓库才能进行此操作");
		}
		$arrResult = $this->_orderRecordModel->confirmOrder($this->_arrInputGet["order_id"]);
		if (empty($arrResult['errNo'])) {
			$this->getMsg(0,"操作成功",1);
			return;
		}
		echo json_encode($arrResult);
	}

	//获取出货单
	public function outputList() {
		if ($this->_is_primary_depot) {
			if (empty($this->_arrInputGet['status'])) {
				$arrList = $this->_orderRecordModel->getSendOutRecordList($this->_depot_id,$this->_arrInputGet["start_time"],
					$this->_arrInputGet["end_time"]);
			} else {
				$arrList = $this->_orderRecordModel->getSendOutRecordList($this->_depot_id,$this->_arrInputGet["start_time"],
					$this->_arrInputGet["end_time"],$this->_arrInputGet['status']);
			}
		} else {
			if (empty($this->_arrInputGet['status'])) {
				$arrList = $this->_orderRecordModel->getSellerSendOutRecordList($this->_depot_id,$this->_arrInputGet["start_time"],
					$this->_arrInputGet["end_time"]);
			} else {
				$arrList = $this->_orderRecordModel->getSellerSendOutRecordList($this->_depot_id,$this->_arrInputGet["start_time"],
					$this->_arrInputGet["end_time"],$this->_arrInputGet['status']);
			}
		}
		if (empty($arrList['errNo'])) {
			$this->getMsg(0,"",$arrList);
			return;
		}
		echo json_encode($arrList);
	}
}