<?php
namespace Depot\Controller;
use Think\Controller;
class SellerController extends BaseController {

	public function _initialize() {
		parent::_initialize();
		$this->_changeModel = D("Seller/Change");
		$this->_purhcaseModel = D("Seller/SellerPurchase");
	}

	//审核改货申请
	public function checkChangeApplication() {
		if ($this->_is_primary_depot) {
			return $this->getMsg(1,"分仓库才能进行此操作");
		}
		$arrResult = $this->_changeModel->checkChangeRecord($this->_arrInputGet["order_id"],
			$this->_arrInputGet["pass"] == 1 ? true : false);
		if (empty($arrResult['errNo'])) {
			$this->getMsg(0,"审核成功",1);
			return;
		}
		echo json_encode($arrResult);
	}

	//改货单
	public function changeList() {
		if ($this->_is_primary_depot) {
			return $this->getMsg(1,"分仓库才能进行此操作");
		}
		$arrResult = $this->_changeModel->getChangeRecordListByDepotId($this->_depot_id);
		if (empty($arrResult['errNo'])) {
			return $this->getMsg(0,"",$arrResult);
		}
		echo json_encode($arrResult);
	}

	//已完成改货单
	public function changeHistoryList() {
		if ($this->_is_primary_depot) {
			return $this->getMsg(1,"分仓库才能进行此操作");
		}
		$arrResult = $this->_changeModel->getChangeRecordHistoryByDepotId($this->_depot_id,
			$this->_arrInputGet["start_time"],$this->_arrInputGet["end_time"]);
		if (empty($arrResult['errNo'])) {
			return $this->getMsg(0,"",$arrResult);
		}
		echo json_encode($arrResult);
	}

	//仓库管理员审核补货申请
	public function checkReplenishment() {
		if ($this->_is_primary_depot) {
			return $this->getMsg(1,"分仓库才能进行此操作");
		}
		$arrResult = $this->_purhcaseModel->checkReplenishment($this->_arrInputGet["repId"],
			$this->_arrInputGet["pass"] == 1 ? true : false);
		if (empty($arrResult['errNo'])) {
			$this->getMsg(0,"审核成功",1);
			return;
		}
		echo json_encode($arrResult);
	}

	//向商户发货
	public function sendOut() {
		if ($this->_is_primary_depot) {
			return $this->getMsg(1,"分仓库才能进行此操作");
		}
		$arrResult = $this->_purhcaseModel->depotSendOut($this->_arrInputGet["repId"]);
		if (empty($arrResult['errNo'])) {
			$this->getMsg(0,"发货成功",1);
			return;
		}
		echo json_encode($arrResult);
	}

	//补货申请列表
	public function repList() {
		if ($this->_is_primary_depot) {
			return $this->getMsg(1,"分仓库才能进行此操作");
		}
		$arrResult = $this->_purhcaseModel->getReplenishmentListByDepotId($this->_depot_id);
		if (empty($arrResult['errNo'])) {
			$this->getMsg(0,"",$arrResult);
			return;
		}
		echo json_encode($arrResult);
	}	
}