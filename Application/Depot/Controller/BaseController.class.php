<?php
namespace Depot\Controller;
use Think\Controller;
class BaseController extends Controller {

	//controller.action ＝> privilege
	protected $_privilegeCheck = array(
		"*.uploadfile" => "3",

		"depot.add" => "1",
		"depot.delete" => "1",
		"depot.update" => "1",
		"depot.depotlist" => "1",
		"depot.citylist" => "2",

		"user.userlist" => "1",	
		"user.register" => "1",

		"group.add" => "1",
		"group.delete" => "1",
		"group.grouplist" => "1",

		"product.primarydepotadd" => "1",
		"product.add" => "2",
		"product.productlist" => "2",
		"product.primarydepotproductlist" => "2",
		"product.primarydepotupdate" => "2",
		"product.delete" => "2",
		"product.getproduct" => "2",
		"product.synchrodata" => "2",
		"product.addproducttype" => "1",
		"product.updateproducttype" => "1",
		"product.producttypelist" => "1",

		"input.primarydepotadd" => "1",
		"input.addapplication" => "2",
		"input.checkorder" => "1",
		"input.purchaselist" => "2",

		"output.add" => "2",
		"output.confirm" => "2",
		"output.outputlist" => "2",

		"cancel.addrefundapplication" => "2",
		"cancel.checkrefundapplication" => "1",
		"cancel.getrefundlist" => "2",
		"cancel.getrefundlisttocheck" => "1",

		"damage.add" => "2",
		"damage.checkdamagerecord" => "1",
		"damage.damagerecordlist" => "2",
		"damage.damagerecordlisttocheck" => "1",

		"seller.checkchangeapplication" => "2",
		"seller.changelist" => "2",
		"seller.sendout" => "2",
		"seller.replist" => "2",
		"seller.checkreplenishment" => "2",
		"seller.changehistorylist" => "2",
	); 

	//controller.action 
	protected $_privilegeUnCheck = array(
		"user.login",	
		"user.checklogin",
		"user.logout",
	);

	//module.page
	protected $_showpagePrivilegeUnCheck = array(
		"index.login",
		"index.index",	
	);

	protected $_arrInputGet;
	protected $_arrInputPost;
	protected $_arrInputRequest;

	public function _initialize() {
		$this->_user = D("User/ManagerUser");
		//$this->_depot = D("Depots");
		$controllerName =  strtolower(CONTROLLER_NAME);
		if ($controllerName == "showpage") {
			if ($this->showpageUnCheck($_GET["page"],$_GET["module"]) || $this->_user->checkLogin()) {
				return true;   
			} else {
				$this->getMsg(C("ERRNO.ERR_NOT_LOGIN"),C("ERRNO.ERR_NOT_LOGIN_DESC"));
				exit;
			}	
		} else {
			$this->checkPrivilege();
		}
	}

	public function getMsg($errCode,$errMsg,$returnData=null) {
		$returnArr = array(
			"errNo" => $errCode,
			"errMsg" => $errMsg,
		);
		if ($returnData != null) {
			$returnArr["data"] = $returnData;
		}
		echo json_encode($returnArr);
		return true;
	}

	private function checkPrivilege() {
		$controllerName =  strtolower(CONTROLLER_NAME);
		$actionName = strtolower(ACTION_NAME);
		$checkName = $controllerName.".".$actionName;
		if (in_array($checkName,$this->_privilegeUnCheck)) {
			return true;
		}
		$userInfo = $this->_user->getCurrentUserInfo();
		if (empty($userInfo) || empty($userInfo["depot_id"])) {
			$this->getMsg(C("ERRNO.ERR_PERMISSION_DENIED"),C("ERRNO.ERR_PERMISSION_DENIED_DESC"));
			exit;
		}
		$this->_userInfo = $userInfo;
		foreach ($this->_privilegeCheck as $name => $priv) {
			if (substr(trim($name),0,1) == "*") {
				$tmpName = explode(".", $name);
				$tmpName = $tmpName[1];
				if ($actionName == $tmpName && $userInfo["privilege"] <= $priv) {
					$this->parseUserInfo();
					return true;
				}
			} else {
				if ($checkName == $name && $userInfo["privilege"] <= $priv) {
					$this->parseUserInfo();
					return true;
				}
			}
		}
		$this->getMsg(C("ERRNO.ERR_PERMISSION_DENIED"),C("ERRNO.ERR_PERMISSION_DENIED_DESC"));
		exit;
	}

	private function parseUserInfo() {
		$this->_depot_id = $this->_userInfo["depot_id"];
		//$this->_depot_info = $this->_depot->getDepotById($this->_depot_id);
		$this->_is_primary_depot = $this->_depot_id == C("DEPOT.PRIMARY_DEPOT_ID") ? true : false;
		$this->_primary_depot_id = C("DEPOT.PRIMARY_DEPOT_ID");

		$this->_arrInputGet = $_GET;
		$this->_arrInputPost = $_POST;
		$this->_arrInputRequest = $_REQUEST;
	}

	private function showpageUnCheck($page,$module) {
		if (empty($page)) {
			$page = "index";
		}
		if (empty($module)) {
			$module = "index";
		}
		$checkName = strtolower($module).".".strtolower($page);
		if (in_array($checkName, $this->_showpagePrivilegeUnCheck)) {
			return true;
		} else {
			return false;
		}
	}

	//图片上传
	public function uploadFile() {
		$upload = new \Think\Upload();// 实例化上传类
    	$upload->maxSize   =  10*1024*1024;// 设置附件上传大小
    	$upload->exts      =  array('jpg', 'gif', 'png', 'jpeg');// 设置附件上传类型
    	$upload->rootPath  =  C("__UPLOAD__"); // 设置附件上传根目录
    	// 上传文件 
    	$info   =   $upload->upload();
    	if(!$info) {// 上传错误提示错误信息
        	return $this->getMsg(1,"上传失败 : ".$upload->getError());
    	}else{// 上传成功
    		$fileArr = array();
    		foreach ($info as $name => $arrValue) {
    			$fileArr[$name]['pic_url'] = C("__UPLOAD__").$arrValue['savepath'].$arrValue['savename'];
    		}
        	return $this->getMsg(0,'上传成功！',$fileArr);
    	}
	}
}
