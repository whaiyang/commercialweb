<?php
namespace Depot\Controller;
use Think\Controller;
class ShowpageController extends BaseController {

	public function _initialize() {
		parent::_initialize();
	}

    public function index(){
    	$module = $_GET["module"];
		$page = $_GET["page"];
		if (empty($module)) {
			$module = "index";
		}
		if (empty($page)) {
			$page = "index";
		}
		$this->display("{$module}:{$page}");    
	}

	public function testSql() 
	{
		$model = M();
		var_dump($model->query("show variables like '%char%'"));
	}

	public function testFunc() {
		echo C("TMPL_PARSE_STRING.__UPLOAD__");
	}
}
