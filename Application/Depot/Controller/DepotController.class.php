<?php
namespace Depot\Controller;
use Think\Controller;
//仅权限为1的管理员有权使用仓库操作
class DepotController extends BaseController {

	public function _initialize() {
		parent::_initialize();
		$this->_depotModel = D("Depots");
	}

	//只针对分仓和承包商
	public function add() {
		$arrResult = $this->_depotModel->addDepot($this->_arrInputGet);
		if (empty($arrResult['errNo'])) {
			$this->getMsg(0,"添加成功",1);
			return;
		}
		echo json_encode($arrResult);
	}

	//只针对分仓和承包商
	public function delete() {
		$arrResult = $this->_depotModel->deleteDepot($this->_arrInputGet);
		if (empty($arrResult['errNo'])) {
			$this->getMsg(0,"删除成功",1);
			return;
		}
		echo json_encode($arrResult);
	}

	public function update() {
		$arrResult = $this->_depotModel->updateDepot($this->_arrInputGet);
		if (empty($arrResult['errNo'])) {
			$this->getMsg(0,"更新成功",1);
			return;
		}
		echo json_encode($arrResult);
	}

	public function depotList() {
		$arrList = $this->_depotModel->select();
		if (!empty($arrList)) {
			foreach ($arrList as &$arrValue) {
				if ($arrValue["type"] == C("DEPOT.NORMAL_DEPOT_TYPE")) {
					$arrValue["type"] = "分仓";
				} else if ($arrValue["type"] == C("DEPOT.CONTRACTOR_DEPOT_TYPE")) {
					$arrValue["type"] = "承包商";
				} else if ($arrValue["type"] == C("DEPOT.PRIMARY_DEPOT_TYPE")) {
					$arrValue["type"] = "总仓";
				} else {
					$arrValue["type"] = "未知";
				}
				$arrValue["create_time"] = date("Y-m-d H:i:s",$arrValue["create_time"]);
				$arrValue["update_time"] = date("Y-m-d H:i:s",$arrValue["update_time"]);
				$arrValue["city"] = C("CITY.".$arrValue["city_code"]);
				if (empty($arrValue["city"])) {
					$arrValue["city"] = "";
				}
			}
		}
		return $this->getMsg(0,"",$arrList);
	}	

	public function cityList() {
		return $this->getMsg(0,"",C("CITY"));
	}
}
