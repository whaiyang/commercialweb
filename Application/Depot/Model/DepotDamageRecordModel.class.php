<?php
	namespace Depot\Model;
	use Think\Model;

	/*
	*退货 model
	* 
	*/
	class DepotDamageRecordModel extends BaseModel {
		protected $trueTableName = 'damage';

		public function _initialize() {
			$this->_productModel = D("Product");
			$this->_detailDamageModel = D("DepotDamageRecordDetail");
		} 

		/**
		* @desc 添加报损纪录
		* @param 分仓id，总仓id，原因，商品id
		* @return result
		*/
		public function addDamageRecord($depotId,$hqDepotId,$reason,$goodsId,$goodsCount) {
			if (empty($depotId) || empty($hqDepotId) || empty($reason) || empty($goodsId) || empty($goodsCount)) {
				return $this->getErrorMsg(C("ERRNO.ERROR_PARAM_ERR"),C("ERRNO.ERROR_PARAM_ERR_DESC"));
			}
			//获取分仓商品信息
			$goodsInfo = $this->_productModel->getProductsByIds($goodsId);
			if (empty($goodsInfo)) {
				return $this->getErrorMsg(C("ERRNO.ERR_GET_PRODUCT_FAIL"),C("ERRNO.ERR_GET_PRODUCT_FAIL_DESC"));
			}
			//计算总价
			$totalPrice = 0;
			foreach ($goodsInfo as $key => $good) {
				$totalPrice += $good["price"] * intval($goodsCount[$key]);
			}
			$curTime = time();
			$damageArr = array(
				"damage_depot_id" => $depotId,
				"hq_depot_id" => $hqDepotId,
				"reason" => $reason,
				"damage_total_price" => $totalPrice,
				"create_time" => $curTime,
				"update_time" => $curTime,
				"status" => C("STATUS.STATUS_DAMAGE_SUBMIT"),
			);
			$this->startTrans();
			$boolAddResult = $this->add($damageArr);
			if (empty($boolAddResult)) {
				$this->rollback();
				return $this->getErrorMsg(C("ERRNO.ERR_ADD_PURCHASE_ORDER_FAIL"),C("ERRNO.ERR_ADD_PURCHASE_ORDER_FAIL_DESC"));
			}
			//插入单据详情
			$boolDetailResult = $this->_detailDamageModel->addRefundDetail($boolAddResult,$goodsInfo,$goodsCount);
			if (empty($boolDetailResult)) {
				$this->rollback();
				return $this->getErrorMsg(C("ERRNO.ERR_ADD_PURCHASE_ORDER_DETAIL_FAIL"),
					C("ERRNO.ERR_ADD_PURCHASE_ORDER_DETAIL_FAIL_DESC"));
			}
			$this->commit();
			return true;
		}	

		/**
		* @desc 审核报损纪录
		* @param 纪录id
		* @return result
		*/
		public function checkDamageRecord($recordId,$boolPass=false) {
			if (empty($recordId)) {
				return $this->getErrorMsg(C("ERRNO.ERROR_PARAM_ERR"),C("ERRNO.ERROR_PARAM_ERR_DESC"));
			}
			$recordInfo = $this->where("id=%d",$recordId)->find();
			if (empty($recordInfo)) {
				return $this->getErrorMsg(C("ERRNO.ERR_GET_PURCHASE_ORDER_FAIL"),
					C("ERRNO.ERR_GET_PURCHASE_ORDER_FAIL_DESC"));
			}
			if ($recordInfo["status"] != C("STATUS.STATUS_DAMAGE_SUBMIT")) {
				return $this->getErrorMsg(C("ERRNO.ERR_PURCHASE_ORDER_WRONG_STATUS"),
					C("ERRNO.ERR_PURCHASE_ORDER_WRONG_STATUS_DESC"));
			}

			$intStatus = C("STATUS.STATUS_DAMAGE_DISAPPROVE");
			if ($boolPass == true) {
				$intStatus = C("STATUS.STATUS_DAMAGE_COMFIRM");
			}
			$this->startTrans();
			$boolUpdateResult = $this->where("id={$recordId}")->save(array("status" => $intStatus,"update_time" => time()));
			if (empty($boolUpdateResult)) {
				$this->rollback();
				return $this->getErrorMsg(C("ERRNO.ERR_UPDATE_PURCHASE_ORDER_FAIL"),
					C("ERRNO.ERR_UPDATE_PURCHASE_ORDER_FAIL_DESC"));
			}
			if ($boolPass == false) {
				$this->commit();
				return true;
			}
			if (empty($this->_detailDamageModel)) {
				$this->_detailDamageModel = D("DepotDamageRecordDetail");
			}
			$updateResult = $this->_detailDamageModel->updateProductOnConfirm($recordId);
			if (empty($updateResult)){
				$this->rollback();
				return $this->getErrorMsg(C("ERRNO.ERR_UPDATE_PRODUCT_FAIL"),
					C("ERRNO.ERR_UPDATE_PRODUCT_FAIL_DESC"));
			}

			$this->commit();
			return true;
		}

		/**
		* @desc 报损纪录列表
		* @param 
		* @return result
		*/
		public function getDamageRecordList($depotId,$hqDepotId,$startTime,$endTime,$intStatus=null) {
			if ((empty($depotId) && empty($hqDepotId)) || empty($startTime) || empty($endTime)) {
				return $this->getErrorMsg(C("ERRNO.ERROR_PARAM_ERR"),C("ERRNO.ERROR_PARAM_ERR_DESC"));
			}
			$startTime = strtotime($startTime);
			$endTime = strtotime($endTime);
			$endTime += 86400;
			if (!empty($hqDepotId)) {
				$conditionArr = array(
					"damage.hq_depot_id" => $hqDepotId,
					"damage.create_time" => array(array("EGT",$startTime),array("LT",$endTime),'and'),	
				);
			} else {
				$conditionArr = array(
					"damage.damage_depot_id" => $depotId,
					"damage.create_time" => array(array("EGT",$startTime),array("LT",$endTime),'and'),	
				);
			}
			if (!empty($intStatus)) {
				$conditionArr['status'] = $intStatus;
			}
			$recordList = $this->join("depots as d_from on damage.damage_depot_id = d_from.id")
			->join("depots as d_to on damage.hq_depot_id = d_to.id")
			->where($conditionArr)
			->field("damage.*,d_from.name as depot_name,d_to.name as primary_depot_name")->select();
			if (empty($recordList)) {
				return array();
			}
			//获取报损详情
			foreach ($recordList as &$arrValue) {
				$arrValue["create_time"] = date("Y-m-d H:i:s",$arrValue["create_time"]);
				$arrValue["update_time"] = date("Y-m-d H:i:s",$arrValue["update_time"]);
				$detailArr = $this->_detailDamageModel->getDetail($arrValue["id"]);
				if (empty($detailArr)) {
					continue;
				}	
				$arrValue["detail"] = $detailArr;
			}	
			return $recordList;
		}
	} 	
