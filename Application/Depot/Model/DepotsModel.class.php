<?php
	namespace Depot\Model;
	use Think\Model;

	/*
	*仓库信息model
	*/
	class DepotsModel extends BaseModel {
		protected $trueTableName = "depots";

		public function getDepotById($id) {
			if (empty($id)) {
				return $this->getErrorMsg(C("ERRNO.ERROR_PARAM_ERR"),C("ERRNO.ERROR_PARAM_ERR_DESC"));
			}
			$arrResult = $this->where("id=%d",$id)->find();
			if (empty($arrResult)) {
				return array();
			}
			return $arrResult;
		}

		public function addDepot($arrInput) {
			if (empty($arrInput["name"]) || empty($arrInput["address"]) || empty($arrInput["schools"])
				|| empty($arrInput["type"]) || empty($arrInput["city_code"])) {
				return $this->getErrorMsg(C("ERRNO.ERROR_PARAM_ERR"),C("ERRNO.ERROR_PARAM_ERR_DESC"));
			}
			if ($arrInput["type"] != C("DEPOT.NORMAL_DEPOT_TYPE") && $arrInput["type"] != C("DEPOT.CONTRACTOR_DEPOT_TYPE")) {
				return $this->getErrorMsg(C("ERRNO.ERR_DEPOT_WRONG_TYPE"),C("ERRNO.ERR_DEPOT_WRONG_TYPE_DESC"));
			}
			$curTime = time();
			$boolResult = $this->add(array(
				"name" => $arrInput["name"],
				"address" => $arrInput["address"],
				"schools" => $arrInput["schools"],
				"type" => $arrInput["type"],
				"city_code" => $arrInput["city_code"],
				"create_time" => $curTime,
				"update_time" => $curTime,
			));
			if (empty($boolResult)) {
				return $this->getErrorMsg(C("ERRNO.ERR_INSERT_FAIL"),C("ERRNO.ERR_INSERT_FAIL_DESC"));
			}
			return true;
		}

		public function deleteDepot($arrInput) {
			if (empty($arrInput['id'])) {
				return $this->getErrorMsg(C("ERRNO.ERROR_PARAM_ERR"),C("ERRNO.ERROR_PARAM_ERR_DESC"));
			}
			$depotInfo = $this->where("id=%d",$arrInput["id"])->find();
			if (empty($depotInfo)) {
				return $this->getErrorMsg(C("ERRNO.ERR_DEPOT_NOT_EXIST"),C("ERRNO.ERR_DEPOT_NOT_EXIST_DESC"));
			}
			if ($depotInfo["type"] == C("DEPOT.PRIMARY_DEPOT_TYPE")) {
				return $this->getErrorMsg(C("ERRNO.ERR_DEPOT_OP_PRIMARY"),C("ERRNO.ERR_DEPOT_OP_PRIMARY_DESC"));
			}
			$boolResult = $this->where("id=%d",$arrInput["id"])->delete();
			if (empty($boolResult)) {
				return $this->getErrorMsg(C("ERRNO.ERR_DELETE_FAIL"),C("ERRNO.ERR_DELETE_FAIL_DESC"));
			}
			return true;
		}

		public function updateDepot($arrInput) {
			if (empty($arrInput["id"])) {
				return $this->getErrorMsg(C("ERRNO.ERROR_PARAM_ERR"),C("ERRNO.ERROR_PARAM_ERR_DESC"));
			}
			if (empty($arrInput["name"]) && empty($arrInput["address"]) && empty($arrInput["schools"])
				&& empty($arrInput["city_code"])) {
				return $this->getErrorMsg(C("ERRNO.ERROR_PARAM_ERR"),"参数不能都为空");
			}
			$updateArr = array();
			if (!empty($arrInput["name"])) {
				$updateArr["name"] = $arrInput["name"];
			}
			if(!empty($arrInput["address"])) {
				$updateArr["address"] = $arrInput["address"];
			}
			if (!empty($arrInput["schools"])) {
				$updateArr["schools"] = $arrInput["schools"];
			}
			if (!empty($arrInput["city_code"])) {
				$updateArr["city_code"] = $arrInput["city_code"];
			}
			$boolResult = $this->where("id=%d",$arrInput["id"])->save($updateArr);
			if (empty($boolResult)) {
				return $this->getErrorMsg(C("ERRNO.ERR_UPDATE_FAIL"),C("ERRNO.ERR_UPDATE_FAIL_DESC"));
			}
			return true;
		}
	}