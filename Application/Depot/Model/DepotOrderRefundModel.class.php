<?php
	namespace Depot\Model;
	use Think\Model;

	/*
	*退货 model
	* 
	*/
	class DepotOrderRefundModel extends BaseModel {
		protected $trueTableName = 'cancel';

		public function _initialize() {
			$this->_productModel = D("Product");
			$this->_detailRefundModel = D("DepotOrderRefundDetail");
		} 

		/**
		* @desc提交退货申请
		* @param 总仓，分仓
		* @return 操作结果
		*/
		public function addOrderRefund($depotTo,$depotFrom,$goodsIdTo,$goodsIdFrom,$goodsCount,$reason) {
			if (empty($depotTo) || empty($depotFrom) || empty($goodsIdFrom) || empty($goodsIdTo) || empty($goodsCount)
				|| !is_array($goodsIdFrom) || !is_array($goodsIdTo) || !is_array($goodsCount) || empty($reason)) {
				return $this->getErrorMsg(C("ERRNO.ERROR_PARAM_ERR"),C("ERRNO.ERROR_PARAM_ERR_DESC"));
			}
			//获取总仓分仓商品信息
			$goodsInfoTo = $this->_productModel->getProductsByIds($goodsIdTo);
			if (empty($goodsInfoTo)) {
				return $this->getErrorMsg(C("ERRNO.ERR_GET_PRODUCT_FAIL"),C("ERRNO.ERR_GET_PRODUCT_FAIL_DESC"));
			}
			$goodsInfoFrom = $this->_productModel->getProductsByIds($goodsIdFrom);
			if (empty($goodsInfoFrom)) {
				return $this->getErrorMsg(C("ERRNO.ERR_GET_PRODUCT_FAIL"),C("ERRNO.ERR_GET_PRODUCT_FAIL_DESC"));
			}
			//计算总价
			$totalPrice = 0;
			foreach ($goodsInfoFrom as $key => $good) {
				$totalPrice += $good["price"] * intval($goodsCount[$key]);
			}
			//添加进货单
			$curTime = time();
			$refundArr = array(
				"depot_id_to" => $depotTo,
				"depot_id_from" => $depotFrom,
				"reason" => $reason,
				"status" => C("STATUS.STATUS_DEPOT_REFUND_SUBMIT"),
				"create_time" => $curTime,
				"update_time" => $curTime,
				"total_price" => $totalPrice,
			);
			$this->startTrans();
			$boolAddResult = $this->add($refundArr);
			if (empty($boolAddResult)) {
				$this->rollback();
				return $this->getErrorMsg(C("ERRNO.ERR_ADD_PURCHASE_ORDER_FAIL"),C("ERRNO.ERR_ADD_PURCHASE_ORDER_FAIL_DESC"));
			}
			//插入单据详情
			$boolDetailResult = $this->_detailRefundModel->addRefundDetail($boolAddResult,$goodsInfoFrom,$goodsInfoTo,$goodsCount);
			if (empty($boolDetailResult)) {
				$this->rollback();
				return $this->getErrorMsg(C("ERRNO.ERR_ADD_PURCHASE_ORDER_DETAIL_FAIL"),
					C("ERRNO.ERR_ADD_PURCHASE_ORDER_DETAIL_FAIL_DESC"));
			}
			$this->commit();
			return true;
		}

		/**
		*@desc 总仓退货
		*@param 仓库id，退货数量，原因
		*@return result
		*/
		public function addPrimaryDepotOrderRefund($depotId,$goodsId,$goodsCount,$reason) {
			if (empty($depotId) || empty($goodsId) || empty($goodsCount) || empty($reason)
				|| !is_array($goodsId) || !is_array($goodsCount)) {
				return $this->getErrorMsg(C("ERRNO.ERROR_PARAM_ERR"),C("ERRNO.ERROR_PARAM_ERR_DESC"));
			}
			//获取总仓商品信息
			$goodsInfo = $this->_productModel->getProductsByIds($goodsId);
			if (empty($goodsInfo)) {
				return $this->getErrorMsg(C("ERRNO.ERR_GET_PRODUCT_FAIL"),C("ERRNO.ERR_GET_PRODUCT_FAIL_DESC"));
			}
			//计算总价
			$totalPrice = 0;
			foreach ($goodsInfo as $key => $good) {
				$totalPrice += $good["price"] * intval($goodsCount[$key]);
			}
			//添加进货单
			$curTime = time();
			$refundArr = array(
				"depot_id_to" => -1,
				"depot_id_from" => $depotId,
				"reason" => $reason,
				"status" => C("STATUS.STATUS_DEPOT_REFUND_APPROVE"),
				"create_time" => $curTime,
				"update_time" => $curTime,
				"total_price" => $totalPrice,
			);
			$this->startTrans();
			$boolAddResult = $this->add($refundArr);
			if (empty($boolAddResult)) {
				$this->rollback();
				return $this->getErrorMsg(C("ERRNO.ERR_ADD_PURCHASE_ORDER_FAIL"),C("ERRNO.ERR_ADD_PURCHASE_ORDER_FAIL_DESC"));
			}
			$refundId = $boolAddResult;
			//插入单据详情
			$boolDetailResult = $this->_detailRefundModel->addPrimaryDepotRefundDetail($refundId,$goodsInfo,$goodsCount);
			if (empty($boolDetailResult)) {
				$this->rollback();
				return $this->getErrorMsg(C("ERRNO.ERR_ADD_PURCHASE_ORDER_DETAIL_FAIL"),
					C("ERRNO.ERR_ADD_PURCHASE_ORDER_DETAIL_FAIL_DESC"));
			}
			$updateResult = $this->_detailRefundModel->updatePrimaryDepotProductOnConfirmRefund($refundId);
			if (empty($updateResult)){
				$this->rollback();
				return $this->getErrorMsg(C("ERRNO.ERR_UPDATE_PRODUCT_FAIL"),
					C("ERRNO.ERR_UPDATE_PRODUCT_FAIL_DESC"));
			}
			$this->commit();
			return true;
		}

		/**
		*@desc 审批退货单据
		*@param 单据id,是否批准
		*@return 操作结果
		*/
		public function checkOrderRefund($refundId,$boolPass=false) {
			if (empty($refundId)) {
				return $this->getErrorMsg(C("ERRNO.ERROR_PARAM_ERR"),C("ERRNO.ERROR_PARAM_ERR_DESC"));
			}
			$refundInfo = $this->where("id=%d",$refundId)->find();
			if (empty($refundInfo)) {
				return $this->getErrorMsg(C("ERRNO.ERR_GET_PURCHASE_ORDER_FAIL"),
					C("ERRNO.ERR_GET_PURCHASE_ORDER_FAIL_DESC"));
			}
			if ($refundInfo["status"] != C("STATUS.STATUS_DEPOT_REFUND_SUBMIT")) {
				return $this->getErrorMsg(C("ERRNO.ERR_PURCHASE_ORDER_WRONG_STATUS"),
					C("ERRNO.ERR_PURCHASE_ORDER_WRONG_STATUS_DESC"));
			}
			$intStatus = C("STATUS.STATUS_DEPOT_REFUND_DISAPPROVE");
			if ($boolPass == true) {
				$intStatus = C("STATUS.STATUS_DEPOT_REFUND_APPROVE");
			}
			$this->startTrans();
			$boolUpdateResult = $this->where("id={$refundId}")->save(array("status" => $intStatus,"update_time" => time()));
			if (empty($boolUpdateResult)) {
				$this->rollback();
				return $this->getErrorMsg(C("ERRNO.ERR_UPDATE_PURCHASE_ORDER_FAIL"),
					C("ERRNO.ERR_UPDATE_PURCHASE_ORDER_FAIL_DESC"));
			}
			if ($boolPass == false) {
				$this->commit();
				return true;
			}
			if (empty($this->_detailRefundModel)) {
				$this->_detailRefundModel = D("DepotOrderRefundDetail");
			}
			$updateResult = $this->_detailRefundModel->updateProductOnConfirmRefund($refundId);
			if (empty($updateResult)){
				$this->rollback();
				return $this->getErrorMsg(C("ERRNO.ERR_UPDATE_PRODUCT_FAIL"),
					C("ERRNO.ERR_UPDATE_PRODUCT_FAIL_DESC"));
			}

			$this->commit();
			return true;
		}
	
		/**
		 * @desc 获取仓库的退货单
		 * @param 时间精确到天
		 * @return 
		 */
		public function getRefundOrderList($depotFrom,$depotTo,$startTime,$endTime,$intStatus=null) {
			if (empty($startTime) || empty($endTime)) {
				return $this->getErrorMsg(C("ERRNO.ERROR_PARAM_ERR"),C("ERRNO.ERROR_PARAM_ERR_DESC"));
			}
			$startTime = strtotime($startTime);
			$endTime = strtotime($endTime);
			$endTime += 86400;
			$conditionArr = array(
				"cancel.create_time" => array(array("EGT",$startTime),array("LT",$endTime),'and'),
			);
			if (!empty($depotFrom)) {
				$conditionArr["depot_id_from"] = $depotFrom;
			} else if(!empty($depotTo)) {
				$conditionArr["depot_id_to"] = $depotTo;
			} else {
				return $this->getErrorMsg(C("ERRNO.ERROR_PARAM_ERR"),C("ERRNO.ERROR_PARAM_ERR_DESC"));
			}
			if (!empty($intStatus)) {
				$conditionArr['status'] = $intStatus;
			}
			$refundList = $this->join("depots as d_from on cancel.depot_id_from = d_from.id")
			->join("depots as d_to on cancel.depot_id_to = d_to.id")
			->where($conditionArr)
			->field("cancel.*,d_from.name as depot_from,d_to.name as depot_to")->select();
			if (empty($refundList)) {
				return array();
			}
			//获取退货单详情
			foreach ($refundList as &$arrValue) {
				$arrValue["create_time"] = date("Y-m-d H:i:s",$arrValue["create_time"]);
				$arrValue["update_time"] = date("Y-m-d H:i:s",$arrValue["update_time"]);
				$detailArr = $this->_detailRefundModel->getDetail($arrValue["id"]);
				if (empty($detailArr)) {
					continue;
				}	
				$arrValue["detail"] = $detailArr;
			}	
			return $refundList;
		}
	} 	
