<?php
	namespace Depot\Model;
	use Think\Model;

	/*
	*退货 model
	*/
	class DepotDamageRecordDetailModel extends BaseModel {
		protected $trueTableName = 'damage_detail';

		public function _initialize() {
			$this->_productModel = D("Product");
		}

		/**
		* @desc 添加报损详情
		* @param 纪录id，商品，数量
		* @return result
		*/
		public function addRefundDetail($recordId,$goodsInfo,$goodsCount) {
			if (empty($recordId) || empty($goodsInfo) || empty($goodsCount)
				|| !is_array($goodsInfo) || !is_array($goodsCount)) {
				return false;
			}
			$detailArr = array();
			foreach ($goodsInfo as $key => $good) {
				$detailArr[] = array(
					"damage_id" => $recordId,
					"damage_goods_id" => $good['id'],
					"damage_goods_amount" => $goodsCount[$key],
					"damage_goods_price" => $good["price"],
					"damage_goods_total_price" => $good["price"] * intval($goodsCount[$key]),
				);
			}
			$boolResult = $this->addAll($detailArr);
			return $boolResult;
		}

		/**
		* @desc 更新分仓商品数
		* @param 
		* @return result
		*/
		public function updateProductOnConfirm($recordId) {
			if (empty($recordId)) {
				return false;
			}
			$recordDetail = $this->where("damage_id={$recordId}")->select();
			if (empty($recordDetail)) {
				return false;
			}
			foreach ($recordDetail as $detail) {
				$goodId = $detail['damage_goods_id'];
				$count = $detail["damage_goods_amount"];
				//更新分仓商品数量
				$updateArr = array("cur_amount" => "cur_amount-{$count}");
				$updateResult = $this->_productModel->updateProduct($goodId,$updateArr);
				if (empty($updateResult)) {
					return false;
				}
			}

			return true;
		}

		public function getDetail($recordId) {
			if (empty($recordId)) {
				return false;
			}
			$detailArr = $this->where("damage_id=%d",$recordId)->select();
			return $detailArr;
		}
	} 	
