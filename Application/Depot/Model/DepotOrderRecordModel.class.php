<?php
	namespace Depot\Model;
	use Think\Model;

	class DepotOrderRecordModel extends BaseModel {
		protected $trueTableName = 'depot_purchase';

		public function _initialize() {
			$this->_productModel = D("Product");
			$this->_detailRecord = D("DepotOrderRecordDetail");
			$this->_sellerPurchaseRecord = D("Seller/SellerPurchase");
			$this->_sellerPurchaseRecordDetail = D("Seller/SellerPurchaseDetail");
		} 	

		/**
		*@desc 生成进货单据
		*@param 进货仓id，出货仓id，进货仓库商品id数组,出货仓库商品id数组,商品进货数
		*@return 操作结果
		*/
		public function createPurchaseOrder($depotTo,$depotFrom,$goodsIdTo,$goodsIdFrom,$goodsCount) {
			if (empty($depotTo) || empty($depotFrom) || empty($goodsIdFrom) || empty($goodsIdTo) || empty($goodsCount)
				|| !is_array($goodsIdFrom) || !is_array($goodsIdTo) || !is_array($goodsCount)) {
				return $this->getErrorMsg(C("ERRNO.ERROR_PARAM_ERR"),C("ERRNO.ERROR_PARAM_ERR_DESC"));
			}
			$goodsInfoTo = $this->_productModel->getProductsByIds($goodsIdTo);
			if (empty($goodsInfoTo)) {
				return $this->getErrorMsg(C("ERRNO.ERR_GET_PRODUCT_FAIL"),C("ERRNO.ERR_GET_PRODUCT_FAIL_DESC"));
			}
			$goodsInfoFrom = $this->_productModel->getProductsByIds($goodsIdFrom);
			if (empty($goodsInfoFrom)) {
				return $this->getErrorMsg(C("ERRNO.ERR_GET_PRODUCT_FAIL"),C("ERRNO.ERR_GET_PRODUCT_FAIL_DESC"));
			}
			//计算总价
			$totalPrice = 0;
			foreach ($goodsInfoFrom as $key => $good) {
				$totalPrice += $good["price"] * intval($goodsCount[$key]);
			}
			$curTime = time();
			$intStatus = C("STATUS.STATUS_DEPOT_PURCHASE_SUBMIT");
			$this->startTrans();
			//插入单据
			$boolAddResult = $this->add(array(
				"depot_to" => $depotTo,
				"depot_from" => $depotFrom,
				"create_time" => $curTime,
				"update_time" => $curTime,
				"status" => $intStatus,
			));
			$this->_pk = $boolAddResult;
			if (empty($boolAddResult)) {
				$this->rollback();
				return $this->getErrorMsg(C("ERRNO.ERR_ADD_PURCHASE_ORDER_FAIL"),C("ERRNO.ERR_ADD_PURCHASE_ORDER_FAIL_DESC"));
			}
			//插入单据详情
			$boolDetailResult = $this->_detailRecord->addPurchaseDetail($this->_pk,$goodsInfoFrom,$goodsInfoTo,$goodsCount);
			if (empty($boolDetailResult)) {
				$this->rollback();
				return $this->getErrorMsg(C("ERRNO.ERR_ADD_PURCHASE_ORDER_DETAIL_FAIL"),
					C("ERRNO.ERR_ADD_PURCHASE_ORDER_DETAIL_FAIL_DESC"));
			}
			$this->commit();
			return true;
		}


		/**
		*@desc 审批进货单据
		*@param 单据id,是否批准
		*@return 操作结果
		*/
		public function checkPurchaseOrder($orderId,$boolPass=false) {
			if (empty($orderId)) {
				return $this->getErrorMsg(C("ERRNO.ERROR_PARAM_ERR"),C("ERRNO.ERROR_PARAM_ERR_DESC"));
			}
			$orderInfo = $this->where("id={$orderId}")->find();
			if (empty($orderInfo)) {
				return $this->getErrorMsg(C("ERRNO.ERR_GET_PURCHASE_ORDER_FAIL"),
					C("ERRNO.ERR_GET_PURCHASE_ORDER_FAIL_DESC"));
			}
			if ($orderInfo['status'] != C("STATUS.STATUS_DEPOT_PURCHASE_SUBMIT")) {
				return $this->getErrorMsg(C("ERRNO.ERR_PURCHASE_ORDER_WRONG_STATUS"),
					C("ERRNO.ERR_PURCHASE_ORDER_WRONG_STATUS_DESC"));
			}
			$status = C("STATUS.STATUS_DEPOT_PURCHASE_DISAPPROVE");
			if ($boolPass === true) {
				$status = C("STATUS.STATUS_DEPOT_PURCHASE_CONFIRM");
			}
			$boolUpdateResult = $this->where("id={$orderId}")->save(array("status" => $status,"update_time" => time()));
			if (empty($boolUpdateResult)) {
				return $this->getErrorMsg(C("ERRNO.ERR_UPDATE_PURCHASE_ORDER_FAIL"),
					C("ERRNO.ERR_UPDATE_PURCHASE_ORDER_FAIL_DESC"));
			}
			return true;
		}

		/**
		*@desc 发货
		*@param 单据id
		*@return 操作结果
		*/
		public function sendOut($orderId) {
			if (empty($orderId)) {
				return $this->getErrorMsg(C("ERRNO.ERROR_PARAM_ERR"),C("ERRNO.ERROR_PARAM_ERR_DESC"));
			}
			$orderInfo = $this->where("id={$orderId}")->find();
			if (empty($orderInfo)) {
				return $this->getErrorMsg(C("ERRNO.ERR_GET_PURCHASE_ORDER_FAIL"),
					C("ERRNO.ERR_GET_PURCHASE_ORDER_FAIL_DESC"));
			}
			if ($orderInfo['status'] != C("STATUS.STATUS_DEPOT_PURCHASE_CONFIRM")) {
				return $this->getErrorMsg(C("ERRNO.ERR_PURCHASE_ORDER_WRONG_STATUS"),
					C("ERRNO.ERR_PURCHASE_ORDER_WRONG_STATUS_DESC"));
			}

			$this->startTrans();
			$boolUpdateResult = $this->where("id={$orderId}")->save(array("status" => C("STATUS.STATUS_DEPOT_PURCHASE_POSTING"),"update_time" => time()));
			if (empty($boolUpdateResult)) {
				$this->rollback();
				return $this->getErrorMsg(C("ERRNO.ERR_UPDATE_PURCHASE_ORDER_FAIL"),
					C("ERRNO.ERR_UPDATE_PURCHASE_ORDER_FAIL_DESC"));
			}
			$this->_detailRecord = D("DepotOrderRecordDetail"); 
			//更新出货仓库商品数量
			$boolDetailResult = $this->_detailRecord->updateProductOnSendOut($orderId);
			if (empty($boolDetailResult)) {
				$this->rollback();
				return $this->getErrorMsg(C("ERRNO.ERR_UPDATE_PRODUCT_FAIL"),
					C("ERRNO.ERR_UPDATE_PRODUCT_FAIL_DESC"));
			}
			$this->commit();
			return true;
		}

		/**
		*@desc 确认收货
		*@param 单据id
		*@return 操作结果
		*/
		public function confirmOrder($orderId) {
			if (empty($orderId)) {
				return $this->getErrorMsg(C("ERRNO.ERROR_PARAM_ERR"),C("ERRNO.ERROR_PARAM_ERR_DESC"));
			}
			$orderInfo = $this->where("id={$orderId}")->find();
			if (empty($orderInfo)) {
				return $this->getErrorMsg(C("ERRNO.ERR_GET_PURCHASE_ORDER_FAIL"),
					C("ERRNO.ERR_GET_PURCHASE_ORDER_FAIL_DESC"));
			}
			if ($orderInfo['status'] != C("STATUS.STATUS_DEPOT_PURCHASE_POSTING")) {
				return $this->getErrorMsg(C("ERRNO.ERR_PURCHASE_ORDER_WRONG_STATUS"),
					C("ERRNO.ERR_PURCHASE_ORDER_WRONG_STATUS_DESC"));
			}

			$this->startTrans();
			$boolUpdateResult = $this->where("id={$orderId}")->save(array("status" => C("STATUS.STATUS_DEPOT_PURCHASE_RECEIVED"),"update_time" => time()));
			if (empty($boolUpdateResult)) {
				$this->rollback();
				return $this->getErrorMsg(C("ERRNO.ERR_UPDATE_PURCHASE_ORDER_FAIL"),
					C("ERRNO.ERR_UPDATE_PURCHASE_ORDER_FAIL_DESC"));
			}
			$this->_detailRecord = D("DepotOrderRecordDetail"); 
			//更新出货仓库商品数量
			$boolDetailResult = $this->_detailRecord->updateProductOnConfirmOrder($orderId);
			if (empty($boolDetailResult)) {
				$this->rollback();
				return $this->getErrorMsg(C("ERRNO.ERR_UPDATE_PRODUCT_FAIL"),
					C("ERRNO.ERR_UPDATE_PRODUCT_FAIL_DESC"));
			}
			$this->commit();
			return true;
		}

		/**
		*@desc 总仓进货
		*@param 总仓id，商品id数组
		*@return 操作结果
		*/
		public function primaryDepotPurchase($depotTo,$goodsId,$goodsCount) {
			if (empty($depotTo) || empty($goodsId) || !is_array($goodsId)
				|| empty($goodsCount) || !is_array($goodsCount)) {
				return $this->getErrorMsg(C("ERRNO.ERROR_PARAM_ERR"),C("ERRNO.ERROR_PARAM_ERR_DESC"));
			}
			$goodsInfo = $this->_productModel->getProductsByIds($goodsId);
			if (!empty($goodsInfo["errNo"])) {
				return $this->getErrorMsg(C("ERRNO.ERR_GET_PRODUCT_FAIL"),C("ERRNO.ERR_GET_PRODUCT_FAIL_DESC"));
			}
			//计算总价
			$totalPrice = 0;
			foreach ($goodsInfo as $key => $good) {
				$totalPrice += $good["price"] * intval($goodsCount[$key]);
			}
			$curTime = time();
			$intStatus = C("STATUS.STATUS_DEPOT_PURCHASE_RECEIVED");
			$this->startTrans();
			//插入单据
			$boolAddResult = $this->add(array(
				"depot_to" => $depotTo,
				"depot_from" => -1,
				"create_time" => $curTime,
				"update_time" => $curTime,
				"total_price" => $totalPrice,
				"status" => $intStatus,
			));
			$orderId = $boolAddResult;
			if (empty($boolAddResult)) {
				$this->rollback();
				return $this->getErrorMsg(C("ERRNO.ERR_ADD_PURCHASE_ORDER_FAIL"),C("ERRNO.ERR_ADD_PURCHASE_ORDER_FAIL_DESC"));
			}
			//插入单据详情
			$boolDetailResult = $this->_detailRecord->addPurchaseDetail($orderId,null,$goodsInfo,$goodsCount);
			if (empty($boolDetailResult)) {
				$this->rollback();
				return $this->getErrorMsg(C("ERRNO.ERR_ADD_PURCHASE_ORDER_DETAIL_FAIL"),
					C("ERRNO.ERR_ADD_PURCHASE_ORDER_DETAIL_FAIL_DESC"));
			}
			//更新商品数量
			$boolDetailResult = $this->_detailRecord->updateProductOnConfirmOrder($orderId);
			if (empty($boolDetailResult)) {
				$this->rollback();
				return $this->getErrorMsg(C("ERRNO.ERR_UPDATE_PRODUCT_FAIL"),
					C("ERRNO.ERR_UPDATE_PRODUCT_FAIL_DESC"));
			}
			$this->commit();
			return true;
		}

		/**
		 * @desc 根据仓库id获取进货表,时间精确到天
		 * @param depot id,start time,end time
		 * @purchase application 
		 */
		public function getPurchaseRecordList($depotId,$startTime,$endTime,$intStatus=null) {
			if (empty($depotId) || empty($startTime) || empty($endTime)) {
				return $this->getErrorMsg(C("ERRNO.ERROR_PARAM_ERR"),C("ERRNO.ERROR_PARAM_ERR_DESC"));
			}
			$startTime = strtotime($startTime);
			$endTime = strtotime($endTime) + 86400;
			$condArr = array(
				"depot_to" => $depotId,
				"create_time" => array(array("EGT",$startTime),array("LT",$endTime),'and'),
			);
			if (!empty($intStatus)) {
				$condArr["status"] = $intStatus;
			}
			$arrList = $this->where($condArr)->select();
			if (empty($arrList)) {
				return array();			
			}
			//获取商品详情
			foreach ($arrList as &$arrValue) {
				$detail = $this->_detailRecord->getDetail($arrValue["id"]);
				$arrValue["detail"] = $detail;
			}

			return $arrList;
		}


		/**
		 * @desc 根据总仓库id获取出货表,时间精确到天
		 * @param depot id,start time,end time
		 * @purchase application 
		 */
		public function getSendOutRecordList($depotId,$startTime,$endTime,$intStatus) {
			if (empty($depotId) || empty($startTime) || empty($endTime)) {
				return $this->getErrorMsg(C("ERRNO.ERROR_PARAM_ERR"),C("ERRNO.ERROR_PARAM_ERR_DESC"));
			}
			$startTime = strtotime($startTime);
			$endTime = strtotime($endTime) + 86400;
			$condArr = array(
				"depot_from" => $depotId,
				"create_time" => array(array("EGT",$startTime),array("LT",$endTime),'and'),
			);
			if (!empty($intStatus)) {
				$condArr["status"] = $intStatus;
			}
			$arrList = $this->where($condArr)->select();
			if (empty($arrList)) {
				return array();         
			}
			//获取商品详情
			foreach ($arrList as &$arrValue) {
			    $detail = $this->_detailRecord->getDetail($arrValue["id"]);
				$arrValue["detail"] = $detail;
			}

			return $arrList;                            
		}

		/**
		 * @desc 根据分仓库id获取向商家的发货表，时间精确到天 ,状态采用商家补货流程的状态值
		 * @param depot id,start time,end time
		 * @return send out record list
		 */		
		public function getSellerSendOutRecordList($depotId,$startTime,$endTime,$intStatus=null) {
			if (empty($depotId) || empty($startTime) || empty($endTime)) {
				return $this->getErrorMsg(C("ERRNO.ERROR_PARAM_ERR"),C("ERRNO.ERROR_PARAM_ERR_DESC"));
			}
			$startTime = strtotime($startTime);
			$endTime = strtotime($endTime) + 86400;
			$condArr = array(
				"depot_id" => $depotId,
				"datetime" => array(array("EGT",$startTime),array("LT",$endTime),'and'),
			);
			if (!empty($intStatus)) {
				$condArr["status"] = $intStatus;
			}
			$arrList = $this->_sellerPurchaseRecord->where($condArr)->select();
			if (empty($arrList)) {
				return array();			
			}
			//获取商品详情
			foreach ($arrList as &$arrValue) {
				$detail = $this->_sellerPurchaseRecordDetail->getDetail($arrValue["id"]);
				$arrValue["detail"] = $detail;
			}

			return $arrList;
		}
	}
