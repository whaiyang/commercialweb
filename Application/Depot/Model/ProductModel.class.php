<?php
	namespace Depot\Model;
	use Think\Model;

	/*
	*商品信息model
	*/
	class ProductModel extends BaseModel {
		protected $trueTableName = "goods";
		protected $productTypeTable = "goods_type";

		/**
		*@desc 按商品id来获取商品信息
		*@param product id
		*@return product infomation
		*/
		public function getProductById($productId) {
			$strSql = "SELECT * FROM {$this->trueTableName} WHERE id=%d";
			$arrResult = $this->query($strSql,array($productId));
			if (empty($arrResult)) {
				return array();
			}
			return $arrResult[0];
		}

		/**
		*@desc 按商品id数组来获取所有商品信息
		*@param product id list
		*@return products infomation
		*/
		public function getProductsByIds($idList) {
			if (empty($idList) || !is_array($idList)) {
				return $this->getErrorMsg(C("ERRNO.ERROR_PARAM_ERR"),C("ERRNO.ERROR_PARAM_ERR_DESC"));
			}
			$productsInfo = $this->where(array("id" => array("in",$idList)))->select();
			if (empty($productsInfo)) {
				return $this->getErrorMsg(C("ERRNO.ERR_GET_PRODUCT_FAIL"),C("ERRNO.ERR_GET_PRODUCT_FAIL_DESC"));
			}
			return $productsInfo;
		}

		/**
		*@desc 更新商品信息
		*@param product id,更新字段和值array(key=>value)
		*@return result
		*/
		public function updateProduct($productId,$arrInput) {
			if (empty($productId) || empty($arrInput)) {
				return $this->getErrorMsg(C("ERRNO.ERROR_PARAM_ERR"),C("ERRNO.ERROR_PARAM_ERR_DESC"));
			}
			$strSql = "UPDATE {$this->trueTableName} SET ";
			$setFields = array();
			foreach ($arrInput as $key => $value) {
				if (is_string($value)) {
					$setFields[] = "{$key}=%s";
				} else {
					$setFields[] = "{$key}=%d";
				}
			}
			$strSql .= join(",",$setFields);
			$strSql .= " WHERE id=%d";
			$boolResult = $this->execute($strSql,array_merge($arrInput,array($productId)));
			if (empty($boolResult)) {
				return $this->getErrorMsg(C("ERRNO.ERR_UPDATE_PRODUCT_FAIL"),C("ERRNO.ERR_UPDATE_PRODUCT_FAIL_DESC"));
			}
			return $boolResult;
		}

		/**
		*@desc 按仓库id来获取商品列表
		*@param depot id
		*@return product infomation
		*/
		public function getProductListByDepotId($depotId) {
			if (empty($depotId)) {
				return false;
			}
			$strSql = "SELECT * FROM {$this->trueTableName} WHERE depot_id=%d AND seller_id=0";
			$arrResult = $this->query($strSql,$depotId);
			if (empty($arrResult)) {
				return array();
			}
			return $arrResult;
		}

		/**
		*@desc 按商户来获取商品列表
		*@param seller id
		*@return product infomation
		*/
		public function getProductListBySellerId($sellerId) {
			$strSql = "SELECT * FROM {$this->trueTableName} WHERE seller_id=%d AND depot_id=0";
			$arrResult = $this->query($strSql,$sellerId);
			if (empty($arrResult)) {
				return array();
			}
			return $arrResult;
		}

		/**
		*@desc 根据商品类别和仓库来筛选商品
		*@param 类别id，仓库id
		*@return product infomation
		*/
		public function getProductByTypeId($typeId,$depotId=null) {
			$strSql = "SELECT * FROM {$this->trueTableName} WHERE type_id=%d";
			if (empty($depotId)) {
				$arrResult = $this->query($strSql,array($typeId));
			} else {
				$strSql .= " AND depot_id=%d";
				$arrResult = $this->query($strSql,array($typeId,$depotId));
			}
			if (empty($arrResult)) {
				return array();
			}
			return $arrResult[0];
		}

		/**
		*@desc 获取商品类型列表
		*@param seller id
		*@return product infomation
		*/
		public function getProductTypeList() {
			$strSql = "SELECT * FROM {$this->productTypeTable}";
			$arrResult = $this->query($strSql);
			if (empty($arrResult)) {
				return array();
			}
			return $arrResult;
		}

		/**
		*@desc 添加商品类型
		*@param $name,$icon_url,$pic_url
		*@return result
		*/
		public function addProductType($name,$iconUrl,$picUrl) {
			if (empty($name)) {
				return $this->getErrorMsg(C("ERRNO.ERROR_PARAM_ERR"),C("ERRNO.ERROR_PARAM_ERR_DESC"));
			}
			$typeArr = array();
			$typeArr["name"] = $name;
			if (!empty($iconUrl)) {
				$typeArr["icon_url"] = $iconUrl;
			}
			if (!empty($picUrl)) {
				$typeArr["pic_url"] = $picUrl;
			}
			$boolResult = $this->table($this->productTypeTable)->field("name,icon_url,pic_url")->add($typeArr);
			if (empty($boolResult)) {
				return $this->getErrorMsg(C("ERRNO.ERR_ADD_PRODUCT_TYPE_FAIL"),C("ERRNO.ERR_ADD_PRODUCT_TYPE_FAIL_DESC"));
			}
			return $boolResult;
		}

		/**
		*@desc 更新商品类型
		*@param id，$name,$icon_url,$pic_url
		*@return result
		*/
		public function updateProductType($productTypeId,$name=null,$iconUrl=null,$picUrl=null) {
			if (empty($productTypeId) || (empty($name) && empty($iconUrl) && empty($picUrl))) {
				return $this->getErrorMsg(C("ERRNO.ERROR_PARAM_ERR"),C("ERRNO.ERROR_PARAM_ERR_DESC"));
			}

			$strSql = "UPDATE {$this->productTypeTable} SET ";
			$fieldsArr = array();
			$valuesArr = array();
			if (!empty($name)) {
				$fieldsArr[] = "name='%s'";
				$valuesArr[] = $name;
			}

			if (!empty($iconUrl)) {
				$fieldsArr[] = "icon_url='%s'";
				$valuesArr[] = $iconUrl;
			}

			if (!empty($picUrl)) {
				$fieldsArr[] = "pic_url='%s'";
				$valuesArr[] = $picUrl;
			}

			$valuesArr[] = $productTypeId;

			$strSql .= join(",",$fieldsArr);
			$strSql .= " WHERE id=%d";

			$boolResult = $this->execute($strSql,$valuesArr);
			if (empty($boolResult)) {
				return $this->getErrorMsg(C("ERRNO.ERR_UPDATE_PRODUCT_TYPE_FAIL"),C("ERRNO.ERR_UPDATE_PRODUCT_TYPE_FAIL_DESC"));
			}
			return $boolResult;
		}

		/**
		*@desc 主仓库添加商品
		*@param 商品信息
		*@param name
		*@param info
		*@param type_id
		*@param cost
		*@param price
		*@param supplier
		*@param pic_url
		*@param cur_amount
		*@param total_amount
		*@param depot_id
		*@param recommand_amount
		*@return result
		*/
		public function depotAddProduct($arrInput) {
			if (empty($arrInput["name"]) || empty($arrInput["info"]) || empty($arrInput["type_id"])
				|| empty($arrInput["cost"]) || empty($arrInput["price"]) || empty($arrInput["supplier"]) || empty($arrInput["pic_url"])
				|| empty($arrInput["cur_amount"]) || empty($arrInput["total_amount"]) || empty($arrInput["depot_id"])
				|| empty($arrInput["recommand_amount"])) {
				return $this->getErrorMsg(C("ERRNO.ERROR_PARAM_ERR"),C("ERRNO.ERROR_PARAM_ERR_DESC"));
			}

			$valuesArr = array(
				"name" => $arrInput["name"],
				"info" => $arrInput["info"],
				"type_id" => $arrInput["type_id"],
				"cost" => $arrInput["cost"],
				"price" => $arrInput["price"],
				"supplier" => $arrInput["supplier"],
				"pic_url" => $arrInput["pic_url"],
				"cur_amount" => $arrInput["cur_amount"],
				"total_amount" => $arrInput["total_amount"],
				"depot_id" => $arrInput["depot_id"],
				"recommand_amount" => $arrInput["recommand_amount"],
			);
			$boolResult = $this->add($valuesArr);
			if (empty($boolResult)) {
				return $this->getErrorMsg(C("ERRNO.ERR_ADD_PRODUCT_FAIL"),C("ERRNO.ERR_ADD_PRODUCT_FAIL_DESC"));
			}
			return $boolResult;
		}

		/**
		* 分仓库添加商品
		* @param 商品id数组
		* @return result
		*/
		public function minorDepotAddProduct($arrInput) {
			if (empty($arrInput["goodsIdArr"]) || !is_array($arrInput["goodsIdArr"]) || empty($arrInput["depot_id"])) {
				return $this->getErrorMsg(C("ERRNO.ERROR_PARAM_ERR"),C("ERRNO.ERROR_PARAM_ERR_DESC"));
			}
			$goodsIdArr = $arrInput["goodsIdArr"];
			$fieldsArr = array(
				"name","info","type_id",
				"cost","price","supplier",
				"pic_url","recommand_amount",
			);
			$strSql = "SELECT ".join(",",$fieldsArr)." FROM {$this->trueTableName} ";
			$strSql .= "WHERE id in (%s)";
			$goodsInfo = $this->query($strSql,join(",",$goodsIdArr));
			if (empty($goodsInfo) || count($goodsInfo) < count($goodsIdArr)) {
				return $this->getErrorMsg(C("ERRNO.ERR_ADD_PRODUCT_FAIL"),C("ERRNO.ERR_ADD_PRODUCT_FAIL_DESC"));
			}

			foreach ($goodsInfo as &$arrValue) {
				$arrValue["cur_amount"] = 0;
				$arrValue["total_amount"] = 0;
				$arrValue["depot_id"] = $arrInput["depot_id"];
			}
			$boolResult = $this->addAll($goodsInfo);
			if (empty($boolResult)) {
				return false;
			}
			return true;
		}

		/**
		*@desc 商户添加商品
		*		传入仓库里的商品id，自动获取商品信息，关联商户id
		*		会在表里新加一条记录 默认on_sale为0
		*@param 商品id，seller id
		*@return result
		*/
		public function sellerAddProduct($productId,$sellerId) {
			if (empty($productId) || empty($sellerId)) {
				return $this->getErrorMsg(C("ERRNO.ERROR_PARAM_ERR"),C("ERRNO.ERROR_PARAM_ERR_DESC"));
			}
			$productInfo = $this->getProductById($productId);
			if (empty($productInfo)) {
				return $this->getErrorMsg(C("ERRNO.ERR_NO_SUCH_PRODUCT"),C("ERRNO.ERR_NO_SUCH_PRODUCT_DESC"));	
			}
			$valuesArr = array(
				"name" => $productInfo["name"],
				"info" => $productInfo["info"],
				"type_id" => $productInfo["type_id"],
				"cost" => $productInfo["cost"],
				"price" => $productInfo["price"],
				"supplier" => $productInfo["supplier"],
				"pic_url" => $productInfo["pic_url"],
				"cur_amount" => 0,
				"total_amount" => 0,
				"recommand_amount" => $productInfo["recommand_amount"],
				"on_sale" => 0,
				"seller_id" => $sellerId,
			);
			$boolResult = $this->add($valuesArr);
			if (empty($boolResult)) {
				return $this->getErrorMsg(C("ERRNO.ERR_ADD_PRODUCT_FAIL"),C("ERRNO.ERR_ADD_PRODUCT_FAIL_DESC"));
			}
			return $boolResult;
		}

		/**
		*@desc 根据商品的名查询此商品是否在指定仓库中有记录
		*@param 商品名数组 ， 仓库id
		*@return 返回去重后的商品数组
		*/
		public function productDepotMatch($productNameArr,$depotId) {
			if (empty($depotId)) {
				return false;
			}
			if (empty($productNameArr) || !is_array($productNameArr)) {
				return $this->where("depot_id=%d",$depotId)->select();
			}
			$fieldsArr = array(
				"id",
				"name","info","type_id",
				"cost","price","supplier","pic_url",
				"recommand_amount",
			);
			$goodsArr = $this->where(array(
				"depot_id" => $depotId,
				"name" => array("not in",$productNameArr),
			))->field($fieldsArr)->select();
			if (empty($goodsArr)) {
				return array();
			}
			return $goodsArr;
		}

		/**
		*@desc 根据商品的名查询此商品是否在指定仓库中有记录
		*@param 商品名数组 ， 仓库id
		*@return 返回仓库没有的商品数组
		*/
		public function productDepotCheck($productNameArr,$depotId) {
			if (empty($depotId) || empty($productNameArr) || !is_array($productNameArr)) {
				return false;
			}
			$goodsArr = $this->where(array(
				"depot_id" => $depotId,
				"name" => array("in",$productNameArr),
			))->field("name")->select();
			$nameArr = array();
			if (!empty($goodsArr)) {
				foreach ($goodsArr as $strName) {
					$nameArr[] = $strName["name"];
				}
			}
			$notInGoodsNameArr = array();
			foreach ($productNameArr as $strName) {
				if (!in_array($strName, $nameArr)) {
					$notInGoodsNameArr[] = $strName;
				}
			}
			return $notInGoodsNameArr;
		}

		/**
		*@desc 根据商品的名查询此商品在指定仓库中的商品id
		*@param 商品名数组 ， 仓库id
		*@return 商品id数组
		*/
		public function productDepotMatchIn($productNameArr,$depotId) {
			if (empty($depotId)) {
				return false;
			}
			if (empty($productNameArr) || !is_array($productNameArr)) {
				return $this->where("depot_id=%d",$depotId)->select();
			}
			$goodsArr = $this->where(array(
				"depot_id" => $depotId,
				"name" => array("in",$productNameArr),
			))->field("id")->select();
			if (empty($goodsArr)) {
				return array();
			}
			$idArr = array();
			foreach ($goodsArr as $goodArr) {
				$idArr[] = $goodArr["id"];
			}
			return $idArr;
		}
	}
