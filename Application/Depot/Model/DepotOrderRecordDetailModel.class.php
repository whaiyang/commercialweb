<?php
	namespace Depot\Model;
	use Think\Model;

	/*
	*进货，出货单详情 model
	*/
	class DepotOrderRecordDetailModel extends BaseModel {
		protected $trueTableName = 'depot_purchase_detail'; 	

		public function _initialize() {
			$this->_productModel = D("Product");
		} 

		/**
		*@desc 根据单据id和商品id数组生成purchase detail
		*@param 单据id，商品信息
		*@return 操作结果
		*/
		public function addPurchaseDetail($orderId,$goodsInfoFrom,$goodsInfoTo,$goodsCount) {
			if (empty($orderId) || empty($goodsInfoTo) || !is_array($goodsInfoTo)
				|| empty($goodsCount) || !is_array($goodsCount)) {
				return false;
			}
			$detailArr = array();
			if (!empty($goodsInfoFrom)) {
				foreach ($goodsInfoFrom as $key => $good) {
					$detailArr[] = array(
						"purchase_id" => $orderId,
						"purchase_goods_id_from" => $good['id'],
						"purchase_goods_id_to" => $goodsInfoTo[$key]["id"],
						"purchase_goods_count" => $goodsCount[$key],
						"purchase_goods_price" => $good["price"],
					);
				}
			} else {//总仓进货
				foreach ($goodsInfoTo as $key => $good) {
					$detailArr[] = array(
						"purchase_id" => $orderId,
						"purchase_goods_id_from" => -1,
						"purchase_goods_id_to" => $good["id"],
						"purchase_goods_count" => $goodsCount[$key],
						"purchase_goods_price" => $good["price"],
					);
				}
			}
			$boolResult = $this->addAll($detailArr);
			return $boolResult;
		}

		/**
		*@desc 确认收货，更新进货方商品信息
		*@param 单据id
		*@return 操作结果
		*/
		public function updateProductOnConfirmOrder($orderId) {
			if (empty($orderId)) {
				return false;
			}
			$orderDetail = $this->where("purchase_id={$orderId}")->select();
			if (empty($orderDetail)) {
				return false;
			}
			foreach ($orderDetail as $detail) {
				$goodId = $detail['purchase_goods_id_to'];
				$count = $detail["purchase_goods_count"];
				$updateArr = array("cur_amount" => "cur_amount+{$count}");
				$updateResult = $this->_productModel->updateProduct($goodId,$updateArr);
				if (empty($updateResult)) {
					return false;
				}
			}

			return true;
		}

		/**
		*@desc 发货，更新发货方商品信息
		*@param 单据id
		*@return 操作结果
		*/
		public function updateProductOnSendOut($orderId) {
			if (empty($orderId)) {
				return false;
			}
			$orderDetail = $this->where("purchase_id={$orderId}")->select();
			if (empty($orderDetail)) {
				return false;
			}
			foreach ($orderDetail as $detail) {
				$goodId = $detail['purchase_goods_id_from'];
				$count = $detail["purchase_goods_count"];
				$updateArr = array("cur_amount" => "cur_amount-{$count}");
				$updateResult = $this->_productModel->updateProduct($goodId,$updateArr);
				if (empty($updateResult)) {
					return false;
				}
			}

			return true;
		}

		/**
		 * @desc 根据单据id获取商品详情
		 * @param order id
		 * @return detail
		 */
		public function getDetail($orderId) {
			if (empty($orderId)) {
				return false;
			}
			$orderDetail = $this->where("purchase_id={$orderId}")->select();
			if (empty($orderDetail)) { 
				return array();      
			}
			return $orderDetail;
		}
	}
