<?php
	namespace Depot\Model;
	use Think\Model;

	/*
	*退货 model
	*/
	class DepotOrderRefundDetailModel extends BaseModel {
		protected $trueTableName = 'cancel_detail';

		public function _initialize() {
			$this->_productModel = D("Product");
		}

		/**
		*@desc 根据单据id和商品id数组生成refund detail
		*@param 单据id，商品信息
		*@return 操作结果
		*/
		public function addRefundDetail($refundId,$goodsInfoFrom,$goodsInfoTo,$goodsCount) {
			if (empty($refundId) || empty($goodsInfoTo) || !is_array($goodsInfoTo)
				|| empty($goodsInfoFrom) || !is_array($goodsInfoFrom)
				|| empty($goodsCount) || !is_array($goodsCount)) {
				return false;
			}
			$detailArr = array();
			foreach ($goodsInfoTo as $key => $good) {
				$detailArr[] = array(
					"cancel_id" => $refundId,
					"cancel_goods_id_to" => $good['id'],
					"cancel_goods_id_from" => $goodsInfoFrom[$key]["id"],
					"cancel_goods_amount" => $goodsCount[$key],
					"cancel_goods_price" => $good["price"],
					"cancel_goods_total_price" => $good["price"] * intval($goodsCount[$key]),
				);
			}
			$boolResult = $this->addAll($detailArr);
			return $boolResult;
		}

		/**
		*@desc 总仓，根据单据id和商品id数组生成refund detail
		*@param 单据id，商品信息
		*@return 操作结果
		*/
		public function addPrimaryDepotRefundDetail($refundId,$goodsInfo,$goodsCount) {
			if (empty($refundId) || empty($goodsInfo) || !is_array($goodsInfo)
				|| empty($goodsCount) || !is_array($goodsCount)) {
				return false;
			}
			$detailArr = array();
			foreach ($goodsInfo as $key => $good) {
				$detailArr[] = array(
					"cancel_id" => $refundId,
					"cancel_goods_id_to" => -1,
					"cancel_goods_id_from" => $good['id'],
					"cancel_goods_amount" => $goodsCount[$key],
					"cancel_goods_price" => $good["price"],
					"cancel_goods_total_price" => $good["price"] * intval($goodsCount[$key]),
				);
			}
			$boolResult = $this->addAll($detailArr);
			return $boolResult;
		}

		/**
		*@desc 确认退货，更新总仓和分仓商品信息
		*@param 单据id
		*@return 操作结果
		*/
		public function updateProductOnConfirmRefund($refundId) {
			if (empty($refundId)) {
				return false;
			}
			$refundDetail = $this->where("cancel_id={$refundId}")->select();
			if (empty($refundDetail)) {
				return false;
			}
			foreach ($refundDetail as $detail) {
				$goodIdTo = $detail['cancel_goods_id_to'];
				$goodIdFrom = $detail['cancel_goods_id_from'];
				$count = $detail["cancel_goods_amount"];
				//更新分仓商品数量
				$updateArr = array("cur_amount" => "cur_amount-{$count}");
				$updateResult = $this->_productModel->updateProduct($goodIdFrom,$updateArr);
				if (empty($updateResult)) {
					return false;
				}
				//更新总仓商品数量
				$updateArr = array("cur_amount" => "cur_amount+{$count}");
				$updateResult = $this->_productModel->updateProduct($goodIdTo,$updateArr);
				if (empty($updateResult)) {
					return false;
				}
			}

			return true;
		}

		/**
		*@desc 总仓退货，更新总仓商品信息
		*@param 单据id
		*@return 操作结果
		*/
		public function updatePrimaryDepotProductOnConfirmRefund($refundId) {
			if (empty($refundId)) {
				return false;
			}
			$refundDetail = $this->where("cancel_id={$refundId}")->select();
			if (empty($refundDetail)) {
				return false;
			}
			foreach ($refundDetail as $detail) {
				$goodIdFrom = $detail['cancel_goods_id_from'];
				$count = $detail["cancel_goods_amount"];
				//更新总仓商品数量
				$updateArr = array("cur_amount" => "cur_amount+{$count}");
				$updateResult = $this->_productModel->updateProduct($goodIdFrom,$updateArr);
				if (empty($updateResult)) {
					return false;
				}
			}

			return true;
		}

		public function getDetail($refundId) {
			if (empty($refundId)) {
				return false;
			}
			$detailArr = $this->where("cancel_id=%d",$refundId)->select();
			return $detailArr;
		}
	} 	
