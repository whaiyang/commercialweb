<?php
namespace Seller\Controller;
use Think\Controller;
class DepotController extends BaseController {

	public function _initialize() {
		parent::_initialize();
		$this->_productModel = D("Depot/Product");
		$this->_changeModel = D("Change");
		$this->_purchaseModel = D("SellerPurchase");
		$this->_purchaseDetailModel = D("SellerPurchaseDetail");
	}

	//提交改货申请
	public function addChangeApplication() {
		$detailsId = explode(",", $this->_arrInputGet["detailsId"]);
		$goodsCount = explode(",", $this->_arrInputGet["goodsCount"]);
		$arrResult = $this->_changeModel->addChangeRecord($this->_arrInputGet["repId"],
			$detailsId,$goodsCount);
		if (empty($arrResult['errNo'])) {
			$this->getMsg(0,"提交改货申请成功",1);
			return;
		}
		echo json_encode($arrResult);
	}

	//改货历史
	public function changeHistory() {
		//$arrResult = $this->_changeModel->getChangeRecordHistoryBySellerId($this->_userInfo["id"],
		//	$this->_arrInputGet["start_time"],$this->_arrInputGet["end_time"]);
		$arrResult = $this->_changeModel->getChangeRecordListBySellerId($this->_uid);
		if (empty($arrResult['errNo'])) {
			$this->getMsg(0,"",$arrResult);
			return;
		}
		echo json_encode($arrResult);
	}

	//提交补货申请
	public function addReplenishmentApplication() {
		if (empty($this->_arrInputGet["goodsId"]) || empty($this->_arrInputGet["goodsCount"])
			|| empty($this->_arrInputGet["depot_id"])) {
			return $this->getMsg(C("ERRNO.ERROR_PARAM_ERR"),C("ERRNO.ERROR_PARAM_ERR_DESC"));
		}
		$goodsId = explode(",", $this->_arrInputGet["goodsId"]);
		$countArr = explode(",", $this->_arrInputGet["goodsCount"]);
		if (count($goodsId) != count($countArr)) {
			return $this->getMsg(C("ERRNO.ERROR_PARAM_ERR"),"进货数量有误");
		}
		$sellerGoods = $this->_productModel->where(array(
			"seller_id" => $this->_uid,
			"id" => array("in",join(",",$goodsId)),
		))->field(array("name"))->select();
		$nameArr = array();
		if (!empty($sellerGoods)) {
			foreach ($sellerGoods as $strName) {
				$nameArr[] = $strName["name"];
			}
		}
		$resultGoodsInfo = $this->_productModel->productDepotCheck($nameArr,$this->_arrInputGet["depot_id"]);
		if (!empty($resultGoodsInfo)) {
			return $this->getMsg(0,"存在仓库没有的商品",$resultGoodsInfo);
		}
		$goodsIdFrom = $this->_productModel->productDepotMatchIn($nameArr,$this->_arrInputGet["depot_id"]);
		$arrResult = $this->_purchaseModel->createReplenishment($this->_userInfo["id"],
			$this->_arrInputGet["depot_id"],$goodsId,$goodsIdFrom,
			$countArr);
		if (empty($arrResult['errNo'])) {
			$this->getMsg(0,"提交补货申请成功",1);
			return;
		}
		echo json_encode($arrResult);
	}

	//补货申请列表
	public function listReplenishment() {
		if (empty($this->_arrInputGet["status"])) {
			$arrResult = $this->_purchaseModel->getReplenishmentList($this->_userInfo["id"],
				$this->_arrInputGet["start_time"],$this->_arrInputGet["end_time"]);
		} else {
			$arrResult = $this->_purchaseModel->getReplenishmentList($this->_userInfo["id"],
				$this->_arrInputGet["start_time"],$this->_arrInputGet["end_time"],$this->_arrInputGet["status"]);
		}
		if (empty($arrResult['errNo'])) {
			$this->getMsg(0,"",$arrResult);
			return;
		}
		echo json_encode($arrResult);
	}

	//确认收货
	public function confirm() {
		$arrResult = $this->_purchaseModel->sellerConfirm($this->_arrInputGet["repId"]);
		if (empty($arrResult['errNo'])) {
			$this->getMsg(0,"收货成功",1);
			return;
		}
		echo json_encode($arrResult);
	}
}