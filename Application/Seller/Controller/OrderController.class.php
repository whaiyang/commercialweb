<?php
namespace Seller\Controller;
use Think\Controller;
class OrderController extends BaseController {

	public function _initialize() {
		parent::_initialize();
		$this->_storeOrderModel = D("Store/Order");
		$this->_orderModel = D("Order");
	}

	//发货
	public function sendOut() {
		if (empty($this->_arrInputGet['order_id'])) {
			return $this->getMsg(C("ERRNO.ERROR_PARAM_ERR"),C("ERRNO.ERROR_PARAM_ERR_DESC"));
		}
		$this->_storeOrderModel->startTrans();
		$arrResult = $this->_storeOrderModel->beginDeliverOrder($this->_arrInputGet['order_id']);
		if (!empty($arrResult["errNo"])) {
			$this->_storeOrderModel->rollback();
			return $this->getMsg(1,"发单失败");
		}
		$arrResult = $this->_orderModel->sellerSendOut($this->_arrInputGet['order_id']);
		if (!empty($arrResult["errNo"])) {
			$this->_storeOrderModel->rollback();
			echo json_encode($arrResult);return;
		}
		$this->_storeOrderModel->commit();
		return $this->getMsg(0,"发单成功");
	}

	//订单列表
	public function orderList() {
		if (empty($this->_arrInputGet['status'])) {

		} else {

		}
	}

	//已支付，待发货订单
	public function payedOrderList() {

	}
}