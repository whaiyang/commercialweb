<?php
namespace Seller\Controller;
use Think\Controller;
class PayController extends BaseController {

	public function _initialize() {
		$this->_payConfig = C("ALIPAY");
		$this->_alipayNotify = new \Pay\Ali\Lib\AlipayNotify($this->_payConfig);
		$this->_aliPay = new \Pay\Ali\AliPay($this->_payConfig);
		$this->_payLog = D("PayLog");
		parent::_initialize();
	}

	/**
	* 获取支付宝的支付链接和支付参数信息
	*/
	public function getPayInfo() {
		if (empty($_GET["seller_id"]) || empty($_GET["total_fee"])) {
			return $this->getMsg(1,"参数错误");
		}
		$orderId = $_GET["seller_id"]."_".time();
		$totalFee = $_GET["total_fee"];
		//支付结果回调地址
		$notifyUrl = U(C("ALIPAY.seller_notify_url"),null,true,true);
		//支付完成页面跳转地址
		$returnUrl = U(C("ALIPAY.seller_return_url"),null,true,true);
		$tradeNo = $this->_payLog->addOne($orderId,$totalFee,"seller_pay");
		if (empty($tradeNo)) {
			return $this->getMsg(1,"添加支付纪录失败");
		}
		//订单号，商品名，总价，描述
		$payInfo = $this->_aliPay->buildPayRequestParam($tradeNo,"充值",$totalFee,"商户充值",$notifyUrl,$returnUrl);
		return $this->getMsg(0,"",$payInfo);
	}

	//支付完成，支付宝会调用此操作,具体业务操作在此处完成
	public function notify() {
		//计算得出通知验证结果
		$verifyResult = $this->_alipayNotify->verifyNotify();
		//验证成功
		if ($verifyResult) {
			\Think\Log::write("[Pay_Notify_Verify_SUCCESS] return: ".json_encode($_POST),"INFO");
			//商户订单号
			$outTradeNo = $_POST['out_trade_no'];
			//支付宝交易号
			$tradeNo = $_POST['trade_no'];
			//交易状态
			$tradeStatus = $_POST['trade_status'];
			//更新支付纪录
			$this->_payLog->payConfirm($outTradeNo,$tradeNo,$tradeStatus);
			//交易成功
			if ($tradeStatus == "TRADE_FINISHED" || $tradeStatus == "TRADE_SUCCESS") {
				//业务代码

			} else {//交易失败
				\Think\Log::write("[Pay_Notify_Trade_Fail] return: ".json_encode($_POST),"WARN");
			}
		} else {//验证失败
			\Think\Log::write("[Pay_Notify_Verify_Fail] return: ".json_encode($_POST),"WARN");
		}
	}

	//支付完成，支付宝会把页面返回到此操作,此处不应有业务操作
	public function callback() {
		//计算得出通知验证结果
		$verifyResult = $this->_alipayNotify->verifyReturn();
		//验证成功
		if ($verifyResult) {
			\Think\Log::write("[Pay_Return_Verify_SUCCESS] return: ".json_encode($_POST),"INFO");
			//商户订单号
			$outTradeNo = $_POST['out_trade_no'];
			//支付宝交易号
			$tradeNo = $_POST['trade_no'];
			//交易状态
			$tradeStatus = $_POST['trade_status'];
			//交易成功
			if ($tradeStatus == "TRADE_FINISHED" || $tradeStatus == "TRADE_SUCCESS") {
				$this->assign("STATUS","SUCCESS");
				$this->display("paysuccess");
			} else {//交易失败
				\Think\Log::write("[Pay_Return_Trade_Fail] return: ".json_encode($_POST),"WARN");
				$this->assign("STATUS","FAIL");
				$this->display("payfail");
			}
		} else {//验证失败
			\Think\Log::write("[Pay_Return_Verify_Fail] return: ".json_encode($_POST),"WARN");
		}
	}

	/**
	* 获取支付宝的退款链接和支付参数信息,暂时只支持一次退一笔
	*/
	public function getRefundInfo() {
		$payLogId = $_GET['pay_id'];
		$reason = $_GET["reason"];
		if (empty($payLogId) || empty($reason)) {
			return $this->getMsg(1,"参数错误");
		}
		$payInfo = $this->_payLog->where("id=%d",$payLogId)->find();
		if (empty($payInfo)) {
			return $this->getMsg(1,"支付信息不存在");
		}
		$notifyUrl = U(C("ALIPAY.seller_refund_notify_url"),null,true,true);
		$returnUrl = U(C("ALIPAY.seller_refund_return_url"),null,true,true);
		$boolResult = $this->_payLog->refundConfirm($payLogId,C("STATUS.STATUS_PAY_REFUNDING"));
		if (empty($boolResult)) {
			return $this->getMsg(1,"更新支付纪录失败");
		}
		$batchNo = date("Ymd").$payLogId;
		$detailData = $payInfo["src_trade_no"]."^".$payInfo["total_fee"]."^".$reason;
		//批次号,退款单数目,详细信息（支付宝单号^退款金额^理由）
		$payInfo = $this->_aliPay->buildRefundRequestParam($batchNo,1,$detailData,$notifyUrl,$returnUrl);
		return $this->getMsg(0,"",$payInfo);
	}

	//退款完成，支付宝会调用此操作,具体业务操作在此处完成
	public function refundNotify() {
		//计算得出通知验证结果
		$verifyResult = $this->_alipayNotify->verifyNotify();
		//验证成功
		if ($verifyResult) {
			\Think\Log::write("[Refund_Notify_Verify_SUCCESS] return: ".json_encode($_POST),"INFO");
			//批次号
			$batchNo = $_POST['batch_no'];		
			//批量退款数据中转账成功的笔数
			$successNum = $_POST['success_num'];
			//批量退款数据中的详细信息
			$resultDetails = $_POST['result_details'];
			//从批次号中提取paylog id
			$payLogId = substr($batchNo, 8);
			$strStatus = C("STATUS.STATUS_PAY_REFUND_OK");
			if ($successNum <= 0) {
				$strStatus = C("STATUS.STATUS_PAY_REFUND_FAIL");
			}
			$boolResult = $this->_payLog->refundConfirm($payLogId,$strStatus,$resultDetails);
			if (empty($boolResult)) {
				\Think\Log::write("[Refund_PayLog_Update_Fail] pay_log_id:{$payLogId} batch_no:{$batchNo}","WARN");
			}
			if ($successNum >= 1) {
				//业务代码
			}
		} else {//验证失败
			\Think\Log::write("[Refund_Notify_Verify_Fail] return: ".json_encode($_POST),"WARN");
		}
	}
}