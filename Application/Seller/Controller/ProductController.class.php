<?php
namespace Seller\Controller;
use Think\Controller;
class ProductController extends BaseController {

	public function _initialize() {
		parent::_initialize();
		$this->_productModel = D("Depot/Product");
		$this->_depotModel = D("Depot/Depots");
	}

	//添加商品
	public function add() {
		$uid = $this->_userInfo["id"];
		$goodId = $this->_arrInputGet["product_id"];
		$arrResult = $this->_productModel->sellerAddProduct($goodId,$uid);
		if (empty($arrResult['errNo'])) {
			return $this->getMsg(0,"添加成功",1);
		}
		echo json_encode($arrResult);
	}

	//商品列表
	public function productList() {
		$uid = $this->_userInfo["id"];
		if (!isset($this->_arrInputGet["on_sale"])) {
			$arrResult = $this->_productModel->where("seller_id=%d",$uid)
			->field("id,name,info,type_id,cost,price,supplier,discount,cur_amount,recommand_amount,pic_url,on_sale,seller_id")->select();
		} else {
			$arrResult = $this->_productModel->where("seller_id=%d AND on_sale=%d",$uid,$this->_arrInputGet["on_sale"])
			->field("id,name,info,type_id,cost,price,supplier,discount,cur_amount,recommand_amount,pic_url,on_sale,seller_id")->select();
		}
		if (empty($arrResult)) {
			$arrResult = array();
		}
		return $this->getMsg(0,"",$arrResult);
	}

	//添加折扣信息
	public function addDiscount() {
		$floatDiscount = $this->_arrInputGet["discount"];
		$goodId = $this->_arrInputGet["product_id"];
		$uid = $this->_userInfo["id"];
		$boolResult = $this->_productModel->where("id=%d AND seller_id=%d",$goodId,$uid)->save(array("discount" => $floatDiscount));
		if (empty($boolResult)) {
			return $this->getMsg(1,"操作失败");
		} else {
			return $this->getMsg(0,"操作成功");
		}
	}

	//商品上架
	public function onSale() {
		if (empty($this->_arrInputGet["product_id"])) {
			return $this->getMsg(C("ERRNO.ERROR_PARAM_ERR"),C("ERRNO.ERROR_PARAM_ERR_DESC"));
		}
		$goodId = $this->_arrInputGet["product_id"];
		$uid = $this->_userInfo["id"];
		$boolResult = $this->_productModel->where("id=%d AND seller_id=%d",$goodId,$uid)->save(
			array("on_sale" => 1)
		);
		if (empty($boolResult)) {
			return $this->getMsg(1,"操作失败");
		} else {
			return $this->getMsg(0,"操作成功");
		}
	}

	//商品下架
	public function pullOff() {
		if (empty($this->_arrInputGet["product_id"])) {
			return $this->getMsg(C("ERRNO.ERROR_PARAM_ERR"),C("ERRNO.ERROR_PARAM_ERR_DESC"));
		}
		$goodId = $this->_arrInputGet["product_id"];
		$uid = $this->_userInfo["id"];
		$boolResult = $this->_productModel->where("id=%d AND seller_id=%d",$goodId,$uid)->save(
			array("on_sale" => 0)
		);
		if (empty($boolResult)) {
			return $this->getMsg(1,"操作失败");
		} else {
			return $this->getMsg(0,"操作成功");
		}
	}

	//指定仓库的商品列表
	public function depotGoodsList() {
		if (empty($this->_arrInputGet["depot_id"])) {
			return $this->getMsg(C("ERRNO.ERROR_PARAM_ERR"),C("ERRNO.ERROR_PARAM_ERR_DESC"));
		}
		$sellerGoods = $this->_productModel->where("seller_id=%d",$this->_userInfo["id"])->field(array("name"))->select();
		$nameArr = array();
		foreach ($minorDepotGoods as $arr) {
			$nameArr[] = $arr["name"];
		}
		$resultGoodsInfo = $this->_productModel->productDepotMatch($nameArr,$this->_arrInputGet["depot_id"]);
		if ($resultGoodsInfo === false) {
			return $this->getMsg(1,"获取商品列表失败");	
		}
		return $this->getMsg(0,"",$resultGoodsInfo);
	}

	//同城仓库列表
	public function depotList() {
		$cityCode = $this->_userInfo["city_code"];
		if (empty($cityCode)) {
			return $this->getMsg(1,"没有找到同城仓库");
		}
		$depotList = $this->_depotModel->where("city_code=%d and type<>%d",$cityCode,C("DEPOT.PRIMARY_DEPOT_TYPE"))->select();
		if (empty($depotList)) {
			return $this->getMsg(1,"没有找到同城仓库");
		}
		return $this->getMsg(0,"",$depotList);
	}
}