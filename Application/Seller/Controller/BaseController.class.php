<?php
namespace Seller\Controller;
use Think\Controller;
class BaseController extends Controller {

	//module.page
	protected $_showpagePrivilegeUnCheck = array(
		"page#user.login",
		'interface#user.login',
	);

	protected $_arrInputGet;
	protected $_arrInputPost;
	protected $_arrInputRequest;

	public function _initialize() {
		$this->_user = D("User/ManagerUser");
		$controllerName =  strtolower(CONTROLLER_NAME);
		$actionName = strtolower(ACTION_NAME);
		$boolUnCheck = $this->showpageUnCheck($_GET["page"],$_GET["module"]);
		$boolUnCheck2 = $this->interfaceUnCheck($controllerName,$actionName);
		if ($boolUnCheck || $boolUnCheck2 || $this->_user->checkLogin()) {
			if (!$boolUnCheck && !$boolUnCheck2) {
				$this->parseUserInfo();
			}
			$this->_arrInputGet = $_GET;
			$this->_arrInputPost = $_POST;
			$this->_arrInputRequest = $_REQUEST;
			return true;   
		} else {
			$this->getMsg(C("ERRNO.ERR_NOT_LOGIN"),C("ERRNO.ERR_NOT_LOGIN_DESC"));
			exit;
		}	
	}

	public function getMsg($errCode,$errMsg,$returnData=null) {
		$returnArr = array(
			"errNo" => $errCode,
			"errMsg" => $errMsg,
		);
		if ($returnData != null) {
			$returnArr["data"] = $returnData;
		}
		echo json_encode($returnArr);
		return true;
	}

	private function showpageUnCheck($page,$module) {
		if (empty($page)) {
			$page = "index";
		}
		if (empty($module)) {
			$module = "index";
		}
		$checkName = "page#".strtolower($module).".".strtolower($page);
		if (in_array($checkName, $this->_showpagePrivilegeUnCheck)) {
			return true;
		} else {
			return false;
		}
	}

	private function interfaceUnCheck($controllerName,$actionName) {
		$checkName = "interface#".strtolower($controllerName).".".strtolower($actionName);
		if (in_array($checkName, $this->_showpagePrivilegeUnCheck)) {
			return true;
		} else {
			return false;
		}
	}

	private function parseUserInfo() {
		$userInfo = $this->_user->getCurrentUserInfo(true);
		if (empty($userInfo) || !empty($userInfo["depot_id"])) {
			$this->getMsg(C("ERRNO.ERR_PERMISSION_DENIED"),C("ERRNO.ERR_PERMISSION_DENIED_DESC"));
			exit;
		}
		$this->_userInfo = $userInfo;
		$this->_uid = $userInfo['id'];
	}

	//图片上传
	public function uploadFile() {
		$upload = new \Think\Upload();// 实例化上传类
    	$upload->maxSize   =  10*1024*1024;// 设置附件上传大小
    	$upload->exts      =  array('jpg', 'gif', 'png', 'jpeg');// 设置附件上传类型
    	$upload->rootPath  =  C("__UPLOAD__"); // 设置附件上传根目录
    	// 上传文件 
    	$info   =   $upload->upload();
    	if(!$info) {// 上传错误提示错误信息
        	return $this->getMsg(1,"上传失败 : ".$upload->getError());
    	}else{// 上传成功
    		$fileArr = array();
    		foreach ($info as $name => $arrValue) {
    			$fileArr[$name]['pic_url'] = C("__UPLOAD__").$arrValue['savepath'].$arrValue['savename'];
    		}
        	return $this->getMsg(0,'上传成功！',$fileArr);
    	}
	}
}
