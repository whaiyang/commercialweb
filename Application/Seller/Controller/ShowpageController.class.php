<?php
namespace Seller\Controller;
use Think\Controller;
class ShowpageController extends Controller {

	public function _initialize() {
		parent::_initialize();
	}

    public function index(){
    	$module = $_GET["module"];
		$page = $_GET["page"];
		if (empty($module)) {
			$module = "index";
		}
		if (empty($page)) {
			$page = "index";
		}
		$this->display("{$module}:{$page}");    
	}
}