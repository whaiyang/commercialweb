<?php
	namespace Seller\Model;
	use Think\Model;

	class SellerPurchaseDetailModel extends BaseModel {
		protected $trueTableName = 'purchase_detail';

		public function _initialize() {
			$this->_productModel = D("Depot/Product");
		} 	

		/**
		*@desc 根据单据id和商品id数组生成replenishment detail
		*@param 单据id，商品信息
		*@return 操作结果
		*/
		public function addReplenishmentDetail($repId,$sellerGoodsInfoArr,$depotGoodsInfoArr,$goodsCount) {
			if (empty($repId) || empty($sellerGoodsInfoArr) || !is_array($sellerGoodsInfoArr)
				|| empty($depotGoodsInfoArr) || !is_array($depotGoodsInfoArr)
				|| empty($goodsCount) || !is_array($goodsCount)) {
				return false;
			}
			$detailArr = array();
			foreach ($depotGoodsInfoArr as $key => $good) {
				$detailArr[] = array(
					"purchase_id" => $repId,
					"depot_goods_id" => $good['id'],
					"purchase_goods_id" => $sellerGoodsInfoArr[$key]["id"],
					"purchase_goods_count" => $goodsCount[$key],
					"purchase_goods_price" => $good["price"],
					"purchase_goods_total_price" => $good["price"] * intval($goodsCount[$key]),
				);
			}
			$boolResult = $this->addAll($detailArr);
			return $boolResult;
		}

		/**
		*@desc 发货，更新发货方商品信息
		*@param 单据id
		*@return 操作结果
		*/
		public function updateProductOnSendOut($repId) {
			if (empty($repId)) {
				return false;
			}
			$orderDetail = $this->where("purchase_id={$repId}")->select();
			if (empty($orderDetail)) {
				return false;
			}
			foreach ($orderDetail as $detail) {
				$goodId = $detail['depot_goods_id'];
				$count = $detail["purchase_goods_count"];
				$updateArr = array("cur_amount" => "cur_amount-{$count}");
				$updateResult = $this->_productModel->updateProduct($goodId,$updateArr);
				if (empty($updateResult)) {
					return false;
				}
			}

			return true;
		}

		/**
		*@desc 楼主确认收货，更新楼主商品数量
		*@param 单据id
		*@return 操作结果
		*/
		public function updateProductOnConfirm($repId) {
			if (empty($repId)) {
				return false;
			}
			$orderDetail = $this->where("purchase_id={$repId}")->select();
			if (empty($orderDetail)) {
				return false;
			}
			foreach ($orderDetail as $detail) {
				$goodId = $detail['purchase_goods_id'];
				$count = $detail["purchase_goods_count"];
				$updateArr = array("cur_amount" => "cur_amount+{$count}");
				$updateResult = $this->_productModel->updateProduct($goodId,$updateArr);
				if (empty($updateResult)) {
					return false;
				}
			}

			return true;
		}

		/**
		 * @desc 根据单据id获取商品详情
		 * @param order id
		 * @return details
		 */
		public function getDetail($repId) {
			if (empty($repId)) {
				return false;
			}
			$orderDetail = $this->where("purchase_id={$repId}")->select();
			if (empty($orderDetail)) { 
				return array();      
			}
			return $orderDetail;
		}

		/**
		 * @desc 根据id获取商品详情
		 * @param order id
		 * @return detail
		 */
		public function getDetailById($detailId,$purchaseId=false) {
			if (empty($detailId)) {
				return false;
			}
			if (empty($purchaseId)) {
				$orderDetail = $this->where("id=%d",$detailId)->select();
			} else {
				$orderDetail = $this->where("id=%d and purchase_id=%d",$detailId,$purchaseId)->select();
			}
			if (empty($orderDetail)) { 
				return array();      
			}
			return $orderDetail[0];
		}
	}