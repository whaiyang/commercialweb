<?php
	namespace Seller\Model;
	use Think\Model;

	class OrderModel extends BaseModel {
		protected $trueTableName = 'order';

		public function _initialize() {
			$this->_productModel = D("Depot/Product");
		} 

		public function sellerSendOut($orderId) {
			if (empty($orderId)) {
				return $this->getMsg(C("ERRNO.ERROR_PARAM_ERR"),C("ERRNO.ERROR_PARAM_ERR_DESC"));
			}
			$orderInfo = $this->where("id=%d",$orderId)->find();
			if (empty($orderInfo)) {
				return $this->getMsg(C("ERRNO.ERROR_NO_SUCH_ORDER_ERR"),C("ERRNO.ERROR_NO_SUCH_ORDER_ERR_DESC"));
			}
			$goodsArr = explode("|",$orderInfo["goods"]);
			$this->startTrans();
			foreach ($goodsArr as $strValue) {
				list($goodId,$goodCount) = explode(",",$strValue);
				$updateArr = array("cur_amount" => "cur_amount-{$goodCount}");
				$updateResult = $this->_productModel->updateProduct($goodId,$updateArr);
				if (empty($updateResult)) {
					$this->rollback();
					return $this->getMsg(C("ERRNO.ERROR_ORDER_GOODS_INFO_ERR"),C("ERRNO.ERROR_ORDER_GOODS_INFO_ERR_DESC"));
				}
			}
			$this->where("id=%d",$orderId)->save(array("update_time" => time()));
			$this->commit();
			return true;
		}
	}
