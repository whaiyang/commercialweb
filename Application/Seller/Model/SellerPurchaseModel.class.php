<?php
	namespace Seller\Model;
	use Think\Model;

	class SellerPurchaseModel extends BaseModel {
		protected $trueTableName = 'purchase';

		public function _initialize() {
			$this->_productModel = D("Depot/Product");
			$this->_purchaseDetail = D("Seller/SellerPurchaseDetail");
		} 

		/**
		* @desc 商户创建补货申请
		* @param 商户id，仓库id，商户补货商品id数组，仓库商品id数组，补货数量
		* @return 操作结果
		*/
		public function createReplenishment($sellerId,$depotFrom,$sellerGoodsIdArr,$depotGoodsIdArr,$goodsCount) {
			if (empty($sellerId) || empty($depotFrom) || empty($sellerGoodsIdArr) || empty($depotGoodsIdArr)
				||empty($goodsCount) || !is_array($sellerGoodsIdArr) || !is_array($depotGoodsIdArr)
				|| !is_array($goodsCount)) {
				return $this->getErrorMsg(C("ERRNO.ERROR_PARAM_ERR"),C("ERRNO.ERROR_PARAM_ERR_DESC"));
			}
			$sellerGoodsInfo = $this->_productModel->getProductsByIds($sellerGoodsIdArr);
			if (empty($sellerGoodsInfo)) {
				return $this->getErrorMsg(C("ERRNO.ERR_GET_PRODUCT_FAIL"),C("ERRNO.ERR_GET_PRODUCT_FAIL_DESC"));
			}
			$depotGoodsInfo = $this->_productModel->getProductsByIds($depotGoodsIdArr);
			if (empty($depotGoodsInfo)) {
				return $this->getErrorMsg(C("ERRNO.ERR_GET_PRODUCT_FAIL"),C("ERRNO.ERR_GET_PRODUCT_FAIL_DESC"));
			}
			//计算总价
			$totalPrice = 0;
			foreach ($depotGoodsInfo as $key => $good) {
				$totalPrice += $good["price"] * intval($goodsCount[$key]);
			}
			$curTime = time();
			$intStatus = C("STATUS.STATUS_REP_PURCHASE_SUBMIT");
			$this->startTrans();
			//插入单据
			$boolAddResult = $this->add(array(
				"depot_id" => $depotFrom,
				"manager_id" => $sellerId,
				"datetime" => $curTime,
				"total_price" => $totalPrice,
				"status" => $intStatus,
			));
			if (empty($boolAddResult)) {
				$this->rollback();
				return $this->getErrorMsg(C("ERRNO.ERR_ADD_PURCHASE_ORDER_FAIL"),C("ERRNO.ERR_ADD_PURCHASE_ORDER_FAIL_DESC"));
			}
			//插入单据详情
			$boolDetailResult = $this->_purchaseDetail->addReplenishmentDetail($boolAddResult,$sellerGoodsInfo,$depotGoodsInfo,$goodsCount);
			if (empty($boolDetailResult)) {
				$this->rollback();
				return $this->getErrorMsg(C("ERRNO.ERR_ADD_PURCHASE_ORDER_DETAIL_FAIL"),
					C("ERRNO.ERR_ADD_PURCHASE_ORDER_DETAIL_FAIL_DESC"));
			}
			$this->commit();
			return true;
		}

		/**
		* @desc 仓库管理员审核补货申请
		* @param 补货单id，是否审批
		* @return 操作结果
		*/
		public function checkReplenishment($repId,$boolPass) {
			if (empty($repId)) {
				return $this->getErrorMsg(C("ERRNO.ERROR_PARAM_ERR"),C("ERRNO.ERROR_PARAM_ERR_DESC"));
			}
			$repInfo = $this->where("id={$repId}")->find();
			if (empty($repInfo)) {
				return $this->getErrorMsg(C("ERRNO.ERR_GET_PURCHASE_ORDER_FAIL"),
					C("ERRNO.ERR_GET_PURCHASE_ORDER_FAIL_DESC"));
			}
			if ($repInfo['status'] != C("STATUS.STATUS_REP_PURCHASE_SUBMIT")) {
				return $this->getErrorMsg(C("ERRNO.ERR_PURCHASE_ORDER_WRONG_STATUS"),
					C("ERRNO.ERR_PURCHASE_ORDER_WRONG_STATUS_DESC"));
			}
			$status = C("STATUS.STATUS_REP_PURCHASE_DISAPPROVE");
			if ($boolPass === true) {
				$status = C("STATUS.STATUS_REP_PURCHASE_CONFIRM");
			}
			$boolUpdateResult = $this->where("id={$repId}")->save(array("status" => $status));
			if (empty($boolUpdateResult)) {
				return $this->getErrorMsg(C("ERRNO.ERR_UPDATE_PURCHASE_ORDER_FAIL"),
					C("ERRNO.ERR_UPDATE_PURCHASE_ORDER_FAIL_DESC"));
			}
			return true;
		}

		/**
		* @desc 仓库管理员发货
		* @param 补货单id
		* @return 操作结果
		*/
		public function depotSendOut($repId) {
			if (empty($repId)) {
				return $this->getErrorMsg(C("ERRNO.ERROR_PARAM_ERR"),C("ERRNO.ERROR_PARAM_ERR_DESC"));
			}
			$repInfo = $this->where("id={$repId}")->find();
			if (empty($repInfo)) {
				return $this->getErrorMsg(C("ERRNO.ERR_GET_PURCHASE_ORDER_FAIL"),
					C("ERRNO.ERR_GET_PURCHASE_ORDER_FAIL_DESC"));
			}
			if ($repInfo['status'] != C("STATUS.STATUS_REP_PURCHASE_CONFIRM")) {
				return $this->getErrorMsg(C("ERRNO.ERR_PURCHASE_ORDER_WRONG_STATUS"),
					C("ERRNO.ERR_PURCHASE_ORDER_WRONG_STATUS_DESC"));
			}

			$this->startTrans();
			$boolUpdateResult = $this->where("id={$repId}")->save(array("status" => C("STATUS.STATUS_REP_PURCHASE_POSTING")));
			if (empty($boolUpdateResult)) {
				$this->rollback();
				return $this->getErrorMsg(C("ERRNO.ERR_UPDATE_PURCHASE_ORDER_FAIL"),
					C("ERRNO.ERR_UPDATE_PURCHASE_ORDER_FAIL_DESC"));
			}
			//更新出货仓库商品数量
			if (empty($this->_purchaseDetail)) {
				$this->_purchaseDetail = D("Seller/SellerPurchaseDetail");
			}
			$boolDetailResult = $this->_purchaseDetail->updateProductOnSendOut($repId);
			if (empty($boolDetailResult)) {
				$this->rollback();
				return $this->getErrorMsg(C("ERRNO.ERR_UPDATE_PRODUCT_FAIL"),
					C("ERRNO.ERR_UPDATE_PRODUCT_FAIL_DESC"));
			}
			$this->commit();
			return true;
		}

		/**
		* @desc 商户确认收货
		* @param 补货单id
		* @return 操作结果
		*/
		public function sellerConfirm($repId) {
			if (empty($repId)) {
				return $this->getErrorMsg(C("ERRNO.ERROR_PARAM_ERR"),C("ERRNO.ERROR_PARAM_ERR_DESC"));
			}
			$repInfo = $this->where("id={$repId}")->find();
			if (empty($repInfo)) {
				return $this->getErrorMsg(C("ERRNO.ERR_GET_PURCHASE_ORDER_FAIL"),
					C("ERRNO.ERR_GET_PURCHASE_ORDER_FAIL_DESC"));
			}
			if ($repInfo['status'] != C("STATUS.STATUS_REP_PURCHASE_POSTING")) {
				return $this->getErrorMsg(C("ERRNO.ERR_PURCHASE_ORDER_WRONG_STATUS"),
					C("ERRNO.ERR_PURCHASE_ORDER_WRONG_STATUS_DESC"));
			}

			$this->startTrans();
			$boolUpdateResult = $this->where("id={$repId}")->save(array("status" => C("STATUS.STATUS_REP_PURCHASE_RECEIVED")));
			if (empty($boolUpdateResult)) {
				$this->rollback();
				return $this->getErrorMsg(C("ERRNO.ERR_UPDATE_PURCHASE_ORDER_FAIL"),
					C("ERRNO.ERR_UPDATE_PURCHASE_ORDER_FAIL_DESC"));
			}
			//更新出货仓库商品数量
			if (empty($this->_purchaseDetail)) {
				$this->_purchaseDetail = D("Seller/SellerPurchaseDetail");
			}
			$boolDetailResult = $this->_purchaseDetail->updateProductOnConfirm($repId);
			if (empty($boolDetailResult)) {
				$this->rollback();
				return $this->getErrorMsg(C("ERRNO.ERR_UPDATE_PRODUCT_FAIL"),
					C("ERRNO.ERR_UPDATE_PRODUCT_FAIL_DESC"));
			}
			$this->commit();
			return true;
		}

		/**
		* @desc 获取补货单表
		* @param 商户id，查询开始时间，查询结束时间，状态
		* @return 操作结果
		*/
		public function getReplenishmentList($sellerId,$startTime,$endTime,$intStatus=null) {
			if (empty($sellerId) || empty($startTime) || empty($endTime)) {
				return $this->getErrorMsg(C("ERRNO.ERROR_PARAM_ERR"),C("ERRNO.ERROR_PARAM_ERR_DESC"));
			}
			$endTime = strtotime($endTime) + 86400;
			if (!empty($intStatus)) {
				$arrList = $this->where(
					"manager_id=%d AND status=%d AND datetime>=%d AND datetime<%d",
					array(
						$sellerId,
						$intStatus,
						$startTime,
						$endTime,	
					)
				)->select();
			} else {
				$arrList = $this->where(
					"manager_id=%d AND datetime>=%d AND datetime<%d",
					array(
						$sellerId,
						$startTime,
						$endTime,	
					)
				)->select();
			}
			if (empty($arrList)) {
				return array();			
			}
			//获取商品详情
			foreach ($arrList as &$arrValue) {
				$detail = $this->_purchaseDetail->getDetail($arrValue["id"]);
				$arrValue["detail"] = $detail;
			}

			return $arrList;
		}

		/**
		* @desc 根据仓库，获取提交的补货单表
		* @param 
		* @return 操作结果
		*/
		public function getReplenishmentListByDepotId($depotId) {
			if (empty($depotId)) {
				return $this->getErrorMsg(C("ERRNO.ERROR_PARAM_ERR"),C("ERRNO.ERROR_PARAM_ERR_DESC"));
			}
			$arrList = $this->where(
				"depot_id=%d AND status=%d",
				array(
					$depotId,
					C("STATUS.STATUS_REP_PURCHASE_SUBMIT"),
				)
			)->select();
			if (empty($arrList)) {
				return array();			
			}
			//获取商品详情
			foreach ($arrList as &$arrValue) {
				$detail = $this->_purchaseDetail->getDetail($arrValue["id"]);
				$arrValue["detail"] = $detail;
			}

			return $arrList;
		}
	}