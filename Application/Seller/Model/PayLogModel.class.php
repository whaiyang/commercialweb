<?php
	namespace Seller\Model;
	use Think\Model;

	class PayLogModel extends BaseModel {
		protected $trueTableName = 'pay_log';

		public function _initialize() {
		}

		public function addOne($tradeNo,$totalFee,$ext=" ") {
			if (empty($tradeNo) || empty($totalFee)) {
				return false;
			}
			$curTime = time();
			$boolResult = $this->add(array(
				"trade_no" => $tradeNo,
				"src_trade_no" => "000",
				"total_fee" => $totalFee,
				"ext" => $ext,
				"status" => C("STATUS.STATUS_PAY_PROCESSING"),
				"create_time" => $curTime,
				"update_time" => $curTime,
			));
			if ($boolResult) {
				return $this->getPk();
			} else {
				return false;
			}
		}

		public function payConfirm($payId,$srcTradeNo,$status) {
			if (empty($payId) || empty($srcTradeNo) || empty($status)) {
				return false;
			}
			$curTime = time();
			$boolResult = $this->where("id=%d",$payId)->save(array(
				"src_trade_no" => $srcTradeNo,
				"status" => $status,
				"update_time" => $curTime,
			));
			return $boolResult;
		} 

		public function refundConfirm($payId,$status,$ext=" ") {
			if (empty($payId) || empty($srcTradeNo) || empty($status)) {
				return false;
			}
			$curTime = time();
			$strSql = "UPDATE {$this->trueTableName} SET ";
			$strSql .= "status=%s,update_time=%d,ext=CONCAT(ext,',',%s) ";
			$strSql .= "WHERE id=%d";
			$boolResult = $this->query($strSql,$status,$curTime,$ext,$payId);
			return $boolResult;
		} 
	}