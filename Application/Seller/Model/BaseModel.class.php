<?php
	namespace Seller\Model;
	use Think\Model;

	class BaseModel extends Model {
		public function getErrorMsg($errCode=0,$errMsg="") {
			return array("errNo" => $errCode,"errMsg" => $errMsg);
		}

		//检测某字段的值是否小于一个数值
		public function checkColumnCount($tableName,$pkValue,$columnName,$intNum,$pkName="id") {
			$info = $this->table($tableName)->where(array($pkName => $pkValue))->field($columnName)->find();
			if (empty($info)) {
				return false;
			}
			$realColumnValue = $info[0][$columnName];
			if ($realColumnValue < $intNum) {
				return false;
			} else {
				return true;
			}
		}
	}