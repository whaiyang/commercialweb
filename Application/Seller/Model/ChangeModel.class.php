<?php
	namespace Seller\Model;
	use Think\Model;

	class ChangeModel extends BaseModel {
		protected $trueTableName = 'change';

		public function _initialize() {
			$this->_productModel = D("Depot/Product");
			$this->_purhcaseModel = D("Seller/SellerPurchase");
			$this->_purchaseDetail = D("Seller/SellerPurchaseDetail");
		} 

		/**
		* @desc 添加一批改货纪录
		* @param 进货单id，需要改货的进货单详情id数组，对应的改货数
		* @return 操作结果
		*/
		public function addChangeRecord($purchaseId,$purchaseDetailIds,$goodsCount) {
			if (empty($purchaseId) || empty($purchaseDetailIds) || empty($goodsCount)
				|| !is_array($purchaseDetailIds) || !is_array($goodsCount)) {
				return $this->getErrorMsg(C("ERRNO.ERROR_PARAM_ERR"),C("ERRNO.ERROR_PARAM_ERR_DESC"));
			} 

			if (count($purchaseDetailIds) != count($goodsCount)) {
				return $this->getErrorMsg(C("ERRNO.ERROR_PARAM_ERR"),C("ERRNO.ERROR_PARAM_ERR_DESC"));
			}

			$purchaseInfo = $this->_purhcaseModel->where("id=%d",array($purchaseId))->find();
			if (empty($purchaseInfo)) {
				return $this->getErrorMsg(C("ERRNO.ERR_PURCHASE_ORDER_NOT_EXIST"),
					C("ERRNO.ERR_PURCHASE_ORDER_NOT_EXIST_DESC"));
			}

			if ($purchaseInfo['status'] != C("STATUS.STATUS_REP_PURCHASE_POSTING")) {
				return $this->getErrorMsg(C("ERRNO.ERR_PURCHASE_ORDER_WRONG_STATUS"),
					C("ERRNO.ERR_PURCHASE_ORDER_WRONG_STATUS_DESC"));
			}

			$this->startTrans();
			$curTime = time();
			//添加改货纪录
			foreach ($purchaseDetailIds as $key => $detailId) {
				$detailInfo = $this->_purchaseDetail->getDetailById($detailId,$purchaseId);
				if (empty($detailInfo)) {
					$this->rollback();
					return $this->getErrorMsg(C("ERRNO.ERR_GET_PURCHASE_ORDER_DETAIL_FAIL"),
						C("ERRNO.ERR_GET_PURCHASE_ORDER_DETAIL_FAIL_DESC"));
				}
				$changeData = array(
					"seller_id" => $purchaseInfo["manager_id"],
					"purchase_id" => $purchaseId,
					"purchase_detail_id" => $detailId,
					"purchase_goods_id" => $detailInfo["purchase_goods_id"],
					"depot_goods_id" => $detailInfo["depot_goods_id"],
					"goods_amount" => $goodsCount[$key],
					"create_time" => $curTime,
					"update_time" => $curTime,
					"status" => C("STATUS.STATUS_CHANGE_SUBMIT"),
				);
				$boolResult = $this->add($changeData);
				if (empty($boolResult)) {
					$this->rollback();
					return $this->getErrorMsg(C("ERRNO.ERR_ADD_PURCHASE_ORDER_FAIL"),
						C("ERRNO.ERR_ADD_PURCHASE_ORDER_FAIL_DESC"));
				}
			}
			//更改进货单状态为正在改货
			$boolResult = $this->_purhcaseModel->where("id=%d",array($purchaseId))->save(array("status" => 
				C("STATUS.STATUS_REP_PURCHASE_CHANGING")));
			if (empty($boolResult)) {
				$this->rollback();
				return $this->getErrorMsg(C("ERRNO.ERR_UPDATE_PURCHASE_ORDER_FAIL"),
						C("ERRNO.ERR_UPDATE_PURCHASE_ORDER_FAIL_DESC"));
			}
			$this->commit();
			return true;
		}

		/**
		* @desc 审核改货纪录
		* @param 记录id，是否通过
		* @return 操作结果
		*/
		public function checkChangeRecord($changeId,$boolPass=false) {
			if (empty($changeId)) {
				return $this->getErrorMsg(C("ERRNO.ERROR_PARAM_ERR"),C("ERRNO.ERROR_PARAM_ERR_DESC"));
			}
			$changeInfo = $this->where("id=%d",array($changeId))->find();
			if (empty($changeInfo)) {
				return $this->getErrorMsg(C("ERRNO.ERR_PURCHASE_ORDER_NOT_EXIST"),
					C("ERRNO.ERR_PURCHASE_ORDER_NOT_EXIST_DESC"));
			} 
			if ($changeInfo['status'] != C("STATUS.STATUS_CHANGE_SUBMIT")) {
				return $this->getErrorMsg(C("ERRNO.ERR_PURCHASE_ORDER_WRONG_STATUS"),
					C("ERRNO.ERR_PURCHASE_ORDER_WRONG_STATUS_DESC"));
			}
			$purchaseId = $changeInfo["purchase_id"];
			$purchaseDetailId = $changeInfo["purchase_detail_id"];
			$goodsAmount = $changeInfo["goods_amount"];
			//更新改货单状态
			$intStatus = C("STATUS.STATUS_CHANGE_DISAPPROVE");
			if ($boolPass == true) {
				$intStatus = C("STATUS.STATUS_CHANGE_COMFIRM");
			}
			$this->startTrans();
			//更新改货单状态
			$boolUpdateResult = $this->where("id=%d",array($changeId))->save(array("status" => $intStatus,"update_time" => time()));
			if (empty($boolUpdateResult)) {
				$this->rollback();
				return $this->getErrorMsg(C("ERRNO.ERR_UPDATE_PURCHASE_ORDER_FAIL"),
					C("ERRNO.ERR_UPDATE_PURCHASE_ORDER_FAIL_DESC"));
			}
			if (empty($this->_purhcaseModel)) {
				$this->_purhcaseModel = D("Seller/SellerPurchase");
			}
			//更新进货单状态
			$boolUpdateResult = $this->_purhcaseModel->where("id=%d",$purchaseId)->save(
				array("status" => C("STATUS.STATUS_REP_PURCHASE_POSTING")));
			if (empty($boolUpdateResult)) {
				$this->rollback();
				return $this->getErrorMsg(C("ERRNO.ERR_UPDATE_PURCHASE_ORDER_FAIL"),
					C("ERRNO.ERR_UPDATE_PURCHASE_ORDER_FAIL_DESC"));
			}
			//改货未被批准，流程结束
			if ($boolPass == false) {
				$this->commit();	
				return true;
			}
			if (empty($this->_purchaseDetail)) {
				$this->_purchaseDetail = D("Seller/SellerPurchaseDetail");
			}
			//更新商户进货单详情商品数量
			$boolUpdateResult = $this->_purchaseDetail->where("id=%d",$purchaseDetailId)->save(array(
				"purchase_goods_count" => array("exp","purchase_goods_count - {$goodsAmount}"),
				"purchase_goods_total_price" => 
					array("exp","purchase_goods_total_price - purchase_goods_price*{$goodsAmount}"),
			));
			if (empty($boolUpdateResult)) {
				$this->rollback();
				return $this->getErrorMsg(C("ERRNO.ERR_UPDATE_PURCHASE_ORDER_FAIL"),
					C("ERRNO.ERR_UPDATE_PURCHASE_ORDER_FAIL_DESC"));
			}
			if (empty($this->_productModel)) {
				$this->_productModel = D("Depot/Product");
			}
			//更新仓库商品数量
			$boolUpdateResult = $this->_productModel->where("id=%d",$changeInfo["depot_goods_id"])->save(array(
				"cur_amount" => array("exp","cur_amount + {$goodsAmount}"),
			));
			if (empty($boolUpdateResult)) {
				$this->rollback();
				return $this->getErrorMsg(C("ERRNO.ERR_UPDATE_PURCHASE_ORDER_FAIL"),
					C("ERRNO.ERR_UPDATE_PURCHASE_ORDER_FAIL_DESC"));
			}
			$this->commit();
			return true;
		}

		/**
		* @desc 获取等待处理的改货申请，
		* @param seller id
		* @return 改货申请表，key是purchase id，value是对应的改货单，按时间降序排列
		*/
		public function getChangeRecordListBySellerId($sellerId) {
			if (empty($sellerId)) {
				return $this->getErrorMsg(C("ERRNO.ERROR_PARAM_ERR"),C("ERRNO.ERROR_PARAM_ERR_DESC"));
			}
			$changeList = $this->join("goods on change.purchase_goods_id=goods.id")
			->join("purchase_detail as d on change.purchase_detail_id=d.id")
			->where("change.seller_id=%d",$sellerId)->field("`change`.*,goods.name,d.purchase_goods_count")->select();
			if (empty($changeList)) {
				return array();
			}
			return $changeList;
		}

		/**
		* @desc 获取等待处理的改货申请，
		* @param depot id
		* @return 改货申请表，key是purchase id，value是对应的改货单，按时间降序排列
		*/
		public function getChangeRecordListByDepotId($depotId) {
			if (empty($depotId)) {
				return $this->getErrorMsg(C("ERRNO.ERROR_PARAM_ERR"),C("ERRNO.ERROR_PARAM_ERR_DESC"));
			}
			//获取所有处于正在改货状态的进货单
			$purchaseRecordArr = $this->_purhcaseModel->where("depot_id=%d AND status=%d",$depotId,C("STATUS.STATUS_REP_PURCHASE_CHANGING"))->select();
			if (empty($purchaseRecordArr)) {
				$purchaseRecordArr = array();
			}
			$arrResult= array();
			foreach ($purchaseRecordArr as $arrValue) {
				//获取对应的所有改货单
				$changeList = $this
				->join("goods on `change`.depot_goods_id=goods.id")
				->join("manager on `change`.seller_id=manager.id")
				->join("purchase_detail as d on `change`.purchase_goods_id=d.purchase_goods_id")
				->where("`change`.purchase_id=%d and d.purchase_id=%d",$arrValue["id"],$arrValue["id"])
				->field("`change`.*,goods.name as product_name,manager.account as seller_name,d.purchase_goods_count,d.purchase_goods_price,d.purchase_goods_total_price")->select();
				foreach ($changeList as &$arrChange) {
					$arrChange["create_time"] = date("Y-m-d H:i:s",$arrChange["create_time"]);
					$arrChange["update_time"] = date("Y-m-d H:i:s",$arrChange["update_time"]);
				}
				$arrResult[$arrValue["id"]] = $changeList;
			}
			return $arrResult;
		}

		/**
		* @desc 获取改货历史，
		* @param depot id
		* @return 改货申请表，key是purchase id，value是对应的改货单，按时间降序排列
		*/
		public function getChangeRecordHistoryByDepotId($depotId,$startTime,$endTime) {
			if (empty($depotId) || empty($startTime) || empty($endTime)) {
				return $this->getErrorMsg(C("ERRNO.ERROR_PARAM_ERR"),C("ERRNO.ERROR_PARAM_ERR_DESC"));
			}
			$startTime = strtotime($startTime);
			$endTime = strtotime($endTime) + 86400;
			$changeList = $this->join("purchase as p on p.id=`change`.purchase_id")
			->join("goods on `change`.depot_goods_id=goods.id")
				->join("manager on `change`.seller_id=manager.id")
				->join("purchase_detail as d on `change`.purchase_goods_id=d.purchase_goods_id")
				->where("`change`.create_time>=%d and `change`.create_time<%d and `change`.status<>%d and d.purchase_id=p.id and p.depot_id=%d",$startTime,$endTime,C("STATUS.STATUS_CHANGE_SUBMIT"),$depotId)
				->field("`change`.*,goods.name as product_name,manager.account as seller_name,d.purchase_goods_count,d.purchase_goods_price,d.purchase_goods_total_price")->select();
			if (empty($changeList)) {
				return array();
			}
			foreach ($changeList as &$arrChange) {
					$arrChange["create_time"] = date("Y-m-d H:i:s",$arrChange["create_time"]);
					$arrChange["update_time"] = date("Y-m-d H:i:s",$arrChange["update_time"]);
			}
			return $changeList;
		}
	}