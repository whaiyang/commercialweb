<?php
	namespace User\Model;
	use Think\Model;

	class UserBaseModel extends Model {

		public function getErrorMsg($errCode=0,$errMsg="") {
			return array("errNo" => $errCode,"errMsg" => $errMsg);
		}
	}