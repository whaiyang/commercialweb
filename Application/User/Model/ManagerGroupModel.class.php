<?php
	namespace User\Model;
	use Think\Model;

	/**
	*@author wanghaiyang
	*@desc 管理员用户组的常用操作
	*/
	class ManagerGroupModel extends UserBaseModel {
		protected $trueTableName = "manager_group";

		/**
		*@desc get user group infomation list
		*@param null
		*@return user group infomation list
		*/
		public function getGroupList() {
			$strSql = "SELECT * from {$this->trueTableName}";
			$arrResult = $this->query($strSql);
			if (empty($arrResult)) {
				$arrResult = array();
			}
			return $arrResult;
		}

		/**
		*@desc get group infomation
		*@param user group id
		*@return group infomation
		*/
		public function getGroupInfoByGid($gId) {
			if (!is_numeric($gId)) {
				$gId = decrypt($gId,$this->secretKey);
			}
			$strSql = "SELECT * from {$this->trueTableName} where id=%d";
			$arrResult = $this->query($strSql,$gId);
			if (empty($arrResult)) {
				$arrResult = array();
			}
			return $arrResult[0];
		}

		/**
		*@desc add a user group
		*@param user group info
		*@param name
		*@param privilege
		*@return result
		*/
		public function addGroup($arrInput) {
			if (empty($arrInput['name']) || empty($arrInput['privilege'])) {
				return $this->getErrorMsg(C("ERRNO.ERROR_PARAM_ERR"),C("ERRNO.ERROR_PARAM_ERR_DESC"));
			}
			$strSql = "INSERT INTO {$this->trueTableName}(name,privilege) VALUES('%s','%s')";
			$boolResult = $this->execute($strSql,array($arrInput['name'],$arrInput['privilege']));
			if (empty($boolResult)) {
				return $this->getErrorMsg(C("ERR_USER_GROUP_ADD_FAIL"),C("ERR_USER_GROUP_ADD_FAIL_DESC"));
			}
			return $boolResult;
		}

		public function deleteGroup($arrInput) {
			if (empty($arrInput['id'])) {
				return $this->getErrorMsg(C("ERRNO.ERROR_PARAM_ERR"),C("ERRNO.ERROR_PARAM_ERR_DESC"));
			}
			$boolResult = $this->where("id=%d",$arrInput['id'])->delete();
			if (empty($boolResult)) {
				return $this->getErrorMsg(C("ERRNO.ERR_USER_GROUP_DELETE_FAIL"),C("ERRNO.ERR_USER_GROUP_DELETE_FAIL_DESC"));
			}
			return $boolResult;
		}

	}
