<?php
	namespace User\Model;
	use Think\Model;

	/**
	*@author wanghaiyang
	*@desc 管理员用户的常用操作
	*/
	class ManagerUserModel extends UserBaseModel {

		protected $trueTableName = "manager";
		protected $groupTableName = "manager_group";
		protected $secretKey = "This-is-a-commercial-o2o-web";

		public function _initialize() {
			$this->_group = D("User/ManagerGroup");
		}
		/**
		*@desc get user infomation
		*@param user id
		*@return user infomation
		*/
		public function getUserInfoByUid($userId) {
			if (!is_numeric($userId)) {
				$userId = decrypt($userId,$this->secretKey);
			}
			$strSql = "SELECT * FROM {$this->trueTableName} WHERE id=%d";
			$arrResult = $this->query($strSql,array($userId));
			if (empty($arrResult)) {
				$arrResult = array();
			}
			return $arrResult[0];
		}
	
		/**
		 * @desc 获取当前登录用户的信息
		 * @param
		 * @return userinfo and privilege info
		 */
		public function getCurrentUserInfo($seller=false) {
			if (empty($_COOKIE[C('MANAGER_USER_INFO_COOKIE')])) {
				return array();
			}	
			$userId = decrypt($_COOKIE[C('MANAGER_USER_INFO_COOKIE')],$this->secretKey);
			if (!$seller) {
				$strSql = "select u.*,g.privilege from {$this->trueTableName} as u ";
				$strSql .= "inner join {$this->groupTableName} as g on u.group_id=g.id ";
				$strSql .= "where u.id={$userId} AND depot_id IS NOT NULL";
			} else {
				$strSql = "select * from {$this->trueTableName} where id={$userId}";
			}
			$arrResult = $this->query($strSql);
			if (empty($arrResult)) {
				return array();
			}
			ini_set('session.gc_maxlifetime',C('COOKIE_EXPIRE_TIME'));
			ini_set('session.cookie_lifetime',C('COOKIE_EXPIRE_TIME'));
			$_SESSION[C('MANAGER_USER_INFO_SESSION')] = $arrResult[0];

			return $arrResult[0];
		}

		/**
		*@desc get user infomation
		*@param user account name
		*@return user infomation
		*/
		public function getUserInfoByAccountName($accountName) {
			$strSql = "SELECT * FROM {$this->trueTableName} WHERE account='%s'";
			$arrResult = $this->query($strSql,array($accountName));
			if (empty($arrResult)) {
				$arrResult = array();
			}
			return $arrResult[0];
		}

		/**
		*@desc get user infomation list
		*@param status
		*@return user infomation list
		*/
		public function getUserInfoList($status=1) {
			$strSql = "SELECT * FROM {$this->trueTableName} ";
			if (!empty($status)) {
				$strSql .= "WHERE status=%d";
				$arrResult = $this->query($strSql,array($status));
			} else {
				$arrResult = $this->query($strSql);
			}
			
			if (empty($arrResult)) {
				$arrResult = array();
			}
			return $arrResult;
		}

		/**
		*@desc 检查用户是否登陆，登陆的话则返回用户信息
		*@param null
		*@return 结果
		*/
		public function checkLogin() {
			$accountId = $_COOKIE[C('MANAGER_USER_INFO_COOKIE')];
			if (empty($accountId)) {
				return false;
			}
			$accountId = decrypt($accountId,$this->secretKey);
		
			$sessionInfo = $_SESSION[C('MANAGER_USER_INFO_SESSION')];
			if (empty($sessionInfo)) {
				return false;
			}
			$sessionId = $sessionInfo['id'];
			if ($accountId != $sessionId) {
				return false;
			}
			$userInfo = $this->getUserInfoByUid($accountId);
			if (empty($userInfo)) {
				return false;
			}
			return true;
		}

		/**
		*@desc 用户登录
		*@param 用户登录信息 account ,password
		*@return 结果
		*/
		public function login($arrInput) {
			if (empty($arrInput["account"]) || empty($arrInput["password"])) {
				return false;
			}
			$account = $arrInput["account"];
			$password = md5($arrInput["password"]);
			$cookieExpireTime = C('COOKIE_EXPIRE_TIME');
			$accountInfo = $this->getUserInfoByAccountName($account);
			if (empty($account)) {
				return false;
			}
			if ($password == $accountInfo['password']) {
				ini_set('session.gc_maxlifetime',C('COOKIE_EXPIRE_TIME'));
				ini_set('session.cookie_lifetime',C('COOKIE_EXPIRE_TIME'));
				setcookie(
					C('MANAGER_USER_INFO_COOKIE'),
					encrypt($accountInfo['id'],$this->secretKey),
					time() + intval(C('COOKIE_EXPIRE_TIME')),
					"/"
				);
				$_SESSION[C('MANAGER_USER_INFO_SESSION')] = $accountInfo;
				return true;
			}
		}

		/**
		*@desc 用户注销
		*@param null
		*@return 结果
		*/
		public function logout() {
			setcookie(C('MANAGER_USER_INFO_COOKIE'),'',time()-3600,"/");
			setcookie("PHPSESSID",'',time()-3600,"/");
			unset($_SESSION[C('MANAGER_USER_INFO_SESSION')]);
			return true;
		}

		/**
		*@desc 管理员注册
		*@param 注册信息
		*@param account : require
		*@param password : rpequire
		*@param depot_id : require
		*@param phone : require
		*@param email : require
		*@param group_id : require
		*@param status : optional
		*@return 结果
		*/
		public function register($arrInput) {
			if (empty($arrInput["account"]) || empty($arrInput["password"]) || empty($arrInput["depot_id"])
				|| empty($arrInput["phone"]) || empty($arrInput["email"]) || empty($arrInput["group_id"])) {
				return $this->getErrorMsg(C("ERRNO.ERROR_PARAM_ERR"),C("ERRNO.ERROR_PARAM_ERR_DESC"));
			}

			if (empty($arrInput["status"])) {
				$arrInput["status"] = 1;
			}

			$groupInfo = $this->_group->getGroupInfoByGid($arrInput["group_id"]);
			if (empty($groupInfo)) {
				return $this->getErrorMsg(C("ERRNO.ERR_USER_GROUP_NOT_EXIST"),C("ERRNO.ERR_USER_GROUP_NOT_EXIST_DESC"));
			}
			$arrInput["password"] = md5($arrInput["password"]);
			$curTime = time();

			$fields = "account,password,phone,email,group_id,status,create_time,update_time,depot_id";
			$valuesStr = "'%s','%s','%s','%s',%d,%d,%d,%d,%d";
			$valuesArr = array(
				$arrInput['account'],
				$arrInput['password'],
				$arrInput['phone'],
				$arrInput['email'],
				$arrInput['group_id'],
				$arrInput['status'],
				$curTime,
				$curTime,
				$arrInput["depot_id"],
			);
			$strSql = "INSERT INTO {$this->trueTableName}({$fields}) VALUES($valuesStr)";
			$arrResult = $this->execute($strSql,$valuesArr);
			if (empty($arrResult)) {
				return $this->getErrorMsg(C("ERRNO.ERR_USER_REGISTER_FAIL"),C("ERRNO.ERR_USER_REGISTER_FAIL_DESC"));
			}
			return $arrResult;
		}

		public function getUserList() {
			$strSql = "select u.id as uid,g.id as gid,u.account,u.phone,u.email,g.name as group_name, ";
			$strSql .= "g.privilege,from_unixtime(u.create_time) as create_time,from_unixtime(u.update_time) as update_time,u.status ";
			$strSql .= "from {$this->groupTableName} as g ";
			$strSql .= "inner join {$this->trueTableName} as u ";
			$strSql .= "on g.id=u.group_id";
			$arrResult = $this->query($strSql);
			if (empty($arrResult)) {
				return $arrResult;
			}
			return $arrResult;
		}
	}
