<?php
	namespace User\Model;
	use Think\Model;

	/**
	*@author wanghaiyang
	*@desc 管理员用户的常用操作
	*/
	class CustomerUserModel extends UserBaseModel {

		protected $trueTableName = "custom";
		protected $secretKey = "This-is-a-commercial-o2o-web";

		/**
		*@desc get user infomation
		*@param user id
		*@return user infomation
		*/
		public function getUserInfoByUid($userId) {
			if (!is_numeric($userId)) {
				$userId = decrypt($userId,$this->secretKey);
			}
			$strSql = "SELECT * FROM {$this->trueTableName} WHERE id=%d";
			$arrResult = $this->query($strSql,array($userId));
			if (empty($arrResult)) {
				$arrResult = array();
			}
			return $arrResult[0];
		}

		/**
		*@desc get user infomation
		*@param user account name
		*@return user infomation
		*/
		public function getUserInfoByAccountName($accountName) {
			$strSql = "SELECT * FROM {$this->trueTableName} WHERE account=%s";
			$arrResult = $this->query($strSql,array($accountName));
			if (empty($arrResult)) {
				$arrResult = array();
			}
			return $arrResult[0];
		}

		/**
		*@desc get user infomation list
		*@param status
		*@return user infomation list
		*/
		public function getUserInfoList($status=1) {
			$strSql = "SELECT * FROM {$this->trueTableName} ";
			if (!empty($status)) {
				$strSql .= "WHERE status=%d";
				$arrResult = $this->query($strSql,array($status));
			} else {
				$arrResult = $this->query($strSql);
			}
			
			if (empty($arrResult)) {
				$arrResult = array();
			}
			return $arrResult;
		}

		/**
		*@desc 检查用户是否登陆，登陆的话则返回用户信息
		*@param null
		*@return 结果
		*/
		public function checkLogin() {
			$accountId = $_COOKIE[C('CUSTOM_USER_INFO_COOKIE')];
			if (empty($accountId)) {
				return false;
			}
			$accountId = decrypt($accountId,$this->secretKey);

			$sessionInfo = $_SESSION[C('CUSTOM_USER_INFO_SESSION')];
			if (empty($sessionInfo)) {
				return false;
			}
			$sessionId = $sessionInfo['id'];
			if ($accountId != $sessionId) {
				return false;
			}
			$userInfo = $this->getUserInfoByUid($accountId);
			if (empty($userInfo)) {
				return false;
			}
			return true;
		}

		/**
		*@desc 用户登录
		*@param 用户登录信息 account ,password
		*@return 结果
		*/
		public function login($arrInput) {
			if (empty($arrInput["account"]) || empty($arrInput["password"])) {
				return false;
			}
			$account = $arrInput["account"];
			$password = md5($arrInput["password"]);
			$cookieExpireTime = C('COOKIE_EXPIRE_TIME');
			$accountInfo = $this->getUserInfoByAccountName($account);
			if (empty($accountInfo)) {
				return false;
			}
			if ($password == $accountInfo['password']) {
				ini_set('session.gc_maxlifetime',C('COOKIE_EXPIRE_TIME'));
				ini_set('session.cookie_lifetime',C('COOKIE_EXPIRE_TIME'));
				setcookie(
					C('CUSTOM_USER_INFO_COOKIE'),
					encrypt($accountInfo['id'],$this->secretKey),
					time() + intval(C('COOKIE_EXPIRE_TIME')),
					"/"
				);
				$_SESSION[C('CUSTOM_USER_INFO_SESSION')] = $accountInfo;
				return true;
			}
			else{
				return false;
			}
		}

		/**
		*@desc 验证用户账号和密码信息
		*@param 用户登录信息 account ,password
		*@return 结果
		*/
		public function checkAccAndPwd($arrInput) {
			if (empty($arrInput["account"]) || empty($arrInput["password"])) {
				return false;
			}
			$account = $arrInput["account"];
			$password = md5($arrInput["password"]);
			$accountInfo = $this->getUserInfoByAccountName($account);
			if (empty($accountInfo)) {
				return false;
			}
			if ($password == $accountInfo['password']) {
				return true;
			}
			else{
				return false;
			}
		}

		/**
		*@desc 用户注销
		*@param null
		*@return 结果
		*/
		public function logout() {
			setcookie(C('CUSTOM_USER_INFO_COOKIE'),'',time()-3600,"/");
			setcookie("PHPSESSID",'',time()-3600,"/");
			unset($_SESSION[C('CUSTOM_USER_INFO_SESSION')]);
			return true;
		}

		/**
		*@desc 用户注册
		*@param 注册信息
		*@param account : require
		*@param password : require
		*@param phone : require
		*@param email : require
		*@param pid : require
		*@param address : require
		*@param credits : require
		*@return 结果
		*/
		public function register($arrInput) {
			if (empty($arrInput["account"]) || empty($arrInput["password"]) || empty($arrInput["credits"]) 
				|| empty($arrInput["phone"]) || empty($arrInput["email"]) || empty($arrInput["address"]) ) {
				return $this->getErrorMsg(C("ERRNO.ERROR_PARAM_ERR"),C("ERRNO.ERROR_PARAM_ERR_DESC"));
			}

			$accountInfo = $this->getUserInfoByAccountName($arrInput["account"]);
			if (!empty($accountInfo)) {
				return $this->getErrorMsg(C("ERRNO.ERR_USER_ALREADY_EXIST"),C("ERRNO.ERR_USER_ALREADY_EXIST_DESC"));
			}
			$arrInput["password"] = md5($arrInput["password"]);
			$arrInput['status'] = 1;
			$curTime = time();
			$curdate = date('Y-m-d');
			$curdate = $curdate.':1';

			$fields = "account,password,credits,phone,email,address,status,create_time,sign_date_value,update_time";
			$valuesStr = "%s,%s,%d,%s,%s,%s,%d,%d,%s,%d";
			$valuesArr = array(
				$arrInput['account'],
				$arrInput['password'],
				$arrInput['credits'],
				$arrInput['phone'],
				$arrInput['email'],
				$arrInput['address'],
				$arrInput['status'],
				$curTime,
				$curdate,
				$curTime,
			);
			$strSql = "INSERT INTO {$this->trueTableName}({$fields}) VALUES($valuesStr)";
			$arrResult = $this->execute($strSql,$valuesArr);
			if (empty($arrResult)) {
				return $this->getErrorMsg(C("ERRNO.ERR_USER_REGISTER_FAIL"),C("ERRNO.ERR_USER_REGISTER_FAIL_DESC"));
			}
			return $arrResult;
		}

		/**
		*@desc 用户修改密码
		*@param 用户信息 account ,new password
		*@return 结果
		*/
		public function changePwd($arrInput) {
			if(arrInput[stasus]!=1){
				return $this->getErrorMsg(C("ERRNO.ERR_USER_NOT_CHECKED"),C("ERRNO.ERR_USER_NOT_CHECKED_DESC"));
			}
			else{
				$codes = md5($arrInput["newpassword"]);
				$valuesArr = array(
					$codes,
					$arrInput[account],
					);
				$strSql = "UPDATE {$this->trueTableName} SET password=%s WHERE account=%s";
				$boolResult = $this->execute($strSql,$valuesArr);
				if($boolResult){
					return true;
				}
				else{
					return $this->getErrorMsg(C("ERRNO.ERR_UPDATE_FAIL"),C("ERRNO.ERR_UPDATE_FAIL_DESC"));
				}
			}
		}
	}
