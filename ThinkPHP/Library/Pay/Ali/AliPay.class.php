<?php

namespace Pay\Ali;

class AliPay {

	private $_configArray;
	private $_partner;
	private $_sellerEmail;
	private $_key;
	private $_signType;
	private $_inputCharset;
	private $_cacert;
	private $_transport;
	private $_notifyUrl;
	private $_returnUrl;

	private $_paymentType = 1;

	private $_alipaySubmit;


	public function __construct($configArr) {
		$this->_configArray = $configArr;
		$this->_partner = $configArr["partner"];
		$this->_sellerEmail = $configArr["seller_email"];
		$this->_key = $configArr["key"];
		$this->_signType = $configArr["sign_type"];
		$this->_inputCharset = $configArr["input_charset"];
		$this->_cacert = getcwd()."/".$configArr["cacert"];
		$this->_transport = $configArr["transport"];
		$this->_configArray["cCj"] = $this->_cacert;

		$this->_alipaySubmit = new Lib\AlipaySubmit($this->_configArray);
	}

	/**
	* 生成支付宝支付链接和参数
	* @param 订单号
	* @param 商品名
	* @param 总价
	* @param 描述
	* @param 异步回调地址
	* @param 支付完成返回地址
	* @return form链接，method，input参数
	*/
	public function buildPayRequestParam($tradeNo,$orderSubject,$totalFee,$orderDesc,$notifyUrl,$returnUrl) {
		$this->_notifyUrl = $notifyUrl;
		$this->_returnUrl = $returnUrl;
		if (empty($this->_partner) || empty($this->_sellerEmail) || empty($this->_paymentType)
			|| empty($notifyUrl) || empty($returnUrl) || empty($this->_inputCharset)
			|| empty($tradeNo) || empty($orderSubject) || empty($totalFee) || empty($orderDesc)) {
			return false;
		}
		
		$parameter = array(
			"service" => "create_direct_pay_by_user",
			"partner" => trim($this->_partner),
			"seller_email" => trim($this->_sellerEmail),
			"payment_type"	=> $this->_paymentType,
			"notify_url"	=> $this->_notifyUrl,
			"return_url"	=> $this->_returnUrl,
			"out_trade_no"	=> $tradeNo,
			"subject"	=> $orderSubject,
			"total_fee"	=> $totalFee,
			"body"	=> $orderDesc,
			"show_url"	=> "",
			"anti_phishing_key"	=> $this->_alipaySubmit->query_timestamp(),
			"exter_invoke_ip"	=> $this->getIP(),
			"_input_charset"	=> trim(strtolower($this->_inputCharset)),
		);

		$formData = $this->_alipaySubmit->buildRequestFormData($parameter,"get");
		return $formData;
	}

	/**
	* 生成支付宝退款链接和参数
	* @param 批次号
	* @param 退款笔数
	* @param 退款详细数据
	* @param 异步回调地址
	* @param 支付完成返回地址
	* @return form链接，method，input参数
	*/
	public function buildRefundRequestParam($batchNo,$batchNum,$detailData,$notifyUrl,$returnUrl) {
		$this->_notifyUrl = $notifyUrl;
		$this->_returnUrl = $returnUrl;
		if (empty($this->_partner) || empty($this->_sellerEmail) || empty($this->_paymentType)
			|| empty($notifyUrl) || empty($returnUrl) || empty($this->_inputCharset)
			|| empty($batchNo) || empty($batchNum) || empty($detailData)) {
			return false;
		}
		
		$parameter = array(
			"service" => "create_direct_pay_by_user",
			"partner" => trim($this->_partner),
			"notify_url"	=> $this->_notifyUrl,
			"seller_email" => trim($this->_sellerEmail),
			"refund_date" => date("Y-m-d H:i:s"),
			"batch_no" => $batchNo,
			"batch_num" => $batchNum,
			"detail_data" => $detailData,
 			"_input_charset"	=> trim(strtolower($this->_inputCharset)),
		);

		$formData = $this->_alipaySubmit->buildRequestFormData($parameter,"get");
		return $formData;
	}

	public function getIP() {
		if(!empty($_SERVER["HTTP_CLIENT_IP"]))	{
  			$cip = $_SERVER["HTTP_CLIENT_IP"];	
		}	elseif(!empty($_SERVER["HTTP_X_FORWARDED_FOR"])) {
  			$cip = $_SERVER["HTTP_X_FORWARDED_FOR"];
		} elseif(!empty($_SERVER["REMOTE_ADDR"])) {
  			$cip = $_SERVER["REMOTE_ADDR"];
		} else{
  			$cip = "";
		}
		return $cip;
	}
}