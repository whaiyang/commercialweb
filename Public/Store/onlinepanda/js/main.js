/**
 * Created by csm on 2015/4/9.
 */


window.onload = function(){
    new JsDatePick({
        useMode:2,
        target:"birthday",
        dateFormat:"%Y-%m-%d",
        yearsRange:[1915,2020]
        /*selectedDate:{				This is an example of what the full configuration offers.
         day:5,						For full documentation about these settings please see the full version of the code.
         month:9,
         year:2006
         },
         yearsRange:[1978,2020],
         limitToToday:false,
         cellColorScheme:"beige",
         dateFormat:"%m-%d-%Y",
         imgPath:"img/",
         weekStartDay:1
         useMode：1为一开始为显示状态，2为一开始为隐藏状态
         target：附加这个日历的元素，请使用ID，比如前面useMode为2，这个target为input，那么就是点击这个input就会打开日历
         dateFormat：选择日期后显示的日期格式
         %d - 日期，始终两位数，比如02
         %j - 日期，十位不带零，比如2
         %m - 月，始终两位数，比如02
         %M - 三个英文的文本月份，比如JAN
         %n - 月，十位不带零，比如2
         %F - 全文本月份，比如 January
         %Y - 四位年
         %y - 两位年
         yearsRange：日历显示范围，如上例不包括2010和2200

         */

    });
};


$(function () {
    $('[data-toggle="popover"]').popover()
})

/*模态框操作*/
$(function(){
    $('#myModal').modal({
        show:true,
        backdrop:true
    })

});

$(function(){
    $('#myModal_fb').modal({
        show:false,
        backdrop:true
    })

});

$(function(){
    $('#myModal_ssicon').modal({
        show:false,
        backdrop:true
    })

});


function lz(){
  $("#myModal_lz").modal('show')
}

$(function(){
    $('#myModal_cm').modal({
        show:false,
        backdrop:true
    })

});


function sysicon(){
    $("ssicon_btn").click($("#myModal_ssicon").modal('show'))
}

function camera(){
    $("camera_btn").click($("#myModal_cm").modal('show'))
}
/*定位操作*/

function Local(){
    $("#local").click($('#myModal').modal('show'))
}
$(function(){
    $("#selectSchool").bind("change",function(){
        var that=this;
        $("#local").html("<span class='glyphicon glyphicon-map-marker'></span>"+$(that).val()

        )
    })
})

/*右边固定拦控制*/
function feedback(){
    $("feedback_btn").click($("#myModal_fb").modal('show'))
}
function wechat(){
    $("wechat_btn").click($("#myModal_wc").modal('show'))
}

/*底部控制*/
function company(){
    $("#company").click(alert("公司简介"))
}

/*图片阅览*/
//本地图片预览
function setImagePreview(fieldupload, image, imagediv) {
    var docObj = document.getElementById(fieldupload);
    var imgObjPreview = document.getElementById(image);
    if (docObj.files && docObj.files[0]) {
        //火狐下，直接设img属性
        imgObjPreview.style.display = 'block';
        imgObjPreview.style.width = '340px';
        imgObjPreview.style.height = '340px';
        //imgObjPreview.src = docObj.files[0].getAsDataURL();
        //火狐7以上版本不能用上面的getAsDataURL()方式获取，需要一下方式
        imgObjPreview.src = window.URL.createObjectURL(docObj.files[0]);
    } else {
        //IE下，使用滤镜
        docObj.select();
        var imgSrc = document.selection.createRange().text;
        var localImagId = document.getElementById(imagediv);
        //必须设置初始大小
        localImagId.style.width = "100%";
        localImagId.style.height = "100%";
        //图片异常的捕捉，防止用户修改后缀来伪造图片
        try {
            localImagId.style.filter = "progid:DXImageTransform.Microsoft.AlphaImageLoader(sizingMethod=scale)"; localImagId.filters.item("DXImageTransform.Microsoft.AlphaImageLoader").src = imgSrc;
        } catch (e) {
            alert("您上传的图片格式不正确，请重新选择!");
            return false;
        }
        imgObjPreview.style.display = 'none';
        document.selection.empty();
    }
    return true;
}

//选择头像

function setIconPreview(icon, image) {
   ;
    var docObj = document.getElementById(icon);
    var url = docObj.src;
    var imgObjPreview = document.getElementById(image);
    imgObjPreview.src=url;
    if(url){
        imgObjPreview.style.display = 'block';
        imgObjPreview.style.width = '100%';
        imgObjPreview.style.height = '100%'
    };
    return true;
}

//积分转盘设置 time和data后台传入
$(function(){
    var timeOut = function(){  //超时函数
        $("#lotteryBtn").rotate({
            angle:0,
            duration: 10000,
            animateTo: 2160, //这里是设置请求超时后返回的角度，所以应该还是回到最原始的位置，2160是因为我要让它转6圈，就是360*6得来的
            callback:function(){
                alert('网络超时')
            }
        });
    };
    var rotateFunc = function(awards,angle,text){  //awards:奖项，angle:奖项对应的角度
        $('#lotteryBtn').stopRotate();
        $("#lotteryBtn").rotate({
            angle:0,
            duration: 5000,
            animateTo: angle+1440, //angle是图片上各奖项对应的角度，1440是我要让指针旋转4圈。所以最后的结束的角度就是这样子^^
            callback:function(){
                alert(text)
            }
        });
    };

    $("#lotteryBtn").rotate({
        bind:
        {
            click: function(){
                var time = [0,1];
                time = time[Math.floor(Math.random()*time.length)];
                if(time==0){
                    timeOut(); //网络超时
                }
                if(time==1){
                    var data = [1,2,3,0]; //返回的数组
                    data = data[Math.floor(Math.random()*data.length)];
                    if(data==1){
                        rotateFunc(1,157,'恭喜您抽中的一等奖')
                    }
                    if(data==2){
                        rotateFunc(2,247,'恭喜您抽中的二等奖')
                    }
                    if(data==3){
                        rotateFunc(3,22,'恭喜您抽中的三等奖')
                    }
                    if(data==0){
                        var angle = [67,112,202,292,337];
                        angle = angle[Math.floor(Math.random()*angle.length)]
                        rotateFunc(0,angle,'很遗憾，这次您未抽中奖')
                    }
                }
            }
        }

    });

})



//中奖信息

$(".txtMarquee-top").slide({mainCell:".bd ul",autoPlay:true,effect:"topMarquee",vis:5,interTime:50,trigger:"click"})

//购物车固定
function menuFixed(id){
    var obj = document.getElementById(id);
    var _getHeight = "260";

    window.onscroll = function(){
        changePos(id,_getHeight);
    }
}
function changePos(id,height){
    var obj = document.getElementById(id);
    var scrollTop = document.documentElement.scrollTop || document.body.scrollTop;
    if(scrollTop < height){
        obj.style.position = 'absolute';
        obj.style.top ='20px';
    }else{
        obj.style.position = 'fixed';
        obj.style.top ='45px';
    }
}

$(menuFixed('cart'))
//cart 加入


function shopping_cart(product){
    /*$("#orders").append($("#product1").clone())
    $("#orders").append('<input class="min" name="" type="button" value="-"/>' +
    '<input class="text_box" name="" type="text" value="1" />' +
    '<input class="add" name="" type="button" value="+""/>')*/
    minone=  function(even){

        var that = $(".add");
        var t=$(that).parent().find('input[class*=text_box]');
        t.val(parseInt(t.val())-1)
        if(parseInt(t.val())<0){
            t.val(0);
        }
        setTotal();


    }
    addone= function(){
        $(".add").click(function(){
            var t=$(this).parent().find('input[class*=text_box]');
            t.val(parseInt(t.val())+1)
            setTotal();
            even.stopPropagation();
        })

    }

    function setTotal(){
        var s=0;
        $("#orders > div").each(function(){
            console.log(this);
            s+=parseInt($(this).find('input[class*=text_box]').val())*parseFloat($(this).find('span[class*=price]').text());
        });
        $("#total").html(s.toFixed(2));
    }

    $("#orders").append($('#'+product).clone());
    $("#orders").append('<input class="min" name="" type="button" value="-" onclick="minone()"/>'

    +'<input class="text_box" name="" type="text" value="1" />' +
    '<input class="add" name="" type="button" value="+" onclick="addone()"/>')

    setTotal();


}

//cookies

function setCookie(name,key){

    $.cookie('name', name,'key',key, {expires: 7, path: '/'});
}

function getCookie(){
   var  a =$.cookie('name')
    return  a;
}

function removeCookies(){
    $.removeCookie('name','key', { path: '/' });
}


function cookienav(){
    if(getCookie()===undefined){
         $("#logout").attr("class","row show");
        $("#login").attr("class","row hide");

    }
    else {
        $("#logout").attr("class","row hide");
        $("#login").attr("class","row show");
    }
}

$(function(){
    $(".follow_text").attr("width","50px");
})



